from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import backref, relationship, joinedload




class UserModel(db.Model):

    __tablename__ = 'user'

    user_id = Column(Integer, primary_key=True)
    company_id = Column("company_id", Integer,nullable = False)
    website_id = Column("website_id", Integer, nullable = False)
    name = Column('name', String(100), nullable = False)
    email = Column('email', String(100), nullable = False)
    password = Column('password', Text, nullable = False)
    mobile = Column('mobile', String(20), nullable = False)
    website = Column('website', Text, nullable = False)
    designation = Column('designation', String(100), nullable = False)
    user_type = Column('user_type', Integer, nullable = True)
    api_key = Column('api_key', Text, nullable = True)    
    trained_json_path = Column('trained_json_path', Text, nullable = True)        
    location = Column('location', Integer, nullable = True)
    email_verify_status = Column('email_verify_status', Integer, nullable = True)
    email_verify_token = Column('email_verify_token', Text, nullable = True)    
    mobile_verify_status = Column('mobile_verify_status', Integer, nullable = True)
    reset_paswd_token = Column('reset_paswd_token', Text, nullable = True)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True)

    @classmethod
    def find_by_email_website(cls, email, website_id):
        return cls.query.filter_by(email = email, website_id = website_id).first()

    
    @classmethod
    def find_by_email_company(cls, email, company_id):
        return cls.query.filter_by(email = email, company_id = company_id).first()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email = email).first()

    @classmethod
    def find_by_token(cls, token):
        return cls.query.filter_by(email_verify_token = token).first()

    @classmethod
    def find_by_password_token(cls, token):
        return cls.query.filter_by(reset_paswd_token = token).first()

    
    @classmethod
    def find_by_mobile(cls, mobile):
        return cls.query.filter_by(mobile = mobile).first()
   

    @classmethod
    def find_by_website(cls, website):
        return cls.query.filter_by(website = website).first()
 
    @classmethod
    def get_by_website_id(cls, website_id):        

        return cls.query.filter_by(website_id=website_id).all()
   
    @classmethod
    def get_by_id(cls, id):        
        return cls.query.filter_by(user_id=id).first()
    
    @classmethod
    def find_by_id(cls, id):        
        return { 'users': list(map(lambda x: UserModel.to_json(x), cls.query.filter_by(user_id=id).all())) }



    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
        db.session.flush()

        return
    
    def delete_from_db(self):

        db.session.delete(self)
        db.session.commit()

    @classmethod
    def to_json(cls, x):
            return {
                'user_id': x.user_id,
                'company_id':x.company_id,
                'website_id':x.website_id,
                'email': x.email,
                'password': x.password,
                'name': x.name,
                'mobile':x.mobile,
                'website': x.website,
                'designation':x.designation,
                'api_key': x.api_key,
                'user_type': x.user_type,
                'trained_json_path':x.trained_json_path,
                'email_verify_status':x.email_verify_status,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at)
            }
    
    @classmethod
    def to_customjson(cls, x):
            return {
                'id': x.user_id,
                'email': x.email,
                'name': x.name
            }
    

    @classmethod
    def return_all(cls, page, searchTerm):        
        page = int(page)

        output = ""

        return output
   
    
    @classmethod
    def get_by_api_key(cls, api_key):        
        return {
            'users': list(map(lambda x: UserModel.to_json(x), UserModel.query.filter_by(api_key=api_key).all()))
            }

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)
    
    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)


class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    __bind_key__ = 'MYSQL'

    id = Column(Integer, primary_key = True)
    jti = Column(String(120))
    
    def add(self):
        db.session.add(self)
        db.session.commit()
    
    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti = jti).first()
        return bool(query)


class ClientModel(db.Model):
    __tablename__ = 'client'
    __bind_key__ = 'MYSQL'

    id = Column(Integer, primary_key = True)
    firstname = Column(String(120), nullable = False)
    lastname = Column(String(120), nullable = False)
    
    def save_to_db(self):
        
        db.session.add(self)
        db.session.commit()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'firstname': x.firstname,
                'lastname': x.lastname
            }
        return {'users': list(map(lambda x: to_json(x), ClientModel.query.all()))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

