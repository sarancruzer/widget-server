from flask_restful import Resource, reqparse
from models.package import PackageModel
from models.subscription import SubscriptionModel
from models.packChatbotCount import PackChatbotCountModel
from shared.token import count_token, generate_count_token
from shared import db
from models.packResponseCount import PackResponseCountModel
from models.packageDetails import PackageDetailsModel
from shared.mailer import packageExpiredAlertMail
from models.website import WebsiteModel
from datetime import datetime
import tldextract


parser = reqparse.RequestParser()

parser.add_argument('api_key', required=True, help='This field cannot be left blank')
parser.add_argument('user_token', required=False, help='This field cannot be left blank')

parser.add_argument('client_domain', required=False, help='This field cannot be left blank')



class ChatbotCount(Resource):
    def post(self):
        
        data = parser.parse_args()
        api_key = data['api_key']
        user_token = data['user_token']


        CHATBOT_PERMIT_COUNT = 2
        
        CHATBOT_CURRENT_COUNT = 0

        query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE api_key='"+ api_key +"'"

        package_details = db.db.engine.execute(query)

        pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]

        CHATBOT_PERMIT_COUNT = pack_result['chatbot_count']



        if user_token:
            userToken = count_token(user_token)
            if userToken != False:
                return {
                    'status': 200,
                    'success': True,
                    'message': 'Successfully subscriped! Token ',
                    'data': True,
                    'user_token':str(user_token)
                }, 200 


        resultList = PackChatbotCountModel.return_all(api_key)

        result = list(map(lambda x: PackChatbotCountModel.to_json(x), resultList))

        token = generate_count_token(api_key)
        token = token.decode("utf-8")

        i=0
        if result:
            for value in result:
                apikey = count_token(value['user_token']) 
                if apikey != False:
                    i = i + 1
            
            CHATBOT_CURRENT_COUNT = i
        
        else:

            new_data = PackChatbotCountModel(
                api_key = api_key,
                user_token = token
            )

            new_data.save_to_db()

            return {
                    'status': 200,
                    'success': True,
                    'message': 'Successfully subscriped!NULL ',
                    'data': True,
                    'count':CHATBOT_CURRENT_COUNT + 1,
                    'user_token':str(token)                
                }, 200

        if CHATBOT_CURRENT_COUNT < CHATBOT_PERMIT_COUNT:

            new_data = PackChatbotCountModel(
                api_key = api_key,
                user_token = token
            )

            db.db.session.commit()
            new_data.save_to_db()
            
            return {
                    'status': 200,
                    'success': True,
                    'message': 'Successfully subscriped! LESS THAN ',
                    'data': True,
                    'count': CHATBOT_CURRENT_COUNT + 1,
                    'user_token':str(token)
                   
                }, 200
        else:

            packageExpiredAlertMail(pack_result['email'],pack_result['contact_person'], "Package expired alert!", "",pack_result)
            
            return {
                    'status': 200,
                    'success': True,
                    'message': 'Successfully subscriped! FINAL',
                    'data': False,
                    'count':CHATBOT_CURRENT_COUNT
                }, 200


class ResponseCount(Resource):
    def post(self):
        
        data = parser.parse_args()
        api_key = data['api_key']

        query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"'"

        package_details = db.db.engine.execute(query)

        pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]

        RESPONSE_PERMIT_COUNT = pack_result['response_count']
        RESPONSE_CURRENT_COUNT = 0

        resultList = PackResponseCountModel.get_by_api_key(api_key)
        
        result = []
        if resultList:
            result = PackResponseCountModel.to_json(resultList)
            RESPONSE_CURRENT_COUNT = result['response_count']

        data = True
        if RESPONSE_CURRENT_COUNT > RESPONSE_PERMIT_COUNT:
            data = False

            packageExpiredAlertMail(pack_result['email'],pack_result['contact_person'], "Package expired alert!", "",pack_result)
        
        try:
            return {
                    'status': 200,
                    'success': True,
                    'message': 'Successfully subscriped! ',
                    'data': data,
                   
                }, 200
        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the subscription',
            }, 400
    


class DomainValidation(Resource):
    def post(self):

        data = parser.parse_args()
        api_key = data['api_key']
        client_domain = data['client_domain']

        query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"'"

        package_details = db.db.engine.execute(query)

        pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]

        current_date = datetime.today().strftime('%Y-%m-%d')

        pack_end_at = pack_result['pack_end_at'].strftime('%Y-%m-%d')

        if pack_end_at < current_date:
                data['is_expired'] = 1
                return {
                    'status':200,
                    'success': True,
                    'message': "Sry your package has been expired",
                    'data': False
                }, 200

        
        try:

            websiteList = WebsiteModel.get_by_apikey(api_key)
            website_data = WebsiteModel.to_json(websiteList)


            client = tldextract.extract(client_domain)
           
            client_domain = client.domain 

            server = tldextract.extract(client_domain)
          
            server_domain = server.domain 

            if server_domain == client_domain:
                return {
                    'status': 200,
                    'success': True,
                    'message': 'Valid domain! ',
                    'data': True,
                   
                }, 200
            else:
                return {
                    'status': 200,
                    'success': True,
                    'message': 'Invalid domain! ',
                    'data': False,
                   
                }, 200

        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the subscription',
            }, 400



        return ""
