from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey


class WebsiteModel(db.Model):
    __tablename__ = 'website'

    website_id = Column('website_id',Integer, primary_key=True)
    company_id = Column(Integer, ForeignKey("company.company_id"), nullable = False)    
    created_by = Column('created_by', Integer, nullable = True) 
    website_code = Column('website_code', String(100), nullable = False) 
    website_name = Column('website_name', String(100), nullable = False)     
    api_key = Column('api_key', Text, nullable = True)    
    trained_json_path = Column('trained_json_path', Text, nullable = True)        
    script_source_code = Column('script_source_code', Text, nullable = True)      
    active_status = Column('active_status', Integer, nullable = True)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)

    

    @classmethod
    def find_by_id(cls, website_id):        
        
        return { 'website': list(map(lambda x: WebsiteModel.to_json(x), cls.query.filter_by(website_id=website_id).all())) }

    @classmethod
    def get_by_id(cls, website_id):        

        return cls.query.filter_by(website_id=website_id).first()

    
    @classmethod
    def find_by_name(cls, website_name):
        return cls.query.filter_by(website_name = website_name).first()


    @classmethod
    def get_by_websitename(cls, websitename, usertoken):
        
        return {
            'website': list(map(lambda x: WebsiteModel.to_json(x), WebsiteModel.query.filter_by(website_name = websitename, user_token = usertoken, status = 0).all()))
        }
   
    @classmethod
    def get_by_apikey(cls, apikey):
        return cls.query.filter_by(api_key = apikey).first()

    
    @classmethod
    def get_count_by_company_id(cls, company_id):
        return cls.query.filter_by(company_id = company_id).count()


    @classmethod
    def get_by_api_key(cls, apikey):
        query = "SELECT w.*, c.company_code FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id  where w.api_key = '" + str(apikey) +"'"       
        result = db.engine.execute(query)        

        data = list(map(lambda x: WebsiteModel.to_json_custom(x), result))[0]        
        return data
    

        
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush()
        db.session.rollback()
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    


    @classmethod
    def to_json(cls, x):
      
        
        return {
                'website_id':x.website_id,
                'company_id':x.company_id,
                'website_name': x.website_name,
                'website_code': x.website_code,
                'api_key':x.api_key,
                'trained_json_path': x.trained_json_path,
                'script_source_code': x.script_source_code,
                'active_status':x.active_status,
                'created_by':x.created_by,            
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status,
                
            }
    
    @classmethod
    def to_json_custom(cls, x):
            return {
                'website_id':x.website_id,
                'company_id':x.company_id,
                'website_name': x.website_name,
                'company_code':x.company_code,
                'website_code': x.website_code,
                'api_key':x.api_key,
                'trained_json_path': x.trained_json_path,
                'script_source_code': x.script_source_code,
                'active_status':x.active_status,
                'created_by':x.created_by,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def return_all(cls, page, current_user):        
        page = int(page)     
        db.session.commit()

        data = []

        if(current_user['user_type'] == 2):
            data = db.session.query(WebsiteModel).filter_by(company_id = current_user['company_id']).all()
        elif(current_user['user_type'] == 3):
            data = db.session.query(WebsiteModel).filter_by(website_id = current_user['website_id']).all()

        return data
    
    @classmethod
    def get_by_website_id(cls, website_id):
        return cls.query.filter_by(website_id = website_id).all()

    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

