from shared.db import db
from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from sqlalchemy.orm.session import Session
from sqlalchemy.orm import session


class ContextGroupModel(db.Model):
    __tablename__ = 'context_group'

    context_gp_id = Column('context_gp_id',Integer, primary_key=True)
    website_id = Column('website_id', Integer)
    context_name = Column('context_name', String(50), nullable = False)
    sentence = Column('sentence', Text, nullable = False)
    response_type = Column('response_type', String(100), nullable = False)
    success_msg = Column('success_msg', Text, nullable = True)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer)

    
    @classmethod
    def get_by_contextname(cls, contextname,website_id):
        
        return ContextGroupModel.query.filter_by(context_name = contextname, website_id = website_id).all()

    @classmethod
    def get_by_context_gp_id(cls, context_gp_id):

        return ContextGroupModel.query.filter_by(context_gp_id = context_gp_id).first()

        return {
            'context_group': list(map(lambda x: ContextGroupModel.to_json(x), ContextGroupModel.query.filter_by(context_gp_id = context_gp_id).all()))
        }
    
    @classmethod
    def get_success_by_contextname(cls, context,website_id):
        
        return ContextGroupModel.query.filter_by(context_name = context, website_id = website_id).order_by("context_gp_id desc").first()
    
    @classmethod
    def get_last(cls, context,website_id):

        return cls.query.filter_by(context_name=context).filter_by(website_id=website_id).order_by("context_gp_id desc").first()

    @classmethod
    def get_last_delete(cls, context,website_id):

        return cls.query.filter_by(context_name=context).filter_by(website_id=website_id).all()
        

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    
    def delete_from_db(self):

        db.session.delete(self)
        db.session.commit()    


    @classmethod
    def to_json(cls, x):

        return {
                'context_gp_id': x.context_gp_id,
                'website_id': x.website_id,
                'context_name': x.context_name,
                'sentence': x.sentence,
                'response_type': x.response_type,
                'success_msg': x.success_msg,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def get_by_id(cls, id):        
        return cls.query.filter_by(id=id).first()

    @classmethod
    def return_all(cls):       
        return {'context_group': list(map(lambda x: ContextGroupModel.to_json(x), ContextGroupModel.query.all()))}
    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

   

