from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from datetime import datetime



class PackageModel(db.Model):
    __tablename__ = 'package'

    package_id = Column('package_id',Integer, primary_key=True)
    package_name = Column('package_name', String(100), nullable = False)
    package_price = Column('package_price', Integer, nullable = False)
    package_duration = Column('package_duration', Integer, nullable = True)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)

    

    @classmethod
    def find_by_id(cls, package_id):        
        return cls.query.filter_by(package_id=package_id).first()

    @classmethod
    def get_by_id(cls, package_id):        

        return cls.query.filter_by(package_id=package_id).first()

    @classmethod
    def get_by_id_expire(cls, package_id):        

        return cls.query.filter_by(package_id=package_id,).first()

    @classmethod
    def get_package_company_id(cls, company_id):        
        
        return cls.query.filter_by(company_id=company_id).first()

    
    @classmethod
    def get_by_packagename(cls, packagename, usertoken):
        
        return {
            'packages': list(map(lambda x: PackageModel.to_json(x), PackageModel.query.filter_by(package_name = packagename, user_token = usertoken, status = 0).all()))
        }

           
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    


    @classmethod
    def to_json(cls, x):

            return {
                'package_id':x.package_id,
                'package_name': x.package_name,
                'package_price': x.package_price,
                'package_duration': x.package_duration,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def return_all(cls, page, searchTerm):        
        page = int(page)        
        db.session.commit()
        data = db.session.query(PackageModel).filter(PackageModel.package_name.like("%" + searchTerm + "%")).paginate(page=page, per_page=10)

        return data

    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

