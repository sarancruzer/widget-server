from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy import text




class PlayBookResponseModel(db.Model):
    __tablename__ = 'playbk_resp_det'

    playbk_resp_det_id = Column('playbk_resp_det_id',Integer, primary_key=True,autoincrement=True)
    playbook_id = Column('playbook_id',Integer, nullable = False)
    node_id = Column('node_id', Integer, nullable = False) 
    inner_node = Column('inner_node', Integer, nullable = False)    
    sequence = Column('sequence', Integer, nullable = False) 
    node_type = Column('node_type', String(100), nullable = False) 
    text_response = Column('text_response',String(100), primary_key=True)
    wait_status = Column('wait_status',Integer, nullable = False)
    jump_node = Column('jump_node', String(100), nullable = False) 
    jump_playbook = Column('jump_playbook',String(100)  , primary_key=True)
    branch_id = Column('branch_id',Integer, nullable = False)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    
    @classmethod
    def find_last_added_record(cls, playBook_id,response,node_id):
        return cls.query.filter_by(playbook_id = playBook_id,sequence=node_id,text_response=response).first()

    @classmethod
    def get_by_playbook_jumpid(cls, playBook_id,node):
        #return cls.query.filter_by(playbook_id = playBook_id,node_id=node).order_by("node_id asc").all()

        query_seq = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id >= "+ str(node)
        response_data_seq = db.engine.execute(query_seq)
        return response_data_seq
    @classmethod
    def get_by_playbook_id(cls, playBook_id,types):
        if types=='res':
            return cls.query.filter_by(playbook_id = playBook_id).order_by(text("node_id asc")).all()
        else:
            # return cls.query.filter_by(playbook_id = playBook_id,node_id=types).order_by("node_id asc").all()
            query_seq = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id > "+ str(types)
            response_data_seq = db.engine.execute(query_seq)
            return response_data_seq
    @classmethod
    def get_by_playbook_id_edit(cls, playBook_id):
        
        return cls.query.filter_by(playbook_id = playBook_id,branch_id=0).order_by(text("node_id asc")).all()

    @classmethod
    def get_by_playbook_limit(cls, playBook_id,node_id,branch,conversation_sequence,inner_node):
        #return cls.query.filter_by(playbook_id = playBook_id, node_id > 4).order_by("node_id asc").all()
        print(str(playBook_id)+" "+str(node_id)+" "+str(conversation_sequence)+" "+str(branch))
        
        if (branch !=  'NULL' and branch != "0") and str(conversation_sequence) != 'None':
            if inner_node !='0' and inner_node !="" and inner_node !=None:
                print("11111111111111111111111111111111 ID")
                query_with_inner = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id = "+ str(node_id) +" AND sequence >"+ str(conversation_sequence)+" AND inner_node = "+str(inner_node)
                #query = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id > "+ str(node_id)
                response_data_node = db.engine.execute(query_with_inner)
                
                get_response_data = list(map(lambda x: PlayBookResponseModel.to_json(x), response_data_node))
                print(get_response_data)
                if get_response_data ==[]:
                    print("2222222222222222222222 ID")
                    query_next = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id > "+ str(node_id)
                    response_data_without_node = db.engine.execute(query_next)
                    return response_data_without_node
                else:
                    print("ELLLLLLLLLLLLSSSSSSSSSSSSSEEEEEEEEEEEEE")
                    query_with_inner = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id = "+ str(node_id) +" AND sequence >"+ str(conversation_sequence)+" AND inner_node = "+str(inner_node)

                    response_data_node = db.engine.execute(query_with_inner)
                    return response_data_node
            else:   
                print("333333333333333333333333 ID2222222233")
                query_sequence = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id = "+ str(node_id) +" AND sequence >"+ str(conversation_sequence)+" AND node_type = 3"
                response_data_sequence = db.engine.execute(query_sequence)
                
                res_sequnce = list(map(lambda x: PlayBookResponseModel.to_json(x), response_data_sequence))
                if res_sequnce == []:
                    print("44444444444444444444444 ID")
                    query_seq = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id > "+ str(node_id)
                    response_data_seq = db.engine.execute(query_seq)
                    return response_data_seq
                else:
                    return response_data_sequence
        else:
            print("5555555555555555555555555555555")
            queryss = "SELECT * from playbk_resp_det where playBook_id = " + str(playBook_id) +" AND node_id > "+ str(node_id)
            response_datass = db.engine.execute(queryss)
            return response_datass
         
    
    @classmethod
    def get_by_playbook_branch(cls, playBook_id,node,opt_id):
        print("PPPLLLLLLLLLAYYYYYYYYY_BRACNCHHHHHHHHHH")
        print(str(playBook_id)+" "+str(node)+" "+str(opt_id))
        return cls.query.filter_by(playbook_id = playBook_id,node_id=node,branch_id=opt_id).order_by(text("node_id asc")).all()
    @classmethod
    def clear_all_by_playbook_id(cls, playBook_id):
        queryOne = "DELETE FROM `playbk_resp_det` where playbook_id = "+ str(playBook_id)
        respons = db.engine.execute(queryOne)

        queryTwo = "DELETE FROM `playbk_reply_opt` where playbook_id = "+ str(playBook_id)
        respons = db.engine.execute(queryTwo)
        
        queryThree = "DELETE FROM `playBook_api_detail` where playbook_id = "+ str(playBook_id)
        respons = db.engine.execute(queryThree)

        queryFour = "DELETE FROM `api_header_detail` where playbook_id = "+ str(playBook_id)
        respons = db.engine.execute(queryFour)

        return respons
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

   

    @classmethod
    def to_json(cls, x):
            return {
                'playbk_resp_det_id':x.playbk_resp_det_id,
                'playbook_id':x.playbook_id,
                'node_id': x.node_id,
                'inner_node':x.inner_node,
                'node_type':x.node_type,
                "sequence":x.sequence,
                'text_response':x.text_response,
                'wait_status': x.wait_status,
                'jump_node':x.jump_node,
                'jump_playbook':x.jump_playbook,
                'branch_id': x.branch_id,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
   

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
