#!flask/bin/python

import os
import sys
from shared.db import db
import models
from flask.globals import request
from models.chatRoom import ChatRoomsModel
from models.chatRoomUser import ChatRoomUsersModel
from models.chatHistoryDetails import ChatHistoryDetailsModel
from models.chatHistory import ChatHistoryModel
from services.cron import subscriptionCron
from services.cron import ChatClearCron

from services import cron as ser
topdir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(topdir)
from datetime import datetime

from flask import Flask, jsonify, abort, make_response, session
from flask_restful import Api
from flask_cors import CORS
from datetime import timedelta
from flask_jwt_extended import JWTManager
from shared.route import init_routes
import resources.paymentResources 
from threading import Lock
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect,socketio



import time




APP_PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_PATH = os.path.join(APP_PATH, 'templates/')

# app = Flask(__name__, template_folder=template_dir)

app = Flask(__name__, template_folder=TEMPLATE_PATH)

app.debug = True

api = Api(app)

CORS(app, origins="*", allow_headers=["Content-Type", "Authorization", "Access-Control-Allow-Credentials"],supports_credentials=True)

app.config['SECRET_KEY'] = 'thisissecret'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/faq'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://cb_user:botroot@54.171.116.100/cb_faq'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(minutes=300)
app.config['JWT_REFRESH_TOKEN_EXPIRES'] = timedelta(minutes=300)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_RECORD_QUERIES'] = True
app.config['SECURITY_PASSWORD_SALT'] = "123456"

# development url
app.config['DOMAIN_URL'] = 'http://localhost:4200/#/'
app.config['SECURITY_PASSWORD_EXPIRES'] = 120
app.config['DOMAIN_URL_SCRIPT'] = 'http://183.82.33.232:5004'
app.config['DOMAIN_SERVER_URL'] = 'http://183.82.33.232:5002'
app.config['ENV'] = 'development'


# # live url
# app.config['DOMAIN_URL'] = 'https://admin.claritaz.com/#/'
# app.config['SECURITY_PASSWORD_EXPIRES'] = 120
# app.config['DOMAIN_URL_SCRIPT'] = 'https://bot.claritaz.com/widget-server/'
# app.config['DOMAIN_SERVER_URL'] = 'https://bot.claritaz.com/widget-server/'
# app.config['ENV'] = 'production'

app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'



jwt = JWTManager(app)
app.config['JWT_BLACKLIST_ENABLED'] = False
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

UPLOAD_FOLDER = './uploads/'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
async_mode = None

# socketio = SocketIO(app)

socketio = SocketIO(app, async_mode=async_mode)
thread = None
thread_lock = Lock()

app.app_context().push()



# cron   = CronTab()

# #add new cron job
# job  = cron.new(command=ChatClearCron())
# job.minute.every(1)




@app.before_first_request
def create_tables():
    print("BEFORE FIRST REQUEST WORKING")
    db.create_all()

@app.before_request
def make_session_permanent():
    app.permanent_session_lifetime = timedelta(minutes=1)


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return models.RevokedTokenModel.is_jti_blacklisted(jti)
    

with app.app_context():
    db.app = app
    db.init_app(app)






@app.errorhandler(400)
def bad_request(error):
    print("BAD REQUEST ERROR:")
    print(error)
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    print("INTERNAL SERVER ERROR:")
    print(error)
    return make_response(jsonify({'error': 'Not found'}), 404)
    

@app.errorhandler(500)
def internal_server_error(error):
    print("INTERNAL SERVER ERROR:")
    print(error)
    return make_response(jsonify({'error': str(error)}), 500)
    # return render_template('500.html', error=e), 500


init_routes(api)





@app.route("/")
def hello():    
    return "<h1 style='color:blue'>Hello Claribot</h1>"



def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        socketio.sleep(10)
        count += 1
        # socketio.emit('my_response',{'data': 'Server generated event', 'count': count},namespace='/test')


@socketio.on('my_event', namespace='/test')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1

    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']})


# @socketio.on('my_broadcast_event', namespace='/test')
# def test_broadcast_message(message):
#     session['receive_count'] = session.get('receive_count', 0) + 1
#     emit('my_response',
#          {'data': message['data'], 'count': session['receive_count']},
#          broadcast=True)


@socketio.on('join', namespace='/test')
def join(message):

    print("join")
    print(message)
    print("++++++++++++++++++++++++++++=")
    print(message['chats'])

    
    join_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1

    if(message['flag'] == 'client'):
        room_data = ChatRoomsModel.get_by_roomcode(message['room'])
        print("ROOM DATA")
        print(room_data.status)

        if room_data.status != 2:
            room_data.status = 1
            room_data.chats = str(message['chats'])
            room_data.save_to_db()
        

        emit('join_room',
        {
            'data': 'In rooms: ' + ', '.join(rooms()),
            'count': session['receive_count'],
            'room': message['room'],
             'accept':'initiate',
             'name':room_data.name,
             'room_status':room_data.status
        }, room=message['room'],broadcast=True)

    
@socketio.on('adminnewjoin', namespace='/test')
def adminnewjoin(message):

    print("adminnewjoin")
    print(message)
    
    join_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1

    emit('join_room',
    {
        'data': 'In rooms: ' + ', '.join(rooms()),
        'count': session['receive_count'],
        'room': message['room'],
         'accept':'true'
    }, room=message['room'],broadcast=True)


@socketio.on('adminjoin', namespace='/test')
def adminjoin(message):

    print("adminjoin")
    print(message)
    roomss = 0
    join_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    trim_id = message['room'].replace('room', '')
    rooms = ChatRoomUsersModel.get_by_roomid(trim_id)
    print(rooms)
    if rooms:
        #rooms = ChatRoomUsersModel.to_json(rooms)
        roomss = 1
    print(roomss)
    if(message['flag'] == 'admin' and roomss==0):
        room_data = ChatRoomsModel.get_by_roomcode(message['room'])    
        room_data.status = 2
        room_data.save_to_db()
        room_list = ChatRoomsModel.to_json(room_data)
        print("$$$$$$$$$$$$$$$$$44")
        print(room_list)
        print(room_list['room_id'])
        print("$$$$$$$$$$$$$$$$$44")

        new_data = ChatRoomUsersModel(
            user_id = message['user_id'],
            room_id = room_list['room_id']
        )
        new_data.save_to_db()

        emit('join_room',
        {
            'data': 'In rooms: ' + ', '.join(rooms()),
            'count': session['receive_count'],
            'room': message['room'],
            'accept':'admin_true',
            'name':room_data.name
        }, room=message['room'],broadcast=True)

    


@socketio.on('reject', namespace='/test')
def reject(message):
    
    print(message)
    leave_room(message['room'])

    room_data = ChatRoomsModel.get_by_roomcode(message['room'])   
    print(room_data.room_id)
    print("rooooooooooooooooooooooooom_daaaaaaaaaaaata") 
    room_data.chats=''
    room_data.status = 0
    room_data.save_to_db()
         
                   

    item = ChatRoomUsersModel.get_by_roomid(room_data.room_id)
    if item:
        item.delete_from_db()

    session['receive_count'] = session.get('receive_count', 0) - 1
    print("reject ROOM//////////////////////////////////////////////////////////////////////////////")

    emit('reject_room',
        {
            'data': 'In rooms: ' + ', '.join(rooms()),
            'count': session['receive_count']
        },room=message['room'])

@socketio.on('leave', namespace='/test')
def leave(message):
    
    if(message['flag'] == 'client'):
        room_data = ChatRoomsModel.get_by_roomcode(message['room'])   
        print(room_data.room_id)
        print("rooooooooooooooooooooooooom_daaaaaaaaaaaata") 
        room_data.status = 0
        room_data.chats=''
        room_data.save_to_db()
         
                   
        item = ChatRoomUsersModel.get_by_roomid(room_data.room_id)
        if item:
            item.delete_from_db()

        session['receive_count'] = session.get('receive_count', 0) - 1
        emit('leave_room',
            {
                'data': 'In rooms: ' + ', '.join(rooms()),
                'count': session['receive_count'],
                'room':message['room']
            },room=message['room'])


@socketio.on('close_room', namespace='/test')
def close(message):
    session['receive_count'] = session.get('receive_count', 0) + 1

    emit('my_response', {'data': 'Room ' + message['room'] + ' is closing.',
                         'count': session['receive_count']},
         room=message['room'])
    close_room(message['room'])


@socketio.on('updateClientChat', namespace='/test')
def update_client_chat_for_admin(message):
    
    print('update_client_chat_for_admin')
    print(message)
    session['receive_count'] = session.get('receive_count', 0) + 0
    room_data = ChatRoomsModel.get_by_roomcode(message['room'])

    room_data.chats = str(message['data']['chat'])
    room_data.save_to_db()
    # emit('my_response',
    #      {'data': message['data'], 'count': session['receive_count'],'room': message['room']},
    #      room=message['room'])
@socketio.on('my_room_event', namespace='/test')
def send_room_message(message):
    
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(background_thread)
    print('my_room_event')
    print('MMMMMMMMMMMMMMMMMMMMMMMMM')
    print(str(message['data']['chats']))
    
    session['receive_count'] = session.get('receive_count', 0) + 1

    room_data = ChatRoomsModel.get_by_roomcode(message['room'])
    print("ROOM DATA")
    print(message['data']['chats'])
    room_data.chats = str(message['data']['chats'])
    room_data.save_to_db()
    room_datass = ChatRoomsModel.get_by_roomcode(message['room'])
    room_datass = ChatRoomsModel.to_json(room_datass)

    current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    history_data = ChatHistoryModel.get_by_id(message['data']['chat_history_id'])
    history_data.updated_at = str(current_date)
    history_data.save_to_db()   

    if(message['data']['user'] == 'client'):

        chat_res_data = ChatHistoryDetailsModel(
            context_name = '',
            sentence =  message['data']['sentence'],
            response =  '',
            chat_res_id = message['data']['chat_history_id']
            )
    else:
        chat_res_data = ChatHistoryDetailsModel(
            context_name = '',
            response =  message['data']['sentence'],
            sentence =  '',
            chat_res_id = message['data']['chat_history_id']
            )
    chat_res_data.save_to_db()
   
        

    print("===================")
    print(room_datass)
    print("===================")

    emit('my_response',
         {'data': message['data'], 'count': session['receive_count'],'room': message['room']},
         room=message['room'])

@socketio.on('my_typing_event', namespace='/test')
def send_typing_message(message):

    print('my_typing_event')
    print(message)

    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('user_response_typing',
         {'data': "user typing"},
         room=message['room'],broadcast=True)



@socketio.on('disconnect_request', namespace='/test')
def disconnect_request():
    session['receive_count'] = session.get('receive_count', 0) + 1

    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']})
    disconnect()


@socketio.on('my_ping', namespace='/test')
def ping_pong():
    emit('my_pong')


@socketio.on('connect', namespace='/test')
def test_connect():
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(background_thread)

    emit('my_response', {'data': 'Connected', 'count': 0})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected', request.sid)

# ChatClearCron()
# schedule.every(1).minutes.do(ChatClearCron)



# while True: 
  
#     # Checks whether a scheduled task  
#     # is pending to run or not 
#     schedule.run_pending() 
#     time.sleep(1) 

# subscriptionCron()
# schedule.every(1).minutes.do(resources.paymentResources.SubscriptionCron)
# schedule.every().hour.do(paymentResources.SubscriptionCron)
# # schedule.every().day.at("10:30").do(job)

# while True:
#     schedule.run_pending()
#     time.sleep(1)
    
# if __name__ == '__main__':
#     app.run(debug=True)

if __name__ == '__main__':

    

    # handler = RotatingFileHandler('app.log', maxBytes=10000, backupCount=3)
    # logger = logging.getLogger('werkzeug')
    # logger.setLevel(logging.ERROR)
    # logger.addHandler(handler)

    # logger = logging.getLogger('werkzeug')
    # hdlr = logging.FileHandler('infoo.log')
    # formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    # hdlr.setFormatter(formatter)
    # logger.addHandler(hdlr) 
    # logger.setLevel(logging.ERROR)      
    # logger.error("error log message")

    # # initialize the log handler
    # logHandler = RotatingFileHandler('info.log', maxBytes=1000, backupCount=1)
    
    # # set the log handler level
    # logHandler.setLevel(logging.INFO)

    # # set the app logger level
    # app.logger.setLevel(logging.INFO)

    # app.logger.addHandler(logHandler)

    # app.logger.warning('testing warning log')
    # app.logger.error('testing error log')
    # app.logger.info('testing info log')
    

    # socketio.run(app)
    socketio.run(app, port=5002, host='0.0.0.0', debug=True)
    #app.run(port=2002, host='0.0.0.0',debug=True)




