import logging
from logging.handlers import RotatingFileHandler

def configLogs():
    logger = logging.getLogger('werkzeug')
    hdlr = logging.FileHandler('logs.log')
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.WARNING)   
    return logger

    


