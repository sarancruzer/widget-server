from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref



class RoleModel(db.Model):
    __tablename__ = 'role'

    role_id = Column('role_id',Integer, primary_key=True)
    role_name = Column('role_name', String(100), nullable = False) 
    user_id = Column('user_id', Integer, nullable = True) 
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    
  
    @classmethod
    def find_by_id(cls, role_id):        
        print(role_id)       
        return { 'role': list(map(lambda x: RoleModel.to_json(x), cls.query.filter_by(role_id=role_id).all())) }

    @classmethod
    def get_by_id(cls, role_id):        
        print(role_id)       
       
        return cls.query.filter_by(role_id=role_id).first()

    
    @classmethod
    def get_by_rolename(cls, rolename, usertoken):
        
        return {
            'role': list(map(lambda x: RoleModel.to_json(x), RoleModel.query.filter_by(role_name = rolename, user_token = usertoken, status = 0).all()))
        }

        
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    


    @classmethod
    def to_json(cls, x):
            return {
                'role_id':x.role_id,
                'role_name': x.role_name,
                'user_id':x.user_id,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
   
    @classmethod
    def return_all(cls, page, searchTerm):        
        page = int(page)
        
        db.session.commit()
        data = db.session.query(RoleModel).filter(RoleModel.role_name.like("%" + searchTerm + "%")).paginate(page=page, per_page=10)

        return data

    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

   
