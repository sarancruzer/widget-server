from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func


class PackageDetailsModel(db.Model):
    __tablename__ = 'package_details'

    package_detail_id = Column('package_detail_id',Integer, primary_key=True)
    
    package_id = Column('package_id',Integer, nullable = False)
    response_count = Column('response_count',Integer, nullable = True)
    chatbot_count = Column('chatbot_count',Integer, nullable = True)
    logs_count = Column('logs_count',Integer, nullable = True)
    bot_theme = Column('bot_theme',String(10), nullable = True)
    website_count = Column('website_count',Integer, nullable = True)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)

    
    
    @classmethod
    def get_by_packagename(cls, packagename):
        return cls.query.filter_by(package_name = packagename).first()

    
    @classmethod
    def get_by_package_detail_id(cls, package_detail_id):
        
        return {
            'package_details': list(map(lambda x: PackageDetailsModel.to_json(x), PackageDetailsModel.query.filter_by(package_detail_id = package_detail_id).all()))
        }


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

     
    @classmethod
    def to_json(cls, x):
            return {
                
                'package_detail_id':x.package_detail_id,
                'package_id': x.package_id,
                'response_count':x.response_count,
                'chatbot_count':x.chatbot_count,
                'logs_count': x.logs_count,
                'bot_theme': x.bot_theme,
                'website_count': x.website_count,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    
    @classmethod
    def get_by_id(cls, package_id):        

        return cls.query.filter_by(package_id=package_id).first()


    
    @classmethod
    def get_by_package_id(cls, package_id):        
        return { 'package': list(map(lambda x: PackageDetailsModel.to_json(x), cls.query.filter_by(package_id=package_id).all())) }


    @classmethod
    def return_all(cls):       
        return {'package_details': list(map(lambda x: PackageDetailsModel.to_json(x), PackageDetailsModel.query.all()))}
    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

