from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from models.website import WebsiteModel

import uuid

from flask import request
from shared.util import custom_paginate

import random
import string
from models.widgetLayout import WidgetLayoutModel
from models.package import PackageModel
from models.subscription import SubscriptionModel

from datetime import datetime
from dateutil.relativedelta import relativedelta
from shared import db
from models.packageDetails import PackageDetailsModel
from models.packResponseCount import PackResponseCountModel
from flask import jsonify





class Subscription(Resource):
    
    def get(self, id):

        item = WebsiteModel.find_by_id(id)

        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'website received',
                'data': item
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    
       

class SubscriptionList(Resource):  
    
    # @jwt_required
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('company_id', required=True, help='This field cannot be left blank')
        parser.add_argument('package_id', required=True, help='Must enter the website_name')
        
        data = parser.parse_args()
        company_id = data['company_id']
        package_id = data['package_id']
        
        package_data = PackageModel.find_by_id(package_id)

        date_after_month = datetime.today()+ relativedelta(months=package_data.package_duration)
        
        pack_start_at = datetime.today().strftime('%Y-%m-%d')
        pack_end_at = date_after_month.strftime('%Y-%m-%d')

        
        new_data = SubscriptionModel(
            company_id = company_id,
            package_id=package_id,
            pack_start_at = pack_start_at,
            pack_end_at = pack_end_at,
            is_expired = 0
        )

        try:
            new_data.save_to_db()
            
            return {
                    'status': 200,
                    'success': True,
                    'message': 'Successfull',
                    'data': SubscriptionModel.to_json(new_data)
                }, 200
        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the subscription',
            }, 400
            
class SubscriptionCompany(Resource):
    @jwt_required
    def get(self):

        current_user = get_jwt_identity()

        current_date = datetime.today().strftime('%Y-%m-%d')

        item = SubscriptionModel.get_by_company_id(current_user['company_id'])

        data = []
        if item is None:

            return {
                    'status':404,
                    'success': True,
                    'message': "Sorry you haven't subscribed for any package,Please subscribe for a package",
                    'data': data
                }, 404
        else:
            data = SubscriptionModel.to_json(item)

            package_id = data['package_id']
           
            pack_det = PackageDetailsModel.get_by_package_id(package_id)
            # pack_data = PackageDetailsModel.to_json(pack_det)


            pack_end_at = datetime.strptime(data['pack_end_at'], '%Y-%m-%d %H:%M:%S')

            pack_end_at = pack_end_at.strftime('%Y-%m-%d')

            if pack_end_at < current_date:
                data['is_expired'] = 1
                return {
                    'status':401,
                    'success': True,
                    'message': "Sorry your package has been expired,Please subscribe or renew your package",
                    'data': data
                }, 401

            else:
                
                return {
                    'status':200,
                    'success': True,
                    'message': "success",
                    'data': data,
                    'pack_data':pack_det
                }, 200
                
        

class SubscriptionDetails(Resource):
    
    def get(self):
        apikey = request.args.get('api_key', '')
        result = SubscriptionModel.get_package_details(apikey)

        if result:
            return {
                'status': 200,
                'success': True,
                'message': 'website received',
                'data': result
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
        
class SubscriptionPlans(Resource):

    @jwt_required    
    def post(self):     

        parser = reqparse.RequestParser()
        parser.add_argument('package_id', required=True, help='Must enter the website_name')   

        data = parser.parse_args()
        package_id = data['package_id']
        
        
        packageList = PackageModel.get_by_id_expire(package_id)
        package_data = PackageModel.to_json(packageList)
        if packageList is None:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
        else:
            #package_data = PackageModel.to_json(packageList)

            packageDetList = PackageDetailsModel.get_by_id(package_id)
            package_det_data = PackageDetailsModel.to_json(packageDetList)

            package_data['package_details'] = package_det_data

            if package_data:
                return {
                    'status': 200,
                    'success': True,
                    'message': 'package received',
                    'data': package_data
                }, 200

            else:
                return {
                    'status': 404,
                    'success': False,
                    'message': 'Item not found',
                }, 404
        

class SubscriptionRenewal(Resource):
    @jwt_required
    def post(self):

        current_user = get_jwt_identity()
        company_id = current_user['company_id']

        subscription_list = SubscriptionModel.get_by_company_id(company_id)
        subscription_data = SubscriptionModel.to_json(subscription_list)

        try:            
            subscription_list.is_expired = 1
            subscription_list.save_to_db()

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred renewed update the subscription',
            }, 400


        package_list = PackageModel.get_by_id(subscription_data['package_id'])
        package_data = PackageModel.to_json(package_list)


        date_after_month = datetime.today()+ relativedelta(months=package_data['package_duration'])
        
        pack_start_at = datetime.today().strftime('%Y-%m-%d')
        pack_end_at = date_after_month.strftime('%Y-%m-%d')

        new_data = SubscriptionModel(
            company_id = company_id,
            package_id=subscription_data['package_id'],
            pack_start_at = pack_start_at,
            pack_end_at = pack_end_at,
            is_expired = 0
        )

        try:
            new_data.save_to_db()
            return {
                    'status': 200,
                    'success': True,
                    'message': 'Successfully renewed! ',
                    'data': SubscriptionModel.to_json(new_data)
                }, 200
        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred renewed the subscription',
            }, 400


class CheckResponseCount(Resource):
    @jwt_required
    def post(self):
        
        current_user = get_jwt_identity()

        website_id = current_user['website_id']

        data = WebsiteModel.get_by_id(website_id)
        website_list = WebsiteModel.to_json(data)

        api_key = website_list['api_key']
        
        query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"' ORDER BY s.subs_id DESC"

        package_details = db.db.engine.execute(query)

        pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]


        RESPONSE_PERMIT_COUNT = pack_result['response_count']

        resultList = PackResponseCountModel.get_by_api_key(api_key)
        
        result = []
        if resultList:
            result = PackResponseCountModel.to_json(resultList)
            RESPONSE_CURRENT_COUNT = result['response_count']
            
        if (RESPONSE_PERMIT_COUNT > RESPONSE_CURRENT_COUNT):
            return {
                'status': 200,
                'success': True,
                'message': 'Your Package Response received ',
                'data': RESPONSE_CURRENT_COUNT
                }, 200
        else:
            return {
                'status': 400,
                'success': False,
                'message': 'Your Package response count limit is over please renew your package',
                'data': RESPONSE_CURRENT_COUNT
            }, 400