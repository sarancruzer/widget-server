from shared.db import db
from sqlalchemy import Column, Integer, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text


class ContextGroupDetailsModel(db.Model):
    __tablename__ = 'context_group_details'

    context_gp_det_id = Column(Integer, primary_key=True)
    context_gp_id = Column('context_gp_id', Integer, nullable = True)
    response = Column('response', Text, nullable = False)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer)

    
    @classmethod
    def get_by_contextname(cls, contextname):
        return cls.query.filter_by(context_name = contextname).first()

    @classmethod
    def get_by_context_gp_id(cls, context_gp_id):
        return {
            'context_group_details': list(map(lambda x: ContextGroupDetailsModel.to_json(x), ContextGroupDetailsModel.query.filter_by(context_gp_id = context_gp_id).all()))
        }
        
   
    @classmethod
    def get_by_context_gp_id_custom(cls, context_gp_id):

        def to_json(x):
            return x.response      

        return {
            'context_group_details': list(map(lambda x: to_json(x), ContextGroupDetailsModel.query.filter_by(context_gp_id=context_gp_id).all()))
        }
    


    @classmethod
    def to_json(cls, x):
            return {
                'context_gp_det_id': x.context_gp_det_id,
                'context_gp_id': x.context_gp_id,
                'response': x.response,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def get_by_id(cls, id):        
        return cls.query.filter_by(id=id).first()
    
    @classmethod
    def return_all(cls):       
        return {'context_group': list(map(lambda x: ContextGroupDetailsModel.to_json(x), ContextGroupDetailsModel.query.all()))}
    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @classmethod
    def get_by_custom_context_gp_id(cls, context_gp_id):
        
        return ContextGroupDetailsModel.query.filter_by(context_gp_id=context_gp_id).all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
    
    def delete_from_db(self):

        db.session.delete(self)
        db.session.commit()


