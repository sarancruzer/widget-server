from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from models.role import RoleModel

from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate

parser = reqparse.RequestParser()

parser.add_argument('role_id', required=False, help='This field cannot be left blank')
parser.add_argument('role_name', required=False, help='Must enter the role_name')




class Role(Resource):

    
    def get(self, role_id):


        item = RoleModel.find_by_id(role_id)
        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'role received',
                'data': item
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = RoleModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'Role has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
        new_data = RoleModel.get_by_id(id)

        if new_data is None:
            new_data = RoleModel(id, data['role_id'])
        else:
            # new_data.user_id = 1
            new_data.role_name = data['role_name']
            
            
        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'role has been updated',
                'data': RoleModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the role',
            }, 400
       

class RoleList(Resource):
    
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')

        result = RoleModel.return_all(page, searchTerm)
        items = result.items
        cus_paging = sqlalchemy_paginate(result)        
        
        data = list(map(lambda x: RoleModel.to_json(x), items))
        return {
                'status': 200,
                'success': True,
                'message': 'Role received',
                'data':data,
                'paginate': cus_paging
                }, 200
    
    @jwt_required
    def post(self):
        
        # if QuestionModel.find_by_name(name):
        #     return {'message': "An item with name '{}' already exists.".format(name)}, 400
        
        data = parser.parse_args()

        current_user = get_jwt_identity()
        # userId = current_user['id']
        
        # rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))
        
        new_data = RoleModel(
            role_id=data['role_id'],
            role_name=data['role_name'],
            user_id= current_user['user_id'],
            status=1
        )

        try:
            new_data.save_to_db()
            return {
                    'status': 200,
                    'success': True,
                    'message': 'role has been created ',
                    'data': RoleModel.to_json(new_data)
                }, 200
        except:
            return {'message':'An error occurred inserting the role '}, 500
        return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the role',
            }, 400
            

