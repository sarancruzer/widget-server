from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate
from models.company import CompanyModel

parser = reqparse.RequestParser()

parser.add_argument('company_name', required=False, help='This field cannot be left blank')
parser.add_argument('business_type', required=False, help='Must enter the package_name')
parser.add_argument('country', required=False, help='Must enter the package_price')
parser.add_argument('contact_person', required=False, help='Must enter the package_duration')
parser.add_argument('designation', required=False, help='Must enter the package_duration')
parser.add_argument('mobile', required=False, help='Must enter the package_duration')
parser.add_argument('email', required=False, help='Must enter the package_duration')
parser.add_argument('website', required=False, help='Must enter the package_duration')
parser.add_argument('website_count', required=False, help='Must enter the package_duration')
parser.add_argument('created_at', required=False, help='Must enter the package_duration')
parser.add_argument('updated_at', required=False, help='Must enter the package_duration')



class Company(Resource):

    
    def get(self, id):

        item = CompanyModel.find_by_company_id(id)

        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'Company received',
                'data': CompanyModel.to_json(item)
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = CompanyModel.find_by_company_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'package has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
        new_data = CompanyModel.find_by_company_id(id)

        if new_data is None:
            new_data = CompanyModel(id, data['company_id'])
        else:
            
            new_data.company_name = data['company_name']
            new_data.business_type = data['business_type']
            new_data.country = data['country']
            new_data.contact_person = data['contact_person']
            new_data.designation = data['designation']
            new_data.mobile = data['mobile']
            new_data.email = data['email']
            new_data.website = data['website']
            new_data.website_count = data['website_count']
            

        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'company has been updated',
                'data': CompanyModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the company',
            }, 400
       

class CompanyList(Resource):
    
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')

        result = CompanyModel.return_all(page, searchTerm)
        items = result.items
        cus_paging = sqlalchemy_paginate(result)        
        
        data = list(map(lambda x: CompanyModel.to_json(x), items))
        return {
                'status': 200,
                'success': True,
                'message': 'packages received',
                'data':data,
                'paginate': cus_paging
                }, 200
    
    @jwt_required

    def post(self):

        data = parser.parse_args()
        current_user = get_jwt_identity()

        # rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))
        
        new_company = CompanyModel(               
                company_name = data['company_name'],
                business_type = data['business_type'],
                country = data['country'],
                contact_person = data['contact_person'],
                designation = data['designation'],
                mobile = data['mobile'],
                email = data['email'],
                website = data['website'],
                website_count = data['website_count']                  
            )

        try:
            new_company.save_to_db()
            return {
                    'status': 200,
                    'success': True,
                    'message': 'company has been created ',
                    'data': CompanyModel.to_json(new_company)
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the company',
            }, 400


class CompanyProfile(Resource):    
    @jwt_required
    def get(self):
        current_user = get_jwt_identity()
        result = CompanyModel.find_by_company_id(current_user['company_id'])        
        data = CompanyModel.to_json(result)

        return {
                'status': 200,
                'success': True,
                'message': 'packages received',
                'data':data,
                }, 200
            

