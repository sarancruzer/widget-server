from flask_restful import Resource, reqparse
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)

from models.user import UserModel


from flask import request

import uuid

import werkzeug, os
from models.company import CompanyModel
from flask.wrappers import Response


parser = reqparse.RequestParser()


class UserLogin(Resource):
    def post(self):

        parser.add_argument('email', help = 'This field cannot be blank', required = False)
        parser.add_argument('password', help = 'This field cannot be blank', required = False)
        
        data = parser.parse_args()

        current_user = UserModel.find_by_email(data['email'])

        if not current_user:
            return {
                'status': 404,
                'success': False,                
                'message': 'User {} doesn\'t exist'.format(data['email']),
            },404

        current_company = CompanyModel.find_by_company_id(current_user.company_id)
        companyObj = CompanyModel.to_json(current_company)

        userObj = UserModel.to_json(current_user)
        userObj['company'] = companyObj
        
        
        if UserModel.verify_hash(data['password'], current_user.password):
            access_token = create_access_token(identity = userObj)
            refresh_token = create_refresh_token(identity = userObj)
            
            return {
                'status': 200,
                'success': True,
                'access_token': access_token,
                'refresh_token': refresh_token,
                'user': userObj,
                'message': 'Logged in as {}'.format(current_user.email),
            },200

        else:
            return {
                'status': 400,
                'success': False,                
                'message': 'Your password is wrong',
            },400

