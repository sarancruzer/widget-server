from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date,func
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from datetime import datetime
from models.packageDetails import PackageDetailsModel



class SubscriptionModel(db.Model):
    __tablename__ = 'subscription'

    subs_id = Column('subs_id',Integer, primary_key=True)
    payment_id = Column('payment_id',Integer, nullable = False)
    company_id = Column('company_id',Integer, nullable = False)
    package_id = Column('package_id',Integer, nullable = False)
    pack_start_at = Column('pack_start_at', DATETIME, nullable=False)
    pack_end_at = Column('pack_end_at', DATETIME, nullable=False) 
    is_expired = Column('is_expired',Integer, nullable = False)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True, server_default = "1")

    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)   
        db.session.rollback()     
        return 

        
    @classmethod
    def to_json(cls, x):
            return {
                
                'subs_id':x.subs_id,
                'company_id':x.company_id,
                'package_id':x.package_id,
                'pack_start_at':str(x.pack_start_at),
                'pack_end_at':str(x.pack_end_at),
                'is_expired':x.is_expired,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
     
    @classmethod
    def get_by_id(cls, subs_id):        
        print(subs_id)       
        
        return cls.query.filter_by(subs_id=subs_id).first()

    @classmethod
    def get_by_company_id(cls, company_id):   
        
        db.session.commit()
        db.session.flush()


        print(company_id)       
        current_date = datetime.today().strftime('%Y-%m-%d')
        print(current_date)              
        # query = db.session.query(SubscriptionModel).filter(SubscriptionModel.pack_end_at >= current_date).filter_by(company_id=company_id).first()
        print("&&&&&&&&&&&&&&&&&&")
        print("query")

        return cls.query.filter_by(company_id=company_id).order_by(SubscriptionModel.created_at.desc()).first()
    
    
    @classmethod
    def return_all(cls):       
        return {'subscription': list(map(lambda x: SubscriptionModel.to_json(x), SubscriptionModel.query.all()))}
    


    @classmethod
    def get_package_details(cls, apikey):

        db.session.commit()
        package_query = "SELECT pd.* FROM website as w LEFT JOIN subscription as s ON s.company_id=w.company_id LEFT JOIN package as p ON p.package_id = s.package_id LEFT JOIN package_details as pd ON pd.package_id = p.package_id WHERE w.api_key ='" + str(apikey) +"'"

        package_data = db.engine.execute(package_query)

        print("PACKAGE DATA")
        print(package_data)

        pack_result = list(map(lambda x: PackageDetailsModel.to_json(x), package_data))[0]

               
        return  pack_result 
    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @classmethod
    def get_company_subscription(cls, company_id):        
        
        return cls.query.filter_by(company_id=company_id).order_by("pack_start_at desc").first()

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
