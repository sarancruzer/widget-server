from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from models.website import WebsiteModel

import uuid

from flask import request
import random
import string
from models.widgetLayout import WidgetLayoutModel
import app
from shared import db
from models.packageDetails import PackageDetailsModel
from models.user import UserModel
import os
from logging.handlers import RotatingFileHandler

import tldextract
from models.company import CompanyModel
from models.userAssignWebsite import UserAssignWebsiteModel

parser = reqparse.RequestParser()

parser.add_argument('company_id', required=False, help='This field cannot be left blank')
parser.add_argument('website_name', required=False, help='Must enter the website_name')
parser.add_argument('website_id', required=False, help='This field cannot be left blank')


class Website(Resource):
    
    def get(self, id):
        item = WebsiteModel.find_by_id(id)
        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'website received',
                'data': item
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = WebsiteModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'website has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
        new_data = WebsiteModel.get_by_id(id)

        if new_data is None:
            new_data = WebsiteModel(id, data['website_id'])
        else:
            # new_data.user_id = 1
            new_data.website_name = data['website_name']

        try:
            
            new_data.save_to_db()

            return {
                'status': 200,
                'success': True,
                'message': 'website has been updated',
                'data': WebsiteModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the website',
            }, 400
       

class WebsiteList(Resource):
    
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)

        current_user = get_jwt_identity()
        company_id = current_user['company_id']

        result = WebsiteModel.return_all(page, current_user)
        
        data = list(map(lambda x: WebsiteModel.to_json(x), result))
        return {
                'status': 200,
                'success': True,
                'message': 'website received',
                'data':data
                }, 200
    
    @jwt_required
    def post(self):
        
        data = parser.parse_args()
        current_user = get_jwt_identity()

        company_list = CompanyModel.find_by_company_id(current_user['company_id'])
        company_data = CompanyModel.to_json(company_list)


        
        website_data = tldextract.extract(data['website_name'])
        website = website_data.domain + '.' + website_data.suffix

        CHATBOT_PERMIT_COUNT = 0
        
        CHATBOT_CURRENT_COUNT = 0

        query = "SELECT pd.* FROM subscription as s LEFT JOIN package as p ON p.package_id=s.package_id LEFT JOIN package_details as pd ON pd.package_id=p.package_id WHERE s.company_id=" + str(current_user['company_id']) + " AND s.is_expired = 0 order by s.created_at DESC"


        result = db.db.engine.execute(query)

        pack_det_list = result.fetchall()
       
        if pack_det_list:
            pack_data = list(map(lambda x: PackageDetailsModel.to_json(x), pack_det_list))[0]

            CHATBOT_PERMIT_COUNT = pack_data['website_count']
      
        website_count = WebsiteModel.get_count_by_company_id(current_user['company_id'])

        CHATBOT_CURRENT_COUNT = website_count

        if CHATBOT_CURRENT_COUNT >= CHATBOT_PERMIT_COUNT:
            return {
                    'status': 400,
                    'success': True,
                    'message': "Sorry You are not allowed to create a new website,pls contact our support team",
                }, 400

        
        if WebsiteModel.find_by_name(website):
            return {
                    'status': 409,
                    'success': True,
                    'message': "Website name '{}' already exists.".format(data['website_name']),
                }, 409
            

       
        # userId = current_user['id']
        
        # rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))

        apiKey = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(50)))

        scriptSourceCode = '<script type="text/javascript" src="' + app.app.config['DOMAIN_URL_SCRIPT'] + 'widgetScriptLoader.js" apiKey="' + apiKey + '"></script>'

       
        
        new_data = WebsiteModel(
            company_id = current_user['company_id'],
            website_name=website,
            website_code = str(uuid.uuid4().hex),
            # website_code = str(uuid.uuid4().fields[-1])[:10],
            script_source_code = scriptSourceCode,
            api_key = apiKey,
            created_by = current_user['user_id'],
            active_status=0
        )
        

        try:

            new_data.save_to_db()

            filepath = "customlogs/" + company_data['company_code'] + "/" + data['website_name']

            os.makedirs(filepath , exist_ok=True)  # Python>3.2

            logHandler = RotatingFileHandler(filepath + "/" + 'info.log', maxBytes=512000, backupCount=3)
            app.app.logger.removeHandler(logHandler)

            new_widget = WidgetLayoutModel(
                company_id = current_user['company_id'],
                website_id = new_data.website_id,
                logo = "file_path",  
                primary_color = "#11dad1",
                user_icon_path = "uploads/icons/user/user1.png",
                bot_icon_path = "uploads/icons/bot/bot1.png",
                bot_header_background = "#fff",
                bot_body_background = "#ddd",
                user_msg_foreground = "#000",         
                user_msg_background = "#eff5fb",
                bot_msg_foreground = "#000",         
                bot_msg_background = "#fff",         
                screen_open_default = 1,
                status = 1
            )

            new_widget.save_to_db()


            uaw_data = UserAssignWebsiteModel(
                company_id = current_user['company_id'],
                website_id = new_data.website_id,
                user_id = current_user['user_id']
                )

            uaw_data.save_to_db()


            # res = createDefaultWidgetComponents(current_user['company_id'],new_data.website_id)

            return {
                    'status': 200,
                    'success': True,
                    'message': 'website has been created '
                }, 200
            

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the websiteee',
            }, 400

class WebsitePublish(Resource):
    
    @jwt_required
    def post(self):

        parserr = reqparse.RequestParser()
        parserr.add_argument('active_status', required=False, help='This field cannot be left blank')
        parserr.add_argument('website_id', required=False, help='This field cannot be left blank')
       
        data = parserr.parse_args()
        website_id = data['website_id']   
        active_status = data['active_status']  

        if active_status == '1':
            active_status = 0
        
        if active_status == '0':
            active_status = 1

        try:
            website_list = WebsiteModel.get_by_id(website_id)
            website_list.active_status = active_status
            website_list.save_to_db()

            return {
                    'status': 200,
                    'success': True,
                    'message': 'website has been published '
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred publishing website',
            }, 400
        
           
           
class GetCrawlWebsite(Resource):
    
    @jwt_required
    def get(self):

        current_user = get_jwt_identity()
        website_id = current_user['website_id']

        result = WebsiteModel.get_by_website_id(website_id)
        # items = result.items
        
        data = list(map(lambda x: WebsiteModel.to_json(x), result))
        return {
                'status': 200,
                'success': True,
                'message': 'crawl website received',
                'data':data
                }, 200

