from shared.db import db
from sqlalchemy import Column, Integer, Text
from sqlalchemy.sql.sqltypes import DATETIME, String
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text


class ContextResponseModel(db.Model):
    __tablename__ = 'context_response'

    context_resp_id = Column('context_resp_id',Integer, primary_key=True)
    website_id = Column('website_id', Integer, nullable = False)
    context_token = Column('context_token', Text, nullable = False)
    context_gp_id = Column('context_gp_id', Integer, nullable = False)
    context_name = Column('context_name', String(100), nullable = False)
    sentence = Column('sentence', Text, nullable = False)
    response = Column('response', Text, nullable = True)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)

    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 

    @classmethod
    def get_by_contextname(cls, contextname, usertoken):

        return cls.query.filter_by(context_name = contextname, context_token = usertoken, status = 0).all()
        

    @classmethod
    def get_by_context_id(cls, context_res_id,user_token):
        
        return {
            'context_response': list(map(lambda x: ContextResponseModel.to_json(x), ContextResponseModel.query.filter_by(id = context_res_id, user_token = user_token, status = 0).all()))
        }

    @classmethod
    def to_json(cls, x):
            return {
                'context_resp_id': x.context_resp_id,
                'website_id':x.website_id,
                'context_token': x.context_token,
                'context_gp_id': x.context_gp_id,
                'context_name': x.context_name,
                'sentence': x.sentence,
                'response': x.response,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def get_by_id(cls, context_resp_id):        
        return cls.query.filter_by(context_resp_id=context_resp_id).first()
    
    @classmethod
    def return_all(cls):       
        return {'context_group': list(map(lambda x: ContextResponseModel.to_json(x), ContextResponseModel.query.all()))}
    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

 
