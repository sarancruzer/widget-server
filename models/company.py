from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey



class CompanyModel(db.Model):
    __tablename__ = 'company'

    company_id = Column(Integer, primary_key=True)
    company_code = Column('company_code', Text, nullable = False)
    company_name = Column('company_name', String(100), nullable = False)
    business_type = Column('business_type', String(100), nullable = False)
    country = Column('country', String(200), nullable = False)
    contact_person = Column('contact_person', String(100), nullable = False)
    designation = Column('designation', String(100), nullable = False)
    mobile = Column('mobile', String(20), nullable = True)
    email = Column('email', String(100), nullable = True)    
    website = Column('website', Text, nullable = True)        
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True,server_default=text('1'))


    @classmethod
    def find_by_company_id(cls, company_id):
        return cls.query.filter_by(company_id = company_id).first()

    
    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
        db.session.flush()
        return
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def to_json(cls, x):
            return {
                'company_id': x.company_id,
                'company_name': x.company_name,
                'company_code': x.company_code,
                'business_type': x.business_type,
                'country':x.country,
                'contact_person': x.contact_person,
                'designation': x.designation,
                'mobile':x.mobile,
                'email':x.email,
                'website':x.website,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at)
            }
    

    @classmethod
    def return_all(cls, page, searchTerm):        
        page = int(page)
        
        db.session.commit()
        data = db.session.query(CompanyModel).filter(CompanyModel.email.like("%" + searchTerm + "%")).paginate(page=page, per_page=10)

        return data

    @classmethod
    def get_all(cls):        
        
        data = db.session.query(CompanyModel).all()

        return data   

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}


