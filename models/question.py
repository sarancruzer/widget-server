from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
import json
import os
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.orm import eagerload
from flask_sqlalchemy import Pagination
from models.user import UserModel
from models.website import WebsiteModel
from models.company import CompanyModel
import csv
import shutil
import app
from faq_model.deeppavlovResource.train.faq.tfidf_logreg_en_faq import train as faq_train


class QuestionModel(db.Model):
    
    __tablename__ = 'question'

    question_id = Column(Integer, primary_key=True)
    user_id = Column('user_id', Integer)
    website_id = Column('website_id', Integer)
    category_id = Column('category_id',Integer)
    playbook_id= Column('playbook_id', Integer)
    class_name = Column('class_name', String(10))
    sentence = Column('sentence', Text)
    response = Column('response', Text)
    context = Column('context', String(100))
    image_link = Column('image_link', Text)
    href_link = Column('href_link', Text)
    audio_link = Column('audio_link', Text)
    video_link = Column('video_link', Text)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer)
    publish_status = Column('publish_status', Integer)
    conversation_status = Column('conversation_status', String(10))

    
    
    
   
    @classmethod
    def find_by_id(cls, id):        

        db.session.commit()
        query = "SELECT q.*, c.category_name,c.category_id FROM question as q LEFT JOIN category as c ON c.category_id = q.category_id  where q.question_id = " + id

        package_data = db.engine.execute(query)

        pack_result = list(map(lambda x: QuestionModel.to_json(x), package_data))[0]
               
        return  pack_result 

    @classmethod
    def find_by_class_val(cls,class_name ):        
        return  cls.query.filter_by(class_name=class_name).count()

    @classmethod
    def get_by_id(cls, id):        

        return cls.query.filter_by(question_id=id).first()
    
    @classmethod
    def get_by_playbookid(cls, id):        

        return cls.query.filter_by(playbook_id=id).first()

    @classmethod
    def get_by_classname(cls, class_name, website_id):        


        return cls.query.filter_by(class_name=class_name, website_id=website_id).first()

    @classmethod
    def find_by_name(cls, name):
        return {
            'questions': list(map(lambda x: QuestionModel.to_json(x), QuestionModel.query.all()))
            }
    
    @classmethod
    def check_question_changes(cls, website_id):
        db.session.rollback()
        return cls.query.filter_by(website_id=website_id,publish_status=1).first()

    @classmethod
    def get_by_categoryid(cls, category_id):        

        return cls.query.filter_by(category_id=category_id).first()  

    @classmethod
    def get_by_question_count(cls, website_id):
        return cls.query.filter_by(website_id = website_id).count()



    def save_to_db(self):  
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
        db.session.flush(self)
   
    @classmethod
    def to_json(cls, x):
            return {
                'question_id': x.question_id,
                'user_id': x.user_id,
                'website_id': x.website_id,
                'class_name': x.class_name,
                'sentence': x.sentence,
                #'category_name':x.category_name,
                'category_id': x.category_id,
                #'playBook_name':x.playBook_name,
                'playbook_id':x.playbook_id,
                'response':x.response,
                'context':x.context,
                'image_link':x.image_link,
                'href_link':x.href_link,
                'audio_link':x.audio_link,
                'video_link':x.video_link,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status,
                'publish_status':x.publish_status,
                'conversation_status':x.conversation_status

            }
    @classmethod
    def cust_to_json(cls, x):
            return {
                'question_id': x.question_id,
                'user_id': x.user_id,
                'website_id': x.website_id,
                'class_name': x.class_name,
                'sentence': x.sentence,
                #'category_name':x.category_name,
                'category_id': x.category_id,
                'playBook_name':x.playBook_name,
                'playbook_id':x.playbook_id,
                'response':x.response,
                'context':x.context,
                'image_link':x.image_link,
                'href_link':x.href_link,
                'audio_link':x.audio_link,
                'video_link':x.video_link,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status,
                'publish_status':x.publish_status,
                'conversation_status':x.conversation_status

            }

    @classmethod
    def return_all(cls, page, searchTerm,website_id,category_id,status):               
        page = int(page)

        current_user = get_jwt_identity()   
      
        userId = current_user['user_id']
                
        db.session.commit()

        if status == "1":
         
            query = "SELECT q.*, c.category_name,c.category_id FROM question as q LEFT JOIN category as c ON c.category_id = q.category_id  where q.website_id = " + str(website_id) + " and (q.status = 1 OR q.status = 3) and (q.playbook_id = 0 OR q.playbook_id = 'NULL')" 
        elif status == "3":    
            query = "SELECT q.*,pm.playBook_name, c.category_name,c.category_id FROM question as q LEFT JOIN category as c ON c.category_id = q.category_id LEFT JOIN playBook_master as pm ON pm.playbook_id = q.playbook_id  where q.website_id = " + str(website_id) + " and (q.playbook_id != 0 AND q.playbook_id != 'NULL')" 
        else:

            query = "SELECT q.*, c.category_name,c.category_id FROM question as q LEFT JOIN category as c ON c.category_id = q.category_id  where q.website_id = " + str(website_id) + " and q.status = 2"

        
        if category_id:
            query = query + " AND q.category_id = " + str(category_id)

        if searchTerm:
            # query = query + " AND q.sentence LIKE '%(" + str(searchTerm) + ")s'"
            query = query + " AND (q.sentence LIKE :search_term OR q.response LIKE :search_term)"
        query = query + " ORDER BY q.question_id DESC"
        print(query)
        print("######################################")

        params = { 'search_term' : '%'+ str(searchTerm) + '%'}

        return  query, params

    
    
    
    @classmethod
    def get_by_name(cls, class_name):

        def to_json(x):
            return {
                'id': x.id,
                'user_id': x.user_id,
                'class_name': x.class_name,
                'sentence': x.sentence,
                'category': x.category,
                'response': x.response,
                'image_link': x.image_link,
                'href_link': x.href_link,
                'audio_link': x.audio_link,
                'video_link': x.video_link,
            }

        return {'questions': list(map(lambda x: to_json(x), QuestionModel.query.filter_by(class_name=class_name)))}

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    @classmethod
    def publish_all(cls,data, current_user):

        sample = []
        sam = []
        
        user_id = current_user['user_id']
        website_id = data['website_id']
        company_id = current_user['company_id']

        website_data = WebsiteModel.query.filter_by(website_id=website_id).first()
        company_data = CompanyModel.query.filter_by(company_id=company_id).first()
        
        folder_name = company_data.company_code

        file_name = website_data.website_code

        data = QuestionModel.query.filter_by(website_id=website_id).filter(QuestionModel.status != 2).all()

        directory = "jsons/" + folder_name + "/"

        if not os.path.exists(directory):
            os.makedirs(directory)
        
        for value in data:
            # if value.conversation_status == '1':
            #     value.response="context_sentence"

            sam.append({ "class": value.class_name, "responses":value.response, "sentence":value.sentence })  
            pass                       
            
        filepath = directory + file_name + '.json'           

        with open(filepath, mode='w') as feedsjson:
            json.dump(sam, feedsjson, indent=4, sort_keys=True)
        


        #Hide For CSV Start
        new_data = UserModel.get_by_id(user_id)

        query = "UPDATE question SET publish_status = 0 WHERE website_id = " + website_id
        ress = db.engine.execute(query)        

        website_data.trained_json_path = filepath
        #Hide For CSV End
        # directory = "faq_config_json/" + company_data.company_code + "/"

        # if not os.path.exists(directory):
        #     os.makedirs(directory)

        # sam.append({ "faq_algorithm": "deeppavlov"})  
                
        # filepath = directory + website_data.website_code + '.json'           

        # with open(filepath, mode='w') as feedsjson:
        #     json.dump(sam, feedsjson, indent=4, sort_keys=True)
                


        
        faq_algorithm = 'faq_config_json/'+folder_name+'/'+file_name+'.json'
        with open(faq_algorithm, 'r+') as f:
            algo_data = json.load(f)
            algorithm_type = algo_data['config'][0]['faq_algorithm']
                    #For CSV
            #algorithm_type='deeppavlov'
        print("AAAAAAAAALFO TYPE")
        print(algorithm_type)
        if algorithm_type=='deeppavlov':
            directoryCsv = "csv/" + folder_name + "/"
            if not os.path.exists(directoryCsv):
                os.makedirs(directoryCsv)
            
            
            filepathCsv = directoryCsv + file_name + '.csv' 
            with open(filepathCsv, mode='w') as csvFile:
                fields = ['Question', 'Answer']
                writer = csv.DictWriter(csvFile, fieldnames=fields)
                writer.writeheader()
                for value in data:
                    print("Class NAMEE")
                    print(value.class_name)
                    # if value.conversation_status == '1':
                    #     value.response="context_sentence"
                    sen = value.sentence.replace(',', '')
                    res = value.response.replace(',', '')
                    writer.writerow({"Question":sen,"Answer":value.class_name })  
                    #writer.writerow(data)
            csvFile.close()

            new_data = UserModel.get_by_id(user_id)
            


            query = "UPDATE question SET publish_status = 0 WHERE website_id = " + website_id
            ress = db.engine.execute(query)        

            website_data.trained_json_path = filepathCsv 

            #dest = '/var/www/html/widget-dev/widget-server/deeppavlovResource/configs/faq/'+company_data.company_code+'/'+website_data.website_code
            

            if app.app.config['ENV']=='development':
                dest = '/var/www/html/widget-dev/widget-server/faq_model/deeppavlovResource/configs/'+company_data.company_code+'/'+website_data.website_code
              

            elif app.app.config['ENV']=='production':
                dest = '/var/www/html/widget-server/faq_model/deeppavlovResource/configs/'+company_data.company_code+'/'+website_data.website_code                
            
            if not os.path.exists(dest):
                os.makedirs(dest)
            shutil.copy('botvenv/lib/python3.6/site-packages/deeppavlov/configs/faq/tfidf_logreg_en_faq.json',dest)
            conf_path = dest+'/tfidf_logreg_en_faq.json'

        
            if app.app.config['ENV']=='development':
                json_path = '/var/www/html/widget-dev/widget-server/'+website_data.trained_json_path
            elif app.app.config['ENV']=='production':
                json_path = '/var/www/html/widget-server/'+website_data.trained_json_path
            print("IIIIIIIIIIINNNNNNNNNN JJJJSSSSSSSSSOOOONNNN")
            print(json_path)
            print("CCCCCCCCCOOOOOONF PATH")
            print(conf_path)
            with open(conf_path, 'r+') as f:
                data = json.load(f)
                data['dataset_reader']['data_url']=json_path

                data['metadata']['variables']['ROOT_PATH'] = "~/.deeppavlov/"+company_data.company_code+'/'+website_data.website_code
            #faq = train_model(configs.faq.tfidf_logreg_en_faq) 
            os.remove(conf_path)
            with open(conf_path, 'w') as f:
                json.dump(data, f, indent=4)
        
            #faq_result = faq_train.FaqTrainService.faq_train()
            faq_result = faq_train.FaqTrainService.faq_train(dest+'/tfidf_logreg_en_faq.json')

        try:
            website_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'question has been published',
                'data': UserModel.to_json(new_data)
            }, 200
        except:
        # except Exception as e: print(e)
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred publishing the questions',
            }, 400

