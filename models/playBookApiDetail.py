from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref



class PlayBookApiDeatailsModel(db.Model):
    __tablename__ = 'playBook_api_detail'

    playBook_api_detail_id = Column('playBook_api_detail_id',Integer, primary_key=True,autoincrement=True)
    playbook_id = Column('playbook_id',Integer, nullable = False)
    node_id = Column('node_id', Integer, nullable = False) 
    branch_id = Column('branch_id', Integer, nullable = False) 
    api_url = Column('api_url',String(255), nullable = False)
    option_id = Column('option_id', Integer, nullable = False) 
    api_method = Column('api_method',String(100), nullable = False)
    api_response_type=Column('api_response_type',String(100), nullable = False)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return

    @classmethod
    def find_last_added_record(cls, playBook_id,response,node_id):
        return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id,option_value=response).first()

    @classmethod
    def find_by_node_id(cls, playBook_id):
        return cls.query.filter_by(playbook_id = playBook_id).all()
    @classmethod
    def find_by_palyBook_node_id(cls, playBook_id,node_id):
        return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id).all()    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def to_json(cls, x):
            return {
                'playBook_api_detail_id':x.playBook_api_detail_id,
                'playbook_id':x.playbook_id,
                'node_id': x.node_id,
                'branch_id':x.branch_id,
                "option_id":x.option_id,
                "api_url":x.api_url,
                'api_method':x.api_method,
                'api_response_type':x.api_response_type,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
   

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
