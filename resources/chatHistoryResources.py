from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

import random
import string
from shared.util import custom_paginate

from flask import request
from shared.util import custom_paginate
import json
from flask.json import jsonify
from models.chatbotResponse import ChatbotResponseModel
from shared import db
import app
from models.website import WebsiteModel
from models.chatHistory import ChatHistoryModel
from models.chatHistoryDetails import ChatHistoryDetailsModel
from models.subscription import SubscriptionModel
from models.packageDetails import PackageDetailsModel


parser = reqparse.RequestParser()

parser.add_argument('website_id', required=False, help='Must enter the website id')


      

class ChatHistoryList(Resource):
    
    @jwt_required

    def get(self): 
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')
        website_id = request.args.get('website_id', '')
      
        current_user = get_jwt_identity()
        print("888888888888888888888888888888888")
        print(current_user)
        company_id = current_user['company_id']
        subscription = SubscriptionModel.get_company_subscription(company_id)
        sub = SubscriptionModel.to_json(subscription)

        package_id = sub['package_id']

        package_det = PackageDetailsModel.get_by_id(package_id)
        pack_details = PackageDetailsModel.to_json(package_det)
        query, params = ChatHistoryModel.return_all(page, searchTerm,website_id,pack_details['logs_count'])


        per_page = 10

        result, cus_paging = custom_paginate(query, page, per_page,params)


        data = list(map(lambda x: ChatHistoryModel.to_json(x), result))


        return {
                'status': 200,
                'success': True,
                'message': 'chathistory received',
                'data':data,
                'paginate': cus_paging
                }, 200

class ChatHistoryCreate(Resource):
    
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('api_key', required=False, help='Must enter the website_id')   
        parser.add_argument('name', required=False, help='Must enter the website_id')   
        parser.add_argument('email', required=False, help='Must enter the website_id')   
        parser.add_argument('user_token', required=False, help='Must enter the website_id')   

         
        data = parser.parse_args()
        websiteList = WebsiteModel.get_by_apikey(data['api_key'])
        website_data = WebsiteModel.to_json(websiteList)


        history_data = ChatHistoryModel(
            company_id=website_data['company_id'],
            website_id = website_data['website_id'],
            user_token=data['user_token'],
            name= data['name'],
            email=data['email'],
            status=1,
        )
        try:
            history_data.save_to_db()
            return {
                    'status': 200,
                    'success': True,
                    'message': 'chat history has been created ',
                    'data': ChatHistoryModel.to_json(history_data)
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the chat',
            }, 400
                                
class ChatHistoryDetail(Resource):
    
    @jwt_required
    def get(self,id):
        query = ChatHistoryDetailsModel.get_by_chat_resp_id(id)

        data = list(map(lambda x: ChatHistoryDetailsModel.to_json(x), query))


        return {
                'status': 200,
                'success': True,
                'message': 'chathistory received',
                'data':data,
                }, 200    


