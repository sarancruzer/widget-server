from shared.db import db
from sqlalchemy import Table, Column, Float, Integer, String, Date, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text


class ChatRoomsModel(db.Model):
    __tablename__ = 'chat_rooms'

    room_id = Column(Integer, primary_key=True)
    company_id = Column('company_id', Integer)
    website_id = Column('website_id', Integer)
    room_name = Column('room_name', String(100), nullable = True)
    room_code = Column('room_code', String(100), nullable = False)
    chats = Column('chats', Text,nullable = True)
    name = Column('name', Text, nullable = False)
    email = Column('email', Text, nullable = False)
    chat_history_id = Column('chat_history_id', Integer)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True,server_default=text('0'))

  
    @classmethod
    def get_all_rooms(cls, website_id):
        return cls.query.filter_by(website_id = website_id, status = 0).first()

    @classmethod
    def get_all_joined_rooms(cls, website_id):
        return cls.query.filter_by(website_id = website_id, status = 2).all()

    @classmethod
    def find_by_id(cls, chat_room_id):        
        return cls.query.filter_by(room_id=chat_room_id).first() 

    @classmethod
    def get_active_rooms(cls, website_id, user_id):

        query = "SELECT cr.*,cru.user_id FROM chat_rooms as cr LEFT JOIN chat_rooms_users as cru ON cru.room_id = cr.room_id WHERE ( cr.website_id=" + website_id + " and cr.status != 0 ) or cru.user_id=" + str(user_id)



        res = db.engine.execute(query) 
        return res

 
    @classmethod
    def get_available_rooms(cls, website_id):
        return cls.query.filter_by(website_id=website_id, status = 0).first()

    @classmethod
    def get_by_roomcode(cls, room_code):
        return cls.query.filter_by(room_code = room_code).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush()
        return
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def to_json(cls, x):
            return {
                'room_id': x.room_id,
                'company_id': x.company_id,
                'website_id': x.website_id,
                'room_name': x.room_name,
                'room_code': x.room_code,
                'name': x.name,
                'email': x.email,
                'chat_history_id': x.chat_history_id,
                'chats':x.chats,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def to_customjson(cls, x):
            return {
                'room_id': x.room_id,
                'user_id': x.user_id,
                'company_id': x.company_id,
                'website_id': x.website_id,
                'room_name': x.room_name,
                'room_code': x.room_code,
                'chats':x.chats,
                'name': x.name,
                'email': x.email,
                'chat_history_id': x.chat_history_id,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    

    @classmethod
    def return_all(cls, page, searchTerm):        
        page = int(page)
        
        db.session.commit()
        data = db.session.query(ChatRoomsModel).filter(ChatRoomsModel.email.like("%" + searchTerm + "%")).paginate(page=page, per_page=10)

        return data

    @classmethod
    def get_all(cls):                
        data = db.session.query(ChatRoomsModel).all()
        return data   

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    
