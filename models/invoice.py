from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import backref, relationship, joinedload



class InvoiceModel(db.Model):

    __tablename__ = 'invoice'

    invoice_id = Column(Integer, primary_key=True)
    invoice_no = Column("invoice_no", Integer,)
    company_id = Column("company_id", Integer,nullable = False)
    package_id = Column("package_id", Integer, nullable = False)
    subscription_type = Column('subscription_type', Integer, nullable = False)
    gross_amount = Column('gross_amount', String(100), nullable = False)
    tax_gst = Column('tax_gst', String(100), nullable = False)
    net_amount = Column('net_amount', String(100), nullable = False)
    invoice_status = Column('invoice_status', Integer, nullable = False)
    cron_status = Column('cron_status', Integer, nullable = True) 
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True)
  
    
    @classmethod
    def get_by_invoiceid(cls, id):        
        return cls.query.filter_by(invoice_id=id).first()

    @classmethod
    def get_by_company_id(cls, id):        
        return cls.query.filter_by(company_id=id).order_by("invoice_id desc").first()

    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
        db.session.flush()

        return
    
    def delete_from_db(self):

        db.session.delete(self)
        db.session.commit()

    @classmethod
    def to_json(cls, x):
            return {
                'invoice_id': x.invoice_id,
                'invoice_no': x.invoice_no,
                'company_id':x.company_id,
                'package_id':x.package_id,
                'package_name':x.package_name,
                'subscription_type': x.subscription_type,
                'gross_amount': x.gross_amount,
                'tax_gst': x.tax_gst,
                'net_amount':x.net_amount,
                'invoice_status': x.invoice_status,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at)
            }
        
    @classmethod
    def to_customjson(cls, x):
            return {
                'invoice_id': x.invoice_id,
                'invoice_no': x.invoice_no,
                'company_id':x.company_id,
                'package_id':x.package_id,
                'subscription_type': x.subscription_type,
                'gross_amount': x.gross_amount,
                'tax_gst': x.tax_gst,
                'net_amount':x.net_amount,
                'invoice_status': x.invoice_status,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at)
            }


    @classmethod
    def return_all(cls, page, searchTerm):               
        page = int(page)

        current_user = get_jwt_identity()

        companyId = current_user['company_id']
                
        db.session.commit()
        
        query = "SELECT i.*, p.package_name FROM invoice as i LEFT JOIN package as p ON p.package_id = i.package_id  where i.company_id = " + str(companyId)
      
        if searchTerm:
            query = query + " AND (i.invoice_no LIKE :search_term OR p.package_name LIKE :search_term)"
        
        query = query + " ORDER BY i.invoice_id DESC"

        params = { 'search_term' : '%'+ str(searchTerm) + '%'}

        return  query, params
