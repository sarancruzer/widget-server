from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import backref, relationship, joinedload



class PaymentModel(db.Model):

    __tablename__ = 'payments'

    payment_id = Column(Integer, primary_key=True)
    company_id = Column("company_id", Integer,nullable = False)
    invoice_id = Column("invoice_id", Integer,nullable = False)
    transaction_id = Column("transaction_id", Integer,nullable = False)
    payment_method = Column("payment_method", String(100), nullable = False)
    payment_status = Column('payment_status', Integer, nullable = False)
    gross_amount = Column('gross_amount', String(100), nullable = False)
    tax_gst = Column('tax_gst', String(100), nullable = False)
    net_amount = Column('net_amount', String(100), nullable = False)
    currency_code = Column('currency_code', String(100), nullable = False)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True)


    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
        db.session.flush(self)

        return
    
    def delete_from_db(self):

        db.session.delete(self)
        db.session.commit()

    @classmethod
    def to_json(cls, x):
            return {
                'payment_id': x.payment_id,
                'invoice_id': x.invoice_id,
                'invoice_no': x.invoice_no,
                'company_id':x.company_id,
                'transaction_id':x.transaction_id,
                'payment_method':x.payment_method,
                'payment_status': x.payment_status,
                'gross_amount': x.gross_amount,
                'tax_gst': x.tax_gst,
                'net_amount':x.net_amount,
                'currency_code': x.currency_code,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at)
            }
    
    
    @classmethod
    def return_all(cls, page, searchTerm):               
        page = int(page)

        current_user = get_jwt_identity()

        companyId = current_user['company_id']
                
        db.session.commit()

        
        query = "SELECT p.*, i.invoice_no FROM payments as p LEFT JOIN invoice as i ON i.invoice_id = p.invoice_id  where p.company_id = " + str(companyId)
      
        if searchTerm:
            query = query + " AND (i.invoice_no LIKE :search_term OR p.transaction_id LIKE :search_term)"
            
        
        
        query = query + " ORDER BY p.payment_id DESC"

        params = { 'search_term' : '%'+ str(searchTerm) + '%'}

        return  query, params


