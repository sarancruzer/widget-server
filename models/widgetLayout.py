from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey



class WidgetLayoutModel(db.Model):
    __tablename__ = 'widget_layout'

    widget_id = Column('widget_id',Integer, primary_key=True)
    company_id = Column("company_id", Integer, nullable = False)    
    website_id = Column('website_id', Integer, nullable = True) 
    logo = Column('logo', Text, nullable = False) 
    primary_color = Column('primary_color', String(100), nullable = False)     
    user_icon_path = Column('user_icon_path', Text, nullable = True)    
    bot_icon_path = Column('bot_icon_path', Text, nullable = True)   
    bot_header_background = Column('bot_header_background', Text, nullable = True)    
    bot_body_background = Column('bot_body_background', Text, nullable = True)        
    user_msg_foreground = Column('user_msg_foreground', Text, nullable = True)    
    user_msg_background = Column('user_msg_background', Text, nullable = True)        
    bot_msg_foreground = Column('bot_msg_foreground', Text, nullable = True)    
    bot_msg_background = Column('bot_msg_background', Text, nullable = True)        
    screen_open_default	 = Column('screen_open_default', Integer, nullable = True) 
       
    bot_heading_text	 = Column('bot_heading_text', Integer, nullable = True) 

    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    modified_by = Column('modified_by', Integer, nullable = True)
    


    @classmethod
    def find_by_website_id(cls, website_id):        
        return cls.query.filter_by(website_id=website_id).first()
        
        
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush()
        db.session.rollback()
        return 


    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def to_json(cls, x):
            return {
                'widget_id':x.widget_id,
                'company_id':x.company_id,
                'website_id': x.website_id,
                'logo': x.logo,
                'primary_color': x.primary_color,
                'user_icon_path': x.user_icon_path,
                'bot_icon_path': x.bot_icon_path,
                'bot_heading_text':x.bot_heading_text,
                'bot_header_background': x.bot_header_background,
                'bot_body_background': x.bot_body_background,
                'user_msg_foreground': x.user_msg_foreground,
                'user_msg_background': x.user_msg_background,
                'bot_msg_foreground': x.bot_msg_foreground,
                'bot_msg_background': x.bot_msg_background,
                'screen_open_default': x.screen_open_default,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

