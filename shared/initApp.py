
from flask import Flask, jsonify, abort, make_response

def initApp():
    app = Flask(__name__, static_url_path="")
    return app