from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from models.role import RoleModel

from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate
from models.invoice import InvoiceModel
from models.payment import PaymentModel
from models.subscription import SubscriptionModel

from datetime import datetime
from dateutil.relativedelta import relativedelta
from models.package import PackageModel
import random
import string
from models.company import CompanyModel
from shared import db
from models.packageDetails import PackageDetailsModel
from shared.mailer import packageSubscribedMail
from models.packResponseCount import PackResponseCountModel
from models.website import WebsiteModel

parser = reqparse.RequestParser()

parser.add_argument('role_id', required=False, help='This field cannot be left blank')
parser.add_argument('role_name', required=False, help='Must enter the role_name')




class Payment(Resource):

    
    def get(self, role_id):


        item = RoleModel.find_by_id(role_id)
        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'role received',
                'data': item
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = RoleModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'Role has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
        new_data = RoleModel.get_by_id(id)

        if new_data is None:
            new_data = RoleModel(id, data['role_id'])
        else:
            # new_data.user_id = 1
            new_data.role_name = data['role_name']
            
            
        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'role has been updated',
                'data': RoleModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the role',
            }, 400


class PaymentList(Resource):
    
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')

 
        query, params = PaymentModel.return_all(page, searchTerm)

        per_page = 10
        result, cus_paging = custom_paginate(query, page, per_page,params)
        data = list(map(lambda x: PaymentModel.to_json(x), result))

        return {
                'status': 200,
                'success': True,
                'message': 'transaction received',
                'data':data,
                'paginate': cus_paging
                }, 200
    
          

class InvoicePaymentCreation(Resource):
    
    @jwt_required
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('package_id', required=True, help='Must enter the package_id')
        parser.add_argument('amount', required=True, help='Must enter the amount')
        parser.add_argument('transaction_id', required=True, help='Must enter the transaction_id')
        parser.add_argument('payment_status', required=True, help='Must enter the payment_status')
        
        data = parser.parse_args()
       
        current_user = get_jwt_identity()

        
        rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))

        company_id = current_user['company_id']
        website_id = current_user['website_id']

        info_data = WebsiteModel.get_by_id(website_id)
        details_info = WebsiteModel.to_json(info_data)

        package_id = data['package_id']

        package_data = PackageModel.find_by_id(package_id)
        details_package = PackageModel.to_json(package_data)
        package_name = details_package['package_name']

        amount = data['amount']
        tax_gst = 18

        # net_amount = 118*18/100+18 = Rs 18
        tax_amount = int(amount)*tax_gst/100+tax_gst

        net_amount = int(amount)+int(tax_amount)

        try:            

            new_invoice = InvoiceModel(
                invoice_no=rand,
                company_id= current_user['company_id'],
                package_id=data['package_id'],
                subscription_type= 1,
                gross_amount=amount,
                tax_gst=tax_gst,
                net_amount=net_amount,
                invoice_status=1,
                cron_status=1,
                status=1
            )

            new_invoice.save_to_db()
            db.db.session.refresh(new_invoice)

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the invoice',
            }, 400

        
        new_payment = PaymentModel(
            invoice_id=new_invoice.invoice_id,
            company_id= current_user['company_id'],
            transaction_id= data['transaction_id'],
            payment_method="",
            payment_status=1,
            gross_amount=amount,
            tax_gst=tax_gst,
            net_amount=net_amount,
            currency_code='₹',
            status=1
        )

        try:
            # db.db.session.rollback()            
            new_payment.save_to_db()
           
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the payment',
            }, 400


        

        date_after_month = datetime.today()+ relativedelta(months=package_data.package_duration)
        
        pack_start_at = datetime.today().strftime('%Y-%m-%d')
        pack_end_at = date_after_month.strftime('%Y-%m-%d')

        
        new_subscription = SubscriptionModel(
            payment_id = new_payment.payment_id,
            company_id = company_id,
            package_id=package_id,
            pack_start_at = pack_start_at,
            pack_end_at = pack_end_at,
            is_expired = 0
        )
        
        apikey = details_info['api_key']
        data ="DELETE FROM pack_response_count WHERE api_key='"+ apikey +"'"
        result = db.db.engine.execute(data)

        new_pack_response_count = PackResponseCountModel(
            api_key = details_info['api_key'],
            response_count = 0,
            status = 1
            
        )
        
        new_pack_response_count.save_to_db()
        

        packageList = PackageModel.get_by_id(package_id)
        package_data = PackageModel.to_json(packageList)

        packageDetList = PackageDetailsModel.get_by_id(package_id)
        package_det_data = PackageDetailsModel.to_json(packageDetList)

        package_data['package_details'] = package_det_data

        packageSubscribedMail(current_user['email'],current_user['name'],"Package Subscribed", "welcome to claritaz bot!", package_data)

        try:
                         
            new_subscription.save_to_db()          
           
            return {
                    'status': 200,
                    'success': True,
                    'message': 'subscription has been created ',
                    'data': SubscriptionModel.to_json(new_subscription)
                }, 200

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the subscription',
            }, 400
            
class PaymentCreation(Resource):
    
    @jwt_required
    def put(self,id):
        
        parser = reqparse.RequestParser()
        parser.add_argument('package_id', required=True, help='Must enter the package_id')
        parser.add_argument('amount', required=True, help='Must enter the amount')
        parser.add_argument('transaction_id', required=True, help='Must enter the transaction_id')
        parser.add_argument('payment_status', required=True, help='Must enter the payment_status')
        
        data = parser.parse_args()

        current_user = get_jwt_identity()

        rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))

        company_id = current_user['company_id']
        package_id = data['package_id']

        package_data = PackageModel.find_by_id(package_id)
        
        invoice_data = InvoiceModel.get_by_invoiceid(id)
        invoice_data.invoice_status = 1
        invoice_data.cron_status = 1

        try:
            invoice_data.save_to_db()
            
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the invoice',
            }, 400

        
        new_payment = PaymentModel(
            invoice_id=id,
            company_id= current_user['company_id'],
            transaction_id= data['transaction_id'],
            payment_method="",
            payment_status=1,
            gross_amount=invoice_data.gross_amount,
            tax_gst=invoice_data.tax_gst,
            net_amount=invoice_data.net_amount,
            currency_code='₹',
            status=1
        )

        try:
            new_payment.save_to_db()          
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the payment',
            }, 400


        date_after_month = datetime.today()+ relativedelta(months=package_data.package_duration)
        
        pack_start_at = datetime.today().strftime('%Y-%m-%d')
        pack_end_at = date_after_month.strftime('%Y-%m-%d')

        new_subscription = SubscriptionModel(
            payment_id = new_payment.payment_id,
            company_id = company_id,
            package_id=package_id,
            pack_start_at = pack_start_at,
            pack_end_at = pack_end_at,
            is_expired = 0
        )
       

        try:
            new_subscription.save_to_db()
            return {
                    'status': 200,
                    'success': True,
                    'message': 'subscription has been created ',
                    'data': SubscriptionModel.to_json(new_subscription)
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the subscription',
            }, 400
            

class SubscriptionCron(Resource):
    
    def post(self):
        print("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT")
        # company_list = CompanyModel.get_all()
        # company_data = list(map(lambda x: CompanyModel.to_json(x), company_list))
        # # company_data = CompanyModel.to_json(company_list)

        # current_date = datetime.today()
        # # current_date = current_date.strftime('%Y-%m-%d')

        


        # for value in company_data:
          
        #     item = SubscriptionModel.get_by_company_id(value['company_id'])
        #     subs_data = []
        #     if item:
        #         subs_data = SubscriptionModel.to_json(item)

        #         pack_end_at = datetime.strptime(subs_data['pack_end_at'], '%Y-%m-%d %H:%M:%S')

        #         diff_days = abs((current_date-pack_end_at).days)               

        #         if diff_days <= 5:

        #             rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))


        #             package_list = PackageModel.get_by_id(subs_data['package_id'])
        #             package_data = PackageModel.to_json(package_list)

        #             amount = package_data['package_price']
        #             tax_gst = 18

        #             # net_amount = 118*18/100+18 = Rs 18
        #             tax_amount = int(amount)*tax_gst/100+tax_gst

        #             net_amount = int(amount)+int(tax_amount)

        #             invoice_list = InvoiceModel.get_by_company_id(value['company_id'])

        #             if invoice_list.cron_status == 1:
        #                 new_invoice = InvoiceModel(
        #                     invoice_no=rand,
        #                     company_id= value['company_id'],
        #                     package_id=subs_data['package_id'],
        #                     subscription_type= 2,
        #                     gross_amount=amount,
        #                     tax_gst=tax_gst,
        #                     net_amount=net_amount,
        #                     invoice_status=0,
        #                     cron_status=0,
        #                     status=1
        #                 )
        #                 new_invoice.save_to_db()

        #                 return {
        #                     'status': 200,
        #                     'success': True,
        #                     'message': 'transaction received',
        #                     'invoice_list.cron_status':invoice_list.cron_status,
        #                     'invoice_list.invoice_id':invoice_list.invoice_id,
        #                     }, 200




                    
        #         else:
        #             pass


        #     else:
        #         print(subs_data)
                    

                
        
        

        # return {
        #         'status': 200,
        #         'success': True,
        #         'message': 'transaction received',
        #         'data':subs_data,
        #         }, 200