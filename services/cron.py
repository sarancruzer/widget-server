from models.company import CompanyModel

from datetime import datetime
from dateutil.relativedelta import relativedelta

from models.subscription import SubscriptionModel
import random
import string
from models.package import PackageModel
from models.invoice import InvoiceModel

def subscriptionCron():
    
    company_list = CompanyModel.get_all()
    company_data = list(map(lambda x: CompanyModel.to_json(x), company_list))
    # company_data = CompanyModel.to_json(company_list)

    current_date = datetime.today()
    # current_date = current_date.strftime('%Y-%m-%d')

    for value in company_data:
        
        item = SubscriptionModel.get_by_company_id(value['company_id'])
        subs_data = []
        if item:
            subs_data = SubscriptionModel.to_json(item)

            pack_end_at = datetime.strptime(subs_data['pack_end_at'], '%Y-%m-%d %H:%M:%S')

            # pack_end_at = pack_end_at.strftime('%Y-%m-%d')


            # someday = datetime.date(2008, 12, 25)
            # diff = current_date-pack_end_at
            # diff.days

            # print("diff.days")
            # print(diff.days)
            
            diff_days = abs((current_date-pack_end_at).days)               

            if diff_days <= 5:

                rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))


                package_list = PackageModel.get_by_id(subs_data['package_id'])
                package_data = PackageModel.to_json(package_list)

                amount = package_data['package_price']
                tax_gst = 18

                # net_amount = 118*18/100+18 = Rs 18
                tax_amount = int(amount)*tax_gst/100+tax_gst

                net_amount = int(amount)+int(tax_amount)

                invoice_list = InvoiceModel.get_by_company_id(value['company_id'])

                if invoice_list.cron_status == 1:
                    new_invoice = InvoiceModel(
                        invoice_no=rand,
                        company_id= value['company_id'],
                        package_id=subs_data['package_id'],
                        subscription_type= 2,
                        gross_amount=amount,
                        tax_gst=tax_gst,
                        net_amount=net_amount,
                        invoice_status=0,
                        cron_status=0,
                        status=1
                    )
                    new_invoice.save_to_db()

                    return {
                        'status': 200,
                        'success': True,
                        'message': 'transaction received',
                        'invoice_list.cron_status':invoice_list.cron_status,
                        'invoice_list.invoice_id':invoice_list.invoice_id,
                        }, 200




                
            else:
                pass


        else:
            print(subs_data)
                

            
    
    

    return {
            'status': 200,
            'success': True,
            'message': 'transaction received',
            'data':subs_data,
            }, 200

def ChatClearCron():
    print("CCCCCCCRRRRRRRRRRRROOOOOOOOOOOOOOOOOONNNNNNNNNN")