from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date

from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func

class CrawlerListModel(db.Model):
    __tablename__ = 'crawler_list'

    crawler_list_id = Column('crawler_list_id',Integer, primary_key=True)
    company_id = Column("company_id",Integer, nullable = False)    
    website_id = Column('website_id', Integer, nullable = True) 
    url = Column('url', Text, nullable = False) 
    content = Column('content', Text, nullable = False)         
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
           
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush()
        db.session.rollback()
        return 

   
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    
    @classmethod
    def to_json(cls, x):     
        
        return {
                'crawler_list_id':x.crawler_list_id,
                'company_id':x.company_id,
                'website_id': x.website_id,
                'url': x.url,
                'content':x.content,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status,
            }
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

