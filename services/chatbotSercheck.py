import nltk
import json
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
from models.question import QuestionModel
from flask_jwt_extended.utils import get_jwt_identity, create_access_token
from models.user import UserModel
from models.contextGroup import ContextGroupModel
from models.contextGroupDetails import ContextGroupDetailsModel
from models.contextResponse import ContextResponseModel
from models.website import WebsiteModel
from models.chatbotResponse import ChatbotResponseModel
from shared import db
import app
from logging.handlers import RotatingFileHandler
import logging
import os
from sqlalchemy.sql.functions import now
from models.company import CompanyModel
from models.packResponseCount import PackResponseCountModel

from datetime import datetime
from dateutil.relativedelta import relativedelta
from models.chatHistoryDetails import ChatHistoryDetailsModel
from resources.restrictionResources import ResponseCount
from autocorrect import spell# word stemmer
stemmer = LancasterStemmer()

sample = []
training_data = []


def out_fun(filepath):
    with open(filepath, 'r') as f:
        training_data = json.load(f)
        #print ("%s sentences in training data" % len(training_data))
        return training_data

# y = out_fun()
# print(y)


# # capture unique stemmed words in the training corpus
# corpus_words = {}
# class_words = {}
# # turn a list into a set (of unique items) and then a list again (this removes duplicates)
# classes = list(set([a['class'] for a in y]))
# for c in classes:
#     # prepare a list of words within each class
#     class_words[c] = []

# # loop through each sentence in our training data
# for data in y:
#     # tokenize each sentence into words
#     for word in nltk.word_tokenize(data['sentence']):
#         # ignore a some things
#         if word not in ["?", "'s"]:
#             # stem and lowercase each word
#             stemmed_word = stemmer.stem(word.lower())
#             # have we not seen this word already?
#             if stemmed_word not in corpus_words:
#                 corpus_words[stemmed_word] = 1
#             else:
#                 corpus_words[stemmed_word] += 1

#             # add the word to our words in class list
#             class_words[data['class']].extend([stemmed_word])



# def calculate_class_score_commonality(sentence, class_name, show_details=True):
#     score = 0
#     # tokenize each word in our new sentence
#     for word in nltk.word_tokenize(sentence):
#         # check to see if the stem of the word is in any of our classes
#         if stemmer.stem(word.lower()) in class_words[class_name]:
#             # treat each word with relative weight
#             score += (1 / corpus_words[stemmer.stem(word.lower())])

#             if show_details:
#                 print ("   match: %s (%s)" % (stemmer.stem(word.lower()), 1 / corpus_words[stemmer.stem(word.lower())]))
#     return score

   

# def classify(sentence):
#     high_class = None
#     high_score = 0
#     for c in class_words.keys():
#         score = calculate_class_score_commonality(sentence, c, show_details=False)
#         if score > high_score and score > 0.1:
#             high_class = c
#             high_score = score


#     resList = responses(high_class,y)
#     return resList

def classify(sentence,yy):


    # capture unique stemmed words in the training corpus
    corpus_words = {}
    class_words = {}
    # turn a list into a set (of unique items) and then a list again (this removes duplicates)
    classes = list(set([a['class'] for a in yy]))

    for c in classes:
        # prepare a list of words within each class
        class_words[c] = []
    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
    
    print(yy)
    print(sentence)

    
    # loop through each sentence in our training data
    for data in yy:
  
        # tokenize each sentence into words
        for word in nltk.word_tokenize(data['sentence']):
            print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&")

            print(word)
            # ignore a some things
            if word not in ["?", "'s"]:
                # stem and lowercase each word
                stemmed_word = stemmer.stem(word.lower())
               
                # have we not seen this word already?
                if stemmed_word not in corpus_words:
                    corpus_words[stemmed_word] = 1
                else:
                    corpus_words[stemmed_word] += 1
           
                # add the word to our words in class list
                class_words[data['class']].extend([stemmed_word])

    print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    print(data['class'])
    high_class = None
    high_score = 0
    
  
    for c in class_words.keys():

        # score = calculate_class_score_commonality(sentence, c, show_details=False)

        score = 0
        # tokenize each word in our new sentence
        for word in nltk.word_tokenize(sentence):
            print("=====================================")
            print(word)
            print(high_class)

            # check to see if the stem of the word is in any of our classes
            if stemmer.stem(word.lower()) in class_words[c]:
                print("22222222222222222222222222222222222222")

                print(class_words)
             
                # treat each word with relative weight
                score += (1 / corpus_words[stemmer.stem(word.lower())])
                



            if score > high_score and score > 0.5:
                high_class = [c,data['class']]
                high_score = score


    # resList = responses(high_class,yy)
    return high_class,yy 


from datetime import datetime


def askqFunc(data):

    sentence = data['sentence']
    apikey = data['api_key']
    context = data['context']
    context_token = data['context_token']
    response = data['response']
    context_res_id = data['context_res_id']
   
    user_token = data['user_token']
    
    userModelRes = UserModel.get_by_api_key(apikey)    
    website_data = WebsiteModel.get_by_api_key(apikey)
    
    # website_data = WebsiteModel.to_json(website_list)

    # company_list = CompanyModel.get_by_apikey(apikey)
    # company_data = CompanyModel.to_json(website_list)

    api_key = data['api_key']
       
    print("=======================#####")

    print(api_key)


        
    query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"'"

    package_details = db.db.engine.execute(query)


    print("PACKAGE DETAILS ############")
    print(package_details)

    pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]

    print("PACK DETAILS")
    print(pack_result)

      
    RESPONSE_PERMIT_COUNT = pack_result['response_count']
    RESPONSE_CURRENT_COUNT = 0

    resultList = PackResponseCountModel.get_by_api_key(api_key)
        
    result = []
    if resultList:
        result = PackResponseCountModel.to_json(resultList)
        RESPONSE_CURRENT_COUNT = result['response_count']

        
    print(result)

    print(RESPONSE_CURRENT_COUNT)
    print("$$$$$$$$$$$$$$$$$$=====================")

    print(RESPONSE_PERMIT_COUNT)



    if RESPONSE_CURRENT_COUNT > RESPONSE_PERMIT_COUNT:
        
        return {
                'status': 400,
                'success': False,
                'message': 'An error occurred getting the response',
            }, 400


    
    print("PARAMS DATA")
    print(data)

    print("WEBSITE DATA")
    print(website_data)
    print(website_data['trained_json_path'])

    

    rList = {'CLASS':'','QUESTION':'','HREF_LINK':'', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': '', 'RAW_TEXT': '', 'SUGGESTIONS_TEXT': []}

    if context and context_token:
        print("CONTEXT")
        print("context_res_id context_res_id context_res_id context_res_id")
        print(context_res_id)
        # updateContextualProcess(context,context_token,response,context_res_id)
        contextRes, contextGp, contextGpDetails,context_token,conv_status = updateContextualProcess(context,context_token,sentence,context_res_id)
        print("#######contextRes contextRes contextRes##########")
        print(contextRes)
        print("#################")

        print("$$$$$$$$$contextGp contextGp contextGp$$$$$$$$$$$")
        print(contextGp)
        print("$$$$$$$$$$$$$$$$$$$$$$")

        print("&&&&&&&&&contextGpDetails contextGpDetails contextGpDetails &&&&&&&&&&&")
        print(contextGpDetails)
        print("&&&&&&&&&&&&&&&&&&&&")


        print("@@@@@@@@@@@@")
        print(conv_status)
        print("@@@@@@@@@@@@@@")

        if conv_status == True:

            contextSuccessMsg = getContextualSuccessMsg(context,website_data['website_id'])

            rList['RAW_TEXT'] = contextSuccessMsg
            rList['CONTEXT_RES_ID'] = ""
            rList['SUGGESTIONS_TEXT'] = ""
            rList['RESPONSE_TYPE'] = ""
            rList['CONTEXT'] = ""
        else:
            rList['RAW_TEXT'] = contextRes[0]['sentence']
            rList['CONTEXT_RES_ID'] = contextRes[0]['context_resp_id']
            rList['SUGGESTIONS_TEXT'] = contextGpDetails['context_group_details']
            rList['RESPONSE_TYPE'] = contextGp['response_type']
            rList['CONTEXT'] = context
            

        rList['QUESTION'] = ""
        rList['HREF_LINK'] = ""
        rList['AUDIO_LINK'] = ""
        rList['VIDEO_LINK'] = ""
        rList['IMAGE_LINK'] = ""
        rList['USER_TOKEN'] = context_token

    else:

        print("CONTEXT NOT")
        print("sentence")
        print(sentence)

        
        directory_path = website_data['trained_json_path']
        
        print("directory_path")
        print(directory_path)
        # directory_path = "jsons/claritaz.json"
        yy = out_fun(directory_path)

        high_class, yy = classify(sentence,yy)
      
        rList = responses(sentence,high_class,apikey,context_token)

        print("x")
        print("rList rList rList rList")

        print(rList)

        # save every response into chatbot_response table
        # new_data = ChatbotResponseModel(
        #     company_id = website_data['company_id'],
        #     website_id =  website_data['website_id'],
        #     user_token =  user_token,
        #     context_name =  rList['CONTEXT'],
        #     sentence =  rList['QUESTION'],
        #     response =  rList['RAW_TEXT']
        #     )
        # new_data.save_to_db()

        chat_res_data = ChatHistoryDetailsModel(
            context_name =  rList['CONTEXT'],
            sentence =  sentence,
            response =  rList['RAW_TEXT'],
            chat_res_id = data['chat_history_id']
            )
        chat_res_data.save_to_db()





        query = "UPDATE pack_response_count SET response_count=response_count+1 WHERE api_key='"+ apikey +"'"
        result = db.db.engine.execute(query)

        current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        date_query = "UPDATE chat_history SET updated_at='"+current_date+"' WHERE chat_resp_id="+ data['chat_history_id']
        result = db.db.engine.execute(date_query)

  
    
    return rList





def responses(sentence,class_val,apikey,context_token):

    rList = {'CLASS':'','QUESTION':'','HREF_LINK':'', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': '', 'RAW_TEXT': '', 'SUGGESTIONS_TEXT': []}

    print("===================")



    print("CLASS VALUE")
    print(class_val)

    print("===================")

    website_list = WebsiteModel.get_by_apikey(apikey)
    website_data = WebsiteModel.to_json(website_list)

    if len(class_val) ==1:
        print("length is equal to one")

        if(class_val != None):
                    
            check_classname = QuestionModel.find_by_class_val(class_val)
            if check_classname == 0:
                class_val = None
                print("check_classname check_classname check_classname")
                print(check_classname)
    



            if(class_val != None):

                rList['CLASS'] = class_val
                print("value value")
                print(class_val)

                qresult = QuestionModel.get_by_classname(class_val,website_data['website_id'])
                print("RESS0000000000000000000")
                print(qresult)

                print(qresult.class_name)

                # qq = qRes['questions'][0]
                qq = qresult

               
            if qq.context:
                contextRes, contextGp, contextGpDetails,context_token = newContextualProcess(qq.context,website_data)

            
                rList['QUESTION'] = ""
                rList['RAW_TEXT'] = contextRes[0]['sentence']
                rList['HREF_LINK'] = ""
                rList['AUDIO_LINK'] = ""
                rList['VIDEO_LINK'] = ""
                rList['IMAGE_LINK'] = ""
                rList['SUGGESTIONS_TEXT'] = contextGpDetails
                rList['RESPONSE_TYPE'] = contextGp['response_type']
                rList['CONTEXT'] = qq.context
                rList['USER_TOKEN'] = context_token
                rList['CONTEXT_RES_ID'] = contextRes[0]['context_resp_id']
                
            else:
                    # print(qq)
                rList['QUESTION'] = qq.sentence
                rList['RAW_TEXT'] = qq.response
                rList['HREF_LINK'] = qq.href_link
                rList['AUDIO_LINK'] = qq.audio_link
                rList['VIDEO_LINK'] = qq.video_link
                rList['IMAGE_LINK'] = qq.image_link
                rList['SUGGESTIONS_TEXT'] = []
                rList['RESPONSE_TYPE'] = ""
                rList['CONTEXT'] = ""
                rList['USER_TOKEN'] = ""
                rList['CONTEXT_RES_ID'] = ""

            return rList


   
        else:
            print('----- else called ------')
            suggests=['About your company','Currently what openings are going on?','What is iPITCH?','How to become a Mentor?','Talk to our Agent']

            rList['QUESTION'] = sentence
            rList['RAW_TEXT'] = "I am not sure I understand. Here are the suggestions I have."
            rList['HREF_LINK'] = ""
            rList['AUDIO_LINK'] = ""
            rList['VIDEO_LINK'] = ""
            rList['IMAGE_LINK'] = ""
            rList['SUGGESTIONS_TEXT'] = suggests
            rList['RESPONSE_TYPE'] = "BUTTON"
            rList['CONTEXT'] = ""
            rList['USER_TOKEN'] = ""
            rList['CONTEXT_RES_ID'] = ""
        
            
        print('-- Response print-------')
        print(rList)
        return rList

    else:
        print("length is Notequal to one")
        resdat =[]

        for value in class_val:
                print("88888888888888888888888888888888")
                print(value)

                if(value != None):
                    rList['CLASS'] = value
                    print("value value")
                    print(value)

                    qresult = QuestionModel.get_by_classname(value,website_data['website_id'])
                    print(qresult)
                    datata = QuestionModel.to_json(qresult)
                    print("RESS0000000000000000000")
                    print(datata)

                    print(datata['response'])
                    
                    # qq = qRes['questions'][0]
                resdat.append(datata['response'])
                print("RESS Array RESS Array RESS Array")

                print(resdat)
            
        qq = qresult

        rList['QUESTION'] = qq.sentence
        rList['RAW_TEXT'] = ""
        rList['HREF_LINK'] = qq.href_link
        rList['AUDIO_LINK'] = qq.audio_link
        rList['VIDEO_LINK'] = qq.video_link
        rList['IMAGE_LINK'] = qq.image_link
        rList['SUGGESTIONS_TEXT'] = resdat
        rList['RESPONSE_TYPE'] = "BUTTON"
        rList['CONTEXT'] = ""
        rList['USER_TOKEN'] = ""
        rList['CONTEXT_RES_ID'] = ""

    return rList


       



def newContextualProcess(context_name,website_data):

    context_token = create_access_token(identity = website_data)

    context_gp_list = ContextGroupModel.get_by_contextname(context_name,website_data['website_id'])


    # context_gp = ContextGroupModel.to_json(context_gp_list)
    context_gp = list(map(lambda x: ContextGroupModel.to_json(x), context_gp_list))
    
    print("CONTEXT GROUP")
    print(context_gp)

    for value in context_gp:
        
        new_data = ContextResponseModel(
                website_id=website_data['website_id'],
                context_token=context_token,
                context_gp_id=value['context_gp_id'],
                context_name=value['context_name'],
                sentence=value['sentence'],
                status=0
            )
        new_data.save_to_db()

    
    getContextResData = getContextualProcess(context_name,context_token)
 

    
    context_gp_details = ContextGroupDetailsModel.get_by_context_gp_id_custom(getContextResData[0]['context_gp_id'])

    context_gp_data = ContextGroupModel.get_by_context_gp_id(getContextResData[0]['context_gp_id'])

    context_gp = ContextGroupModel.to_json(context_gp_data)
    
    return getContextResData,context_gp,context_gp_details['context_group_details'],context_token

def getContextualProcess(context_name,context_token):

    
    context_resp_list = ContextResponseModel.get_by_contextname(context_name,context_token)

    context_resp = list(map(lambda x: ContextResponseModel.to_json(x), context_resp_list))

    print("CONTEXT RESP")
    print(context_resp)
    
    return context_resp    

def updateContextualProcess(context_name,context_token,response,context_res_id):



    new_data = ContextResponseModel.get_by_id(context_res_id)

    print("CONTEXT RESPppppppppppppppp"+context_res_id)
    print(new_data)

    new_data.response = response
    new_data.status = 1

    new_data.save_to_db()

    getContextResData = getContextualProcess(context_name,context_token)
    conv_status = False

    if getContextResData:
        print("GET CONTEXTRES DATA FALSE")

        # context_data = ContextResponseModel.get_by_id(context_res_id)
        # context_data = ContextResponseModel.to_json(context_data)

        context_data = ContextResponseModel.get_by_contextname(context_name,context_token)
        context_data = list(map(lambda x: ContextResponseModel.to_json(x), context_data))

        print(context_data)
        print(context_data[0]['context_gp_id'])
        context_gp_details = ContextGroupDetailsModel.get_by_context_gp_id_custom(context_data[0]['context_gp_id'])
        context_gp_data = ContextGroupModel.get_by_context_gp_id(context_data[0]['context_gp_id'])
        context_gp = ContextGroupModel.to_json(context_gp_data)
        print("context_gp_detailsddddddddddddddddddddddddddddddddddddddddddddddd")
        print(context_gp_details)
    else:
        print("GET CONTEXTRES DATA TRUE")
        conv_status = True
        context_gp_details = []
        context_gp = []
        # context_gp_details['context_group_details'] = []
        # context_gp['context_group'] = []


    return getContextResData,context_gp,context_gp_details,context_token,conv_status

def getContextualSuccessMsg(context,website_id):
    print("CONTEXT MSG")
    print(context)
    print(website_id)

    context_msg = ContextGroupModel.get_success_by_contextname(context,website_id)
    print(context_msg)

    return context_msg.success_msg


def readLogs(api_key):

    website_data = WebsiteModel.get_by_api_key(api_key)

    query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE api_key='"+ api_key +"'"

    package_details = db.db.engine.execute(query)


    print("PACKAGE DETAILS ############")
    print(package_details)

    pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]

    print("PACK DETAILS")
    print(pack_result)
   
    # pack_end_at = datetime.strptime(str(pack_result['pack_end_at']), '%Y-%m-%d %H:%M:%S')
    # print(pack_end_at)

    logs_count = pack_result['logs_count']


    # current_date = datetime.datetime.strptime("2011-01-01","%Y-%m-%d")
    date_before_week = datetime.today() - relativedelta(weeks=logs_count)
    
    print(date_before_week)
    print("------------------------")


    filepath = "customlogs/" + str(website_data['company_code']) + "/" + str(website_data['website_name']) + "/" + "info.log"
    # filepath = "logs/" + company_code + "/" + website_name + "/" + "info.log"

    data = []
    with open(filepath, 'r') as f_in:
       
        lines = (line.rstrip() for line in f_in) 
        lines = list(line for line in lines if line) # Non-blank lines in a list
        # print(x)

        for key,value in enumerate(lines):
            # print(value)
            infoList = value.split('INFO')
            print(infoList)

            logged_date = infoList[0].split(',')[0]
            infoqaList = infoList[1].split('###')
            question = infoqaList[0]
            answer = infoqaList[1]

            logged_date = datetime.strptime(logged_date, '%Y-%m-%d %H:%M:%S')

            if date_before_week <= logged_date:
                data.append({'question':question, 'answer':answer,'logged_date': str(logged_date)})
        

    return data
