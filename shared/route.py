from resources import userResources, questionResources,chatbotResources,packageResources,categoryResources,websiteResources,\
    operatorResources, companyResources,roleResources,\
    widgetLayoutResources, subscriptionResources, restrictionResources,\
    dashboardResources, paymentResources, invoiceResources,chatHistoryResources,\
    manualChatResources, crawlerResources,playBookResource
#from src.controllers import userController

def init_routes(api):
    #To check whether api is working
    api.add_resource(userResources.Api, '/api')

    #Users api
    api.add_resource(userResources.UserRegistration, '/api/registration')
    api.add_resource(userResources.UserLogin, '/api/authenticate')
    api.add_resource(userResources.UserLogoutAccess, '/api/logout/access')
    api.add_resource(userResources.UserLogoutRefresh, '/api/logout/refresh')
    api.add_resource(userResources.TokenRefresh, '/api/token/refresh')
    api.add_resource(userResources.SecretResource, '/api/secret')
    
    api.add_resource(userResources.VerifyEmail, '/api/token/verify/email')
    api.add_resource(userResources.ResendVerifyEmail, '/api/token/verify/email/resend')
    
    # profile email verify
    api.add_resource(userResources.SendVerifyEmail, '/api/token/verify/email/send')

    # Forgot password mail
    api.add_resource(userResources.SendForgotPasswordEmail, '/api/password/forgot/email/send')
    api.add_resource(userResources.VerifyForgotPasswordEmail, '/api/password/forgot/email/verify')
    api.add_resource(userResources.UpdateForgotPasswordEmail, '/api/password/forgot/email/update')

    # Password change
    api.add_resource(userResources.PasswordChange, '/api/password/change')


    #Users api
    api.add_resource(userResources.UserList, '/api/users')
    api.add_resource(userResources.User, '/api/users/<string:id>')
    api.add_resource(userResources.UserData, '/api/users/client/<string:id>')
    
    

    # get current user
    api.add_resource(userResources.currentUser, '/api/users/current-user')
    api.add_resource(userResources.UserAddress, '/api/users/address')

    api.add_resource(userResources.AppCrawler, '/api/crawler')
    # api.add_resource(crawlerResources.CrawlerList, '/api/crawler/list')
    # api.add_resource(crawlerResources.Crawler, '/api/crawler/list/<string:id>')
    api.add_resource(questionResources.ContextType, '/api/contextType')


    #Operators api
    api.add_resource(operatorResources.OperatorList, '/api/operators')
    api.add_resource(operatorResources.Operator, '/api/operators/<string:id>')
    api.add_resource(operatorResources.OperatorAssign, '/api/operators/assign')
    api.add_resource(operatorResources.chatReqMail, '/api/operators/sendmail/chat')

        
    
    # questions api
    api.add_resource(questionResources.QuestionList, '/api/questions')
    api.add_resource(questionResources.Question, '/api/questions/<string:id>')
    api.add_resource(questionResources.QuestionPublish, '/api/questions/publish')
    api.add_resource(questionResources.QuestionPublishCheck, '/api/questions/publish/check')
    api.add_resource(questionResources.CrawlerToQns, '/api/questions/move')

    # packages api
    api.add_resource(packageResources.PackageList, '/api/packages')
    api.add_resource(packageResources.Package, '/api/packages/<string:id>')

    # subscription api
    api.add_resource(subscriptionResources.SubscriptionList, '/api/subscription')
    api.add_resource(subscriptionResources.Subscription, '/api/subscription/<string:id>')
    api.add_resource(subscriptionResources.SubscriptionCompany, '/api/subscription/company')

    api.add_resource(subscriptionResources.SubscriptionDetails, '/api/subscription/details')
    api.add_resource(subscriptionResources.SubscriptionPlans, '/api/subscription/plans')
    api.add_resource(subscriptionResources.SubscriptionRenewal, '/api/subscription/renewal')
    api.add_resource(subscriptionResources.CheckResponseCount, '/api/subscription/response')


    # payment creation api
    api.add_resource(paymentResources.InvoicePaymentCreation, '/api/invoice/payment/subscription')
    api.add_resource(paymentResources.PaymentCreation, '/api/invoice/payment/subscription/<string:id>')

    # payment subscription cron
    api.add_resource(paymentResources.SubscriptionCron, '/api/payment/subscription/cron')
    

    # invoice api
    api.add_resource(invoiceResources.InvoiceList, '/api/invoice')
    api.add_resource(invoiceResources.InvoiceView, '/api/invoice/view/<string:id>')

    # transaction api
    api.add_resource(paymentResources.PaymentList, '/api/payment')

    # category api
    api.add_resource(categoryResources.CategoryList, '/api/category')
    api.add_resource(categoryResources.Category, '/api/category/<string:id>')
    api.add_resource(categoryResources.categoryMaster, '/api/categoryMaster')

    # role api
    api.add_resource(roleResources.RoleList, '/api/role')
    api.add_resource(roleResources.Role, '/api/role/<string:id>')

    # website api
    api.add_resource(websiteResources.WebsiteList, '/api/websites')
    api.add_resource(websiteResources.Website, '/api/websites/<string:id>')
    api.add_resource(websiteResources.WebsitePublish, '/api/websites/publish')
 
    # widget layout api
    api.add_resource(widgetLayoutResources.WidgetComponent, '/api/widgets/components/<string:id>')
    api.add_resource(widgetLayoutResources.WidgetComponentList, '/api/widgets/components/publish')
    api.add_resource(widgetLayoutResources.WidgetIcons, '/api/widgets/components/icons')

    # api.add_resource(widgetLayoutResources.WidgetComponentClient, '/api/widgets/components/client')
    
    #chatbot api
    api.add_resource(chatbotResources.AskQ, '/api/askq')
    api.add_resource(chatbotResources.AutomateTestAskQ, '/api/automateTestAskq')    
    api.add_resource(chatbotResources.AutomateImportJsonData, '/api/automateImportJsonData')

    api.add_resource(chatbotResources.Logs, '/api/logs/list')

    #test api for mulple json
    # api.add_resource(chatbotResources.AskQQ, '/api/askqq')

    #Company crud api
    api.add_resource(companyResources.CompanyList, '/api/company')
    api.add_resource(companyResources.Company, '/api/company/<string:id>')
    api.add_resource(companyResources.CompanyProfile, '/api/company/profile')

    # Package Restrictions api

    api.add_resource(restrictionResources.ChatbotCount, '/api/restriction/chatbot/count')
    api.add_resource(restrictionResources.ResponseCount, '/api/restriction/response/count')
    api.add_resource(restrictionResources.DomainValidation, '/api/restriction/domain/validation')

    #api for getting total questions and operators
    api.add_resource(dashboardResources.DashboardList, '/api/dashboard')
    api.add_resource(dashboardResources.OperatorsList, '/api/dashboard/operators')
    api.add_resource(dashboardResources.SendmailWidgetcode, '/api/dashboard/sendmail/widgetcode')
    api.add_resource(dashboardResources.SendmailContactForm, '/api/dashboard/sendmail/contact')


    #api for getting chathistory
    api.add_resource(chatHistoryResources.ChatHistoryList, '/api/chathistory')
    api.add_resource(chatHistoryResources.ChatHistoryCreate, '/api/chathistory/create') 
    api.add_resource(chatHistoryResources.ChatHistoryDetail, '/api/chathistory/history/<string:id>')

    #api for getting manualChat
    api.add_resource(manualChatResources.RoomAvailable, '/api/chat/room/available')
    api.add_resource(manualChatResources.GetAllRooms, '/api/chat/room/lists')
    api.add_resource(manualChatResources.GetActiveRooms, '/api/chat/room/lists/active')
    api.add_resource(manualChatResources.SendMailOperator, '/api/chat/room/mail')

    
    #api for getting crawler
    # api.add_resource(crawlerResources.WebsiteDetails, '/api/websitelist')
    api.add_resource(crawlerResources.WebsiteCrawler, '/api/websitelist/<string:id>')
    api.add_resource(crawlerResources.GetCrawlWebsite, '/api/websites/crawl/<string:id>')

    api.add_resource(playBookResource.PlayBook, '/api/playBook')
    api.add_resource(playBookResource.CreatePlayBook, '/api/createPlayBook')
    api.add_resource(playBookResource.SavePlayBookData, '/api/savePlayBookData')
    api.add_resource(playBookResource.getPlayBookData, '/api/questions/getPlayBook/<string:playbook_id>/<string:question_id>')
    #api.add_resource(userController.getUsers,'/api/userList')

    

    






    
