from shared.db import db
from sqlalchemy import Column, Integer, Text
from sqlalchemy.sql.sqltypes import DATETIME, String
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from flask_sqlalchemy import Pagination
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity


class ChatHistoryDetailsModel(db.Model):
    __tablename__ = 'chat_history_details'

    chat_res_det_id = Column('chat_res_det_id',Integer, primary_key=True)
    chat_res_id = Column('chat_res_id', Integer, nullable = False)
    context_name = Column('context_name', String(100), nullable = False)
    sentence = Column('sentence', Text, nullable = False)
    response = Column('response', Text, nullable = True)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)

    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 



    @classmethod
    def to_json(cls, x):
            return {
                'chat_res_det_id': x.chat_res_det_id,
                'chat_res_id':x.chat_res_id,
                'context_name': x.context_name,
                'sentence': x.sentence,
                'response': x.response,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def get_by_id(cls, chat_res_det_id):        
        return cls.query.filter_by(chat_res_det_id=chat_res_det_id).first()

    @classmethod
    def get_by_chat_resp_id(cls, chat_res_det_id):        
        return cls.query.filter_by(chat_res_id=chat_res_det_id).all()  

    @classmethod
    def return_all(cls, page, searchTerm,website_id):               
        page = int(page)
        
        query = "SELECT q.*,c.website_id FROM chatbot_response as q LEFT JOIN website as c ON c.website_id = q.website_id  where q.website_id = " + str(website_id)  

        if searchTerm:
            query = query + " AND q.sentence LIKE :search_term"



        params = { 'search_term' : '%'+ str(searchTerm) + '%'}

        return  query, params
        

    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

   

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
