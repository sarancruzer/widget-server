from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)
import requests
from flask_json import FlaskJSON, json_response, as_json
from datetime import datetime

from models.question import QuestionModel
import random
import string

from flask import request
from shared.util import custom_paginate

from flask import jsonify,make_response

from flask.json import jsonify
import json as simplejson

from models.user import UserModel
from shared import db
import app
from models.contextType import ContextTypeModel
from models.contextGroup import ContextGroupModel
from models.contextGroupDetails import ContextGroupDetailsModel

parser = reqparse.RequestParser()

parser.add_argument('user_id', required=False, help='This field cannot be left blank')
parser.add_argument('class_name', required=False, help='Must enter the class_name')
parser.add_argument('sentence', required=False, help='Must enter the sentence')
parser.add_argument('category_id', required=False, help='Must enter the category')
parser.add_argument('response', required=False, help='Must enter the response')
parser.add_argument('image_link', required=False, help='Must enter the image_link')
parser.add_argument('context', required=False, help='Must enter the context')
parser.add_argument('conversation_status_yes', required=False, help='Must enter the conversation_status_yes')
#parser.add_argument('questionNode',dest='questionNode',default=[], required=False)
parser.add_argument('href_link', required=False, help='Must enter the href_link')
parser.add_argument('audio_link', required=False, help='Must enter the audio_link')
parser.add_argument('video_link', required=False, help='Must enter the video_link')
parser.add_argument('website_id', required=False, help='Must enter the website id')
parser.add_argument('success_response', required=False, help='Must enter the success_response')



class Question(Resource):

    
    def get(self, id):


        item = QuestionModel.find_by_id(id)

        if item['context'] and item['conversation_status']:
            
            
            context_group_record = ContextGroupModel.get_by_contextname(item['context'],item['website_id'])
            context_data_details = list(map(lambda x: ContextGroupModel.to_json(x), context_group_record))

            
            item['questionNode'] = context_data_details
            item['success_response'] = context_data_details[0]['success_msg']
            for value in item['questionNode']:
                context_group_detail =ContextGroupDetailsModel.get_by_custom_context_gp_id(value['context_gp_id'])
                context_group_details = list(map(lambda x: ContextGroupDetailsModel.to_json(x), context_group_detail))

                value['options'] = context_group_details
        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'question received',
                'data':  item
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = QuestionModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'question has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()

        new_data = QuestionModel.get_by_id(id)

        data['website_id'] = data['website_id']
        if new_data is None:
            new_data = QuestionModel(id, data['class_name'])
        else:
            # new_data.user_id = 1
            new_data.sentence = data['sentence']
            new_data.category_id = data['category_id']
            new_data.response = data['response']
            new_data.image_link = data['image_link']
            new_data.href_link = data['href_link']
            new_data.publish_status = 1
            new_data.conversation_status_yes = data['conversation_status_yes']
            new_data.context=data['context']
            new_data.website_id=data['website_id']

            if data['context'] and data['conversation_status_yes']:
                new_data.context=data['context'] 
                data_details_delete=[]                    
                last_data_delete = ContextGroupModel.get_last_delete(data['context'],data['website_id'])

                if last_data_delete:
                    data_details_delete = list(map(lambda x: ContextGroupModel.to_json(x), data_details_delete))

                item = ContextGroupModel.get_by_contextname(data['context'],data['website_id'])
                #item = list(map(lambda x: ContextGroupModel.to_json(x), item))
                if item:
                    data_delete ="DELETE FROM context_group WHERE context_name='"+ data['context'] +"' AND website_id = "+str(data['website_id'])
                    result = db.db.engine.execute(data_delete)
                
                node = request.get_json(silent=True)
                ques_node = node['questionNode']
                
                for value in ques_node:
                    context_data = ContextGroupModel(
                        website_id=data['website_id'],
                        context_name=data['context'],
                        sentence=value['sentence'],
                        response_type=value['response_type'],
                        success_msg=data['success_response'],
                        status=1
                    )
                    context_data.save_to_db()
                    
                    last_data = ContextGroupModel.get_last(data['context'],data['website_id'])
                    data_details = ContextGroupModel.to_json(last_data)
                    if data_details_delete:
                        for dele in data_details_delete:
                            data_delete_group ="DELETE FROM context_group_details WHERE context_gp_id = "+dele['context_gp_id']
                            result = db.db.engine.execute(data_delete_group)

                    for opt in value['options']:
        
                        context_gp_data = ContextGroupDetailsModel(
                            context_gp_id=data_details['context_gp_id'],
                            response=opt['response'],
                            status=1
                        )
                        context_gp_data.save_to_db()
        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'question has been updated'
            }, 200
        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the questions',
            }, 400
       
class ContextType(Resource):
    def get(self):
        item = ContextTypeModel.get_all_context()
        data = list(map(lambda x: ContextTypeModel.to_json(x), item))

        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'question received',
                'data': data
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
class QuestionList(Resource):
    
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')
        website_id = request.args.get('website_id', '')
        category_id = request.args.get('category_id', '')
        status = request.args.get('status','')      
        print("QQQQQQ LISTTTTT")
        query, params = QuestionModel.return_all(page, searchTerm,website_id,category_id,status)
        per_page = 10

        result, cus_paging = custom_paginate(query, page, per_page,params)
        if status != '3':
            data = list(map(lambda x: QuestionModel.to_json(x), result))
        else:
            data = list(map(lambda x: QuestionModel.cust_to_json(x), result))
        return {
                'status': 200,
                'success': True,
                'message': 'question received',
                'data':data,
                'paginate': cus_paging
                }, 200
    
    @jwt_required
    def post(self):
      
        data = parser.parse_args()
            
        node = request.get_json(silent=True)

        #ques_node = node['questionNode']


        current_user = get_jwt_identity()
        userId = current_user['user_id']
        
        
        rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))
        
        new_data = QuestionModel(
            user_id=userId,
            class_name=rand,
            website_id=data['website_id'],
            sentence=data['sentence'],
            category_id=data['category_id'],
            response=data['response'],
            image_link=data['image_link'],
            audio_link=data['audio_link'],
            video_link=data['video_link'],
            href_link=data['href_link'],
            status=1,
            publish_status=1,
            conversation_status=data['conversation_status_yes'],
            playbook_id=0
        )
        # if data['conversation_status_yes'] and data['context']:
        #     new_data.context = data['context']

        #     for value in ques_node:
        #         context_data = ContextGroupModel(
        #                 website_id=data['website_id'],
        #                 context_name=data['context'],
        #                 sentence=value['sentence'],
        #                 response_type=value['response_type'],
        #                 success_msg=data['success_response'],
        #                 status=1
        #         )
        #         context_data.save_to_db()
        #         last_data = ContextGroupModel.get_last(data['context'],data['website_id'])
        #         data_details = ContextGroupModel.to_json(last_data)
                
        #         for opt in value['options']:
        
        #             context_gp_data = ContextGroupDetailsModel(
        #                     context_gp_id=data_details['context_gp_id'],
        #                     response=opt['response'],
        #                     status=1
        #             )
        #             context_gp_data.save_to_db()
        try:
            db.db.session.rollback()            
            new_data.save_to_db()

            
            return {
                    'status': 200,
                    'success': True,
                    'message': 'question has been created '
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the questions',
            }, 400
            

class QuestionPublish(Resource):
    
    @jwt_required
    def post(self):

        current_user = get_jwt_identity()
        data = parser.parse_args()
       
        result = QuestionModel.publish_all(data, current_user)             
        
        return result

class QuestionPublishCheck(Resource):
    
    # @jwt_required
    def post(self):

        params = parser.parse_args()
        website_id = params['website_id']

        data = True
        try:
            db.db.session.rollback() 
            result = QuestionModel.check_question_changes(website_id)     

            if result:
                data = False

            return {
                    'status': 200,
                    'success': True,
                    'message': 'Question changes has been updated',
                    'data': data
                }, 200
                
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the questions',
            }, 400

        
        
class CrawlerToQns(Resource):
    

    @jwt_required
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('data', required=False, type=dict, help='This field cannot be left blank')              
        parser.add_argument('category_id', help = 'This field cannot be blank', required = True)

        current_user = get_jwt_identity()

        userId = current_user['user_id']

        params = parser.parse_args()
        category_id = params['category_id']

        req_data = request.get_json()

        data = req_data['data']        

        try:
            for value in data:            
               
                new_data = QuestionModel(
                    user_id=userId,
                    class_name=value['class_name'],
                    website_id=value['website_id'],
                    sentence=value['sentence'],
                    category_id=category_id,
                    response=value['response'],
                    image_link=value['image_link'],
                    audio_link=value['audio_link'],
                    video_link=value['video_link'],
                    href_link=value['href_link'],
                    status=3,
                    publish_status=1,
                    playbook_id=0
                )
                db.db.session.rollback()            
                new_data.save_to_db()
            
            return {
                    'status': 200,
                    'success': True,
                    'message': 'crawler question has been moved to questions'
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred while moving the questions',
            }, 400




