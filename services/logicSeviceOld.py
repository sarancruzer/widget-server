import nltk
import json
#from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
from models.question import QuestionModel
# word stemmer
stemmer = LancasterStemmer()

sample = []
training_data = []

# print(y)
corpus_words = {}
class_words = {}
all_tags = []


def out_fun():
    with open('jsons/claritaz.json', 'r') as f:
        training_data = json.load(f)
        #print ("%s sentences in training data" % len(training_data))
        return training_data

# y = out_fun()

# classes = list(set([a['class'] for a in y]))
# for c in classes:
#     class_words[c] = []
# print(len(class_words)) 

# print ("%s sentences of training data" % len(y))

# loop through each sentence in our training data
# for data in y:
#     # tokenize each sentence into words
#     for word in nltk.word_tokenize(data['sentence']):
#         # ignore a some things
#         if word not in ["?", "'s"]:
#             # stem and lowercase each word
#             stemmed_word = stemmer.stem(word.lower())
#             # have we not seen this word already?
#             if stemmed_word not in corpus_words:
#                 corpus_words[stemmed_word] = 1
#             else:
#                 corpus_words[stemmed_word] += 1

#             # add the word to our words in class list
#             class_words[data['class']].extend([stemmed_word])
            

# we now have each stemmed word and the number of occurances of the word in our training corpus (the word's commonality)
# print ("Corpus words and counts: %s \n" % corpus_words)
# also we have all words in each class
# print ("Class words: %s" % class_words)

print(len(class_words))


#Test Question - "leave code for Maternity Leave"------('V5TSRKFH')
#Who is the Production Associate Grade - OCC0PSFM
# calculate a score for a given class taking into account word commonality
# sentence = "What is the address?"
def calculate_class_score_commonality(sentence, class_name, show_details=True):
    score = 0
    # tokenize each word in our new sentence
    for word in nltk.word_tokenize(sentence):
        # check to see if the stem of the word is in any of our classes
        if stemmer.stem(word.lower()) in class_words[class_name]:
            # treat each word with relative weight
            score += (1 / corpus_words[stemmer.stem(word.lower())])

            if show_details:
                pass
                # print ("   match: %s (%s)" % (stemmer.stem(word.lower()), 1 / corpus_words[stemmer.stem(word.lower())]))
    return score

# now we can find the class with the highest score
# for c in class_words.keys():
#     print ("Class: %s  Score: %s \n" % (c, calculate_class_score_commonality(sentence, c)))
   

# return the class with highest score for sentence
def classify(sentence):
    high_class = None
    high_score = 0

    y = out_fun()
    
    classes = list(set([a['class'] for a in y]))
    for c in classes:
        class_words[c] = []
    print(len(class_words))

    print ("%s sentences of training data" % len(y))

    for data in y:
        # tokenize each sentence into words
        for word in nltk.word_tokenize(data['sentence']):
            # ignore a some things
            if word not in ["?", "'s"]:
                # stem and lowercase each word
                stemmed_word = stemmer.stem(word.lower())
                # have we not seen this word already?
                if stemmed_word not in corpus_words:
                    corpus_words[stemmed_word] = 1
                else:
                    corpus_words[stemmed_word] += 1
                    
            # add the word to our words in class list
            class_words[data['class']].extend([stemmed_word])

    # loop through our classes
    for c in class_words.keys():

        # calculate score of sentence for each class
        score = calculate_class_score_commonality(sentence, c, show_details=False)
        # keep track of highest score
        if score > high_score and score > 0.01:
            high_class = c
            high_score = score
    
    resList = responses(high_class,y)

    # return high_class, high_score
    return resList



def askqFunc(sentences):
    print("sentence")
    print(sentences)

    x = classify(sentences)
    print("x")
    print(x)
    return x



# def response(class_val,y):
#     rList = {'class':'','sentence':'','responses':'','question':[]}
#     if(class_val != None):
#         for value in y:
#             if(class_val == value['class']):
#                 rList['class'] = value['class']
#                 rList['sentence'] = value['sentence']
#                 rList['responses'] = value['responses']
#     else:
#         print('----- else called ------')
#         rList = {'class':'','sentence':'','responses':'I am not sure I understand. Here are the suggestions I have.','question':[]}
#         rList['question']=['Norms for casual leave','Insurance benefits for employees','Norms for Mispunch','Provident Fund Details']
#     print('-- Response print-------')
#     print(rList)
#     return rList


def responses(class_val,y):
    rList = {'CLASS':'','QUESTION':'','HREF_LINK':'', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': '', 'RAW_TEXT': '', 'SUGGESTIONS_TEXT': []}

    if(class_val != None):
        for value in y:
            if(class_val == value['class']):
                rList['CLASS'] = value['class']
                
                res = QuestionModel.get_by_id(value['class'])
                print("RESS")
                print(res)
                qq = res['questions'][0]
                print(qq)

                rList['QUESTION'] = qq['sentence']
                rList['RAW_TEXT'] = qq['response']
                rList['HREF_LINK'] = qq['href_link']
                rList['AUDIO_LINK'] = qq['audio_link']
                rList['VIDEO_LINK'] = qq['video_link']
                rList['IMAGE_LINK'] = qq['image_link']
                rList['SUGGESTIONS_TEXT'] = []
                
                return rList



    else:
        print('----- else called ------')
        suggests=['About your company','Do you have any wallkin','Whether it is startup company','Company benefits for employees']

        rList = {'CLASS':'','QUESTION':'','HREF_LINK':'https://www.claritaz.com/who-we-are.html', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': 'uploads/claritaz/Screenshot%20from%202018-09-26%2011-14-28.png', 'RAW_TEXT': 'I am not sure I understand. Here are the suggestions I have.', 'SUGGESTIONS_TEXT': suggests}        
        
    print('-- Response print-------')
    print(rList)
    return rList

