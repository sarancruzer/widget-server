from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from flask import request
from models.userAssignWebsite import UserAssignWebsiteModel
from models.question import QuestionModel
from models.subscription import SubscriptionModel
from models.package import PackageModel
from shared.util import custom_paginate
from models.user import UserModel
from shared.mailer import sendWidgetSCriptcode
from flask.templating import render_template
from models.website import WebsiteModel
from shared.mailer import contactFormMail

class DashboardList(Resource):
  
    @jwt_required
    def post(self):
        parser = reqparse.RequestParser()

        parser.add_argument('website_id', required=True, help='This field cannot be left blank')
        parser.add_argument('company_id', required=True, help='This field cannot be left blank')

        data = parser.parse_args()


        current_user = get_jwt_identity()

        website_id = current_user['website_id']
        company_id = current_user['company_id']

        operator_count = UserAssignWebsiteModel.get_by_operator_count(current_user['website_id'])
        
        question_count = QuestionModel.get_by_question_count(current_user['website_id'])

        Package_details = SubscriptionModel.get_by_company_id(current_user['company_id'])
        data = SubscriptionModel.to_json(Package_details)  
        
        Pack_det = PackageModel.get_by_id(data['package_id'])
        items = PackageModel.to_json(Pack_det)  


        no_of_operators = operator_count
        no_of_questions = question_count
        pack_start_at = data['pack_start_at']
        pack_end_at = data['pack_end_at']
        package_name = items['package_name']
        package_duration = items['package_duration']


        return {
               'status': 200,
               'success': True,
               'message': 'details received',
               'data': [{"no_of_operators":no_of_operators,
                "no_of_questions":no_of_questions,
                "pack_start_at":pack_start_at,
                "pack_end_at":pack_end_at,
                "package_name":package_name,
                "package_duration":package_duration}]
                 }, 200

        return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404


class OperatorsList(Resource):

    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        website_id = request.args.get('website_id', '')

        query, params = UserAssignWebsiteModel.get_operator_by_website_id(website_id)
        per_page = 10
        result, cus_paging = custom_paginate(query, page, per_page,params)      

        
        data = list(map(lambda x: UserModel.to_customjson(x), result))

        return {
                'status': 200,
                'success': True,
                'message': 'Operators received lists',
                'data':data
                }, 200  


class SendmailWidgetcode(Resource):

    @jwt_required
    def post(self):

        current_user = get_jwt_identity()
        
        page = request.args.get('page', 1)

        parser = reqparse.RequestParser()

        parser.add_argument('sender_address', required=True, help='This field cannot be left blank')
        parser.add_argument('receiver_address', required=True, help='This field cannot be left blank')

        parser.add_argument('subject', required=True, help='This field cannot be left blank')
        parser.add_argument('message', required=True, help='This field cannot be left blank')

        data = parser.parse_args()
     
        req_data = request.get_json()
        receiver_address = req_data['receiver_address']
        sender_address = req_data['sender_address']
        subject = req_data['subject']
     

        current_user = get_jwt_identity()
        website_id = current_user['website_id']
        

        result = WebsiteModel.get_by_id(website_id)

        data = WebsiteModel.to_json(result)

        sendWidgetSCriptcode(receiver_address,sender_address,subject, render_template('widgetCopycode.html',data = data), data)


        return {
                'status': 200,
                'success': True,
                'message': 'mail sent!',
                }, 200  
 

class SendmailContactForm(Resource):

    def post(self):

        parser = reqparse.RequestParser()

        parser.add_argument('name', required=True, help='This field cannot be left blank')
        parser.add_argument('email', required=True, help='This field cannot be left blank')
        parser.add_argument('message', required=True, help='This field cannot be left blank')

        data = parser.parse_args()
        print("===============================")
        print(data)
        print("===============================")
      
        contactFormMail("Claribot Enquiry Form", render_template('contactForm.html',data = data), data)

        try:
            return {
                    'status': 200,
                    'success': True,
                    'message': 'mail sent',
                    }, 200  
                   
        
        except:
            
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred while sending mail',
            }, 400




    