from models.widgetLayout import WidgetLayoutModel

def createDefaultWidgetComponents(company_id,website_id):
    try:
        new_widget = WidgetLayoutModel(
        company_id = company_id,
        website_id = website_id,
        logo = "uploads/logo/logo.png",  
        primary_color = "#11dad1",
        user_icon_path = "uploads/icons/user/user1.png",
        bot_icon_path = "uploads/icons/bot/bot1.png",
        bot_header_background = "#fff",
        bot_body_background = "#ddd",
        user_msg_foreground = "#000",         
        user_msg_background = "#eff5fb",
        bot_msg_foreground = "#000",         
        bot_msg_background = "#fff",         
        screen_open_default = 1,
        status = 1
    )

        new_widget.save_to_db()

        return True

    except Exception as e:
        print(e)
        return False


        
