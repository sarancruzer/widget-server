from shared.db import db
from sqlalchemy import Column, Integer, Text
from sqlalchemy.sql.sqltypes import DATETIME, String
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from flask_sqlalchemy import Pagination
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity


class ChatHistoryModel(db.Model):

    __tablename__ = 'chat_history'

    chat_resp_id = Column('chat_resp_id',Integer, primary_key=True)
    company_id = Column('company_id', Integer, nullable = False)
    website_id = Column('website_id', Integer, nullable = False)
    user_token = Column('user_token', Text, nullable = False)
    name = Column('name', Text, nullable = False)
    email = Column('email', Text, nullable = False)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)

    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 



    @classmethod
    def to_json(cls, x):
            return {
                'chat_resp_id': x.chat_resp_id,
                'company_id':x.company_id,
                'website_id': x.website_id,
                'user_token': x.user_token,
                'name': x.name,
                'email': x.email,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def get_by_id(cls, chat_resp_id):        
        return cls.query.filter_by(chat_resp_id=chat_resp_id).first()

    @classmethod
    def return_all(cls, page, searchTerm,website_id,log_count):               
        page = int(page)
       
        query = "SELECT q.*,c.website_id FROM chat_history as q LEFT JOIN website as c ON c.website_id = q.website_id  where q.created_at >= DATE_SUB(CURDATE(), INTERVAL "+str(log_count)+" WEEK) AND q.website_id = " + str(website_id)  

        if searchTerm:
            query = query + " AND (q.name LIKE :search_term OR q.email LIKE :search_term)"
        query = query +" ORDER BY q.updated_at DESC"
        params = { 'search_term' : '%'+ str(searchTerm) + '%'}

        return  query, params
        

    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}
    
    @classmethod
    def update_by_date(cls,id,date):
        date_query = "UPDATE chat_history SET updated_at='"+date+"' WHERE chat_resp_id="+ id
        res = db.engine.execute(date_query) 
        return  res
      

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
