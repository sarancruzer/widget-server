from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref



class CrawlerModel(db.Model):
    __tablename__ = 'crawler'

    crawler_id = Column('crawler_id',Integer, primary_key=True)
    company_id = Column("company_id",Integer, nullable = True)    
    website_id = Column('website_id', Integer, nullable = True) 
    domain = Column('domain', Text)
    subdomain = Column('subdomain', Text)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    

    @classmethod
    def find_by_id(cls, crawler_id):        
        print(crawler_id)       
        return { 'crawler': list(map(lambda x: CrawlerModel.to_json(x), cls.query.filter_by(crawler_id=crawler_id).all())) }

    @classmethod
    def get_by_id(cls, crawler_id):        
        print(crawler_id)       

        return cls.query.filter_by(crawler_id=crawler_id).first()

    @classmethod
    def find_by_domain(cls, domain):
        return cls.query.filter_by(domain = domain).first()


    @classmethod
    def get_by_domain(cls, domain, usertoken):        
        return {
            'crawler': list(map(lambda x: CrawlerModel.to_json(x), CrawlerModel.query.filter_by(domain = domain, user_token = usertoken, status = 0).all()))
        }

    
    @classmethod
    def get_by_website_id(cls, website_id):
        return cls.query.filter_by(website_id = website_id).first()

    
       
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    


    @classmethod
    def to_json(cls, x):
            return {
                'crawler_id':x.crawler_id,
                'company_id':x.company_id,
                'website_id':x.website_id,
                'domain':x.domain,
                'subdomain':x.subdomain,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
        
   
    @classmethod
    def return_all(cls, page, searchTerm):        
        page = int(page)
        
        db.session.commit()
        data = db.session.query(CrawlerModel).filter(CrawlerModel.domain.like("%" + searchTerm + "%")).paginate(page=page, per_page=10)

        return data
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()            
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

