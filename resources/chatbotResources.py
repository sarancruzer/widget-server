from flask_restful import Resource, reqparse

from services.automateTestService import automateTestAskQFunc, automateImportData
from services.chatbotService import askqFunc, readLogs
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from models.packageDetails import PackageDetailsModel
from models.packResponseCount import PackResponseCountModel
from shared import db
import nltk
import autocorrect
from autocorrect import spell
import re
from flask import request

from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")

from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

parser = reqparse.RequestParser()

parser.add_argument('sentence', help = 'This field cannot be blank', required = False)
parser.add_argument('api_key', help = 'This field cannot be blank', required = False)


class AskQ(Resource):    
    def post(self):

        parser.add_argument('sentence', help = 'This field cannot be blank', required = False)
        parser.add_argument('api_key', help = 'This field cannot be blank', required = False)
        parser.add_argument('context', help = 'This field cannot be blank', required = False)
        parser.add_argument('username', help = 'This field cannot be blank', required = False)
        parser.add_argument('context_token', help = 'This field cannot be blank', required = False)
        parser.add_argument('response', help = 'This field cannot be blank', required = False)
        parser.add_argument('context_res_id', help = 'This field cannot be blank', required = False)
        parser.add_argument('user_token', help = 'This field cannot be blank', required = False)
        parser.add_argument('chat_history_id', help = 'This field cannot be blank', required = False)
        parser.add_argument('interest_category', help = 'This field cannot be blank')
        parser.add_argument('playBook_id', help = 'This field cannot be blank', required = False)
        parser.add_argument('selected_nodeType', help = 'This field cannot be blank', required = False)
        parser.add_argument('conversation_sequence', help = 'This field cannot be blank', required = False)
        parser.add_argument('inner_node', help = 'This field cannot be blank', required = False)
        parser.add_argument('user_input_status', help = 'This field cannot be blank', required = False)
        parser.add_argument('node_id', help = 'This field cannot be blank', required = False)
        parser.add_argument('branch_id', help = 'This field cannot be blank', required = False)


        data = parser.parse_args()
        sentence = data['sentence']
        print("OOOOOOPPPPPPPPT Deatil11111111")
        if data['playBook_id'] and (data['selected_nodeType'] == "3" or data['selected_nodeType'] == "4"):
            print("OOOOOOPPPPPPPPT Deatil222222222")
            node = request.get_json(silent=True)
            option_node = node['selected_option']
            data['selected_option'] = option_node
            print(option_node)
            
        # tokenize the pattern
        sentence_words = nltk.word_tokenize(sentence)
        # remove punctuation and split into seperate words
        #sentence_words = re.findall(r'\w+', sentence, flags=re.UNICODE | re.LOCALE) 
        
        # autocorrect to remove the user input wrong spell
        #sentence_words = [autocorrect.spell(word) for word in sentence_words]

        oneString = ' '.join(sentence_words)
        # stem each word
        # sentence_words = [stemmer.stem(word.lower()) for word in sentence_words]
        # lemmatize words
        # sentence_words = [lemmatizer.lemmatize(word) for word in sentence_words]
        # stop words removal from user input
        # sentence_words = [word for word in sentence_words if not word in stop_words]
        data['sentence'] = oneString
        print("QQQQQQQQQQQQUUUUUUUUUUSSSSSSSSS")
        #print(option_node)
        res = askqFunc(data)
        return res


# class AskQQ(Resource):
#     def post(self):

#         data = parser.parse_args()
#         sentence = data['sentence']
#         apikey = data['api_key']      
#         res = askqFuncCall(sentence,apikey)
#         return res

class AutomateTestAskQ(Resource):
    def post(self):
        
        data = parser.parse_args()
        id = ''
        res = automateTestAskQFunc(id)
        return res


class AutomateImportJsonData(Resource):
    def post(self):
        res = automateImportData()
        return res


class Logs(Resource):
    def post(self):
        
        data = parser.parse_args()
        api_key = data['api_key']

        result = readLogs(api_key)

        return {
            'status': 200,
            'success': True,
            'message': 'Logs received',
            'data':result
            }, 200


        

