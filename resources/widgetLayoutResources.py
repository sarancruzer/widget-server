from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from models.website import WebsiteModel

import uuid

from flask import request
from shared.util import custom_paginate

import random
import string
from models.widgetLayout import WidgetLayoutModel
from models.icon import IconModel
import base64
from shared.db import db
import binascii
import os


UPLOAD_FOLDER_LOGO = 'uploads/logo/'


parser = reqparse.RequestParser()
parser.add_argument('website_id', required=False, help='This field cannot be left blank')



class WidgetComponent(Resource):
    
    @jwt_required
    def get(self,id):

        website_id = id

        item = WidgetLayoutModel.find_by_website_id(website_id)

        result = {}

        if item:
            result = WidgetLayoutModel.to_json(item)
            
        if result:
            return {
                'status': 200,
                'success': True,
                'message': 'widget received',
                'data': result
            }, 200

        else:
            return {
                'status': 400,
                'success': False,
                'message': 'widget not found',
            }, 400

            
class WidgetComponentList(Resource):
    
    @jwt_required
    def post(self):

        parser.add_argument('website_id', required=False, help='This field cannot be left blank')
        parser.add_argument('logo', required=False, help='This field cannot be left blank')
        parser.add_argument('primary_color', required=False, help='This field cannot be left blank')
        parser.add_argument('user_icon_path', required=False, help='This field cannot be left blank')
        parser.add_argument('bot_icon_path', required=False, help='This field cannot be left blank')
        parser.add_argument('bot_header_background', required=False, help='This field cannot be left blank')
        parser.add_argument('bot_body_background', required=False, help='This field cannot be left blank')
        parser.add_argument('user_msg_foreground', required=False, help='This field cannot be left blank')
        parser.add_argument('user_msg_background', required=False, help='This field cannot be left blank')
        parser.add_argument('bot_msg_foreground', required=False, help='This field cannot be left blank')
        parser.add_argument('bot_msg_background', required=False, help='This field cannot be left blank')
        parser.add_argument('screen_open_default', required=False, help='This field cannot be left blank')
        parser.add_argument('bot_heading_text', required=False, help='This field cannot be left blank')


        data = parser.parse_args()

        current_user = get_jwt_identity()
        new_data = WidgetLayoutModel.find_by_website_id(data['website_id'])        
        file_path = data['logo']
        file_name = str(uuid.uuid4().hex[:8]) + '.png'
        logo = data['logo'].split(',')
        if len(logo) > 1:
            imgdata = base64.b64decode(logo[1])   

            file_path = os.path.join(UPLOAD_FOLDER_LOGO,file_name)
            with open(file_path, 'wb') as f:
                f.write(imgdata)
        else:
            print("not decoded")

        
        if new_data is None:

            new_data = WidgetLayoutModel(
                company_id = current_user['company_id'],
                website_id = data['website_id'],
                logo = file_path,  
                primary_color = data['primary_color'],
                user_icon_path = data['user_icon_path'],
                bot_icon_path = data['bot_icon_path'],
                bot_header_background = data['bot_header_background'],
                bot_body_background = data['bot_body_background'],
                user_msg_foreground = data['user_msg_foreground'],         
                user_msg_background = data['user_msg_background'],
                bot_msg_foreground = data['bot_msg_foreground'],         
                bot_msg_background = data['bot_msg_background'],  
                bot_heading_text=data['bot_heading_text'],       
                screen_open_default = 1,
                status = 1,
                modified_by = current_user['user_id']
            )           

        else:
            new_data.company_id = current_user['company_id']
            new_data.website_id = data['website_id']
            new_data.logo = file_path  
            new_data.primary_color = data['primary_color']
            new_data.user_icon_path = data['user_icon_path']
            new_data.bot_icon_path = data['bot_icon_path']
            new_data.bot_header_background = data['bot_header_background']
            new_data.bot_body_background = data['bot_body_background']
            new_data.user_msg_foreground = data['user_msg_foreground']         
            new_data.user_msg_background = data['user_msg_background']
            new_data.bot_msg_foreground = data['bot_msg_foreground']         
            new_data.bot_msg_background = data['bot_msg_background']   
            new_data.bot_heading_text = data['bot_heading_text']      
            new_data.screen_open_default = 1,
            new_data.status = 1,
            new_data.modified_by = current_user['user_id']
                     

        try:
            new_data.save_to_db()


            return {
                'status': 200,
                'success': True,
                'message': 'widget layout has been updated',
                'data': WidgetLayoutModel.to_json(new_data)
            }, 200
        except Exception as e:
            # db.session.rollback()            
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the widget layout',
            }, 400

class WidgetIcons(Resource):

    @jwt_required
    def post(self):

        user_icons = IconModel.get_user_icons()     

        bot_icons = IconModel.get_bot_icons()     

        result = {}

        result['bot_icons'] = bot_icons['bot_icons']
        result['user_icons'] = user_icons['user_icons']


        if user_icons:
            return {
                'status': 200,
                'success': True,
                'message': 'widget icons received',
                'data': result
            }, 200

        else:
            return {
                'status': 400,
                'success': False,
                'message': 'widget icons not found',
            }, 400
            


