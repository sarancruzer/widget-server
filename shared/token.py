# from itsdangerous import URLSafeTimedSerializer
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

import app


# def generate_confirmation_token(email):
#     serializer = URLSafeTimedSerializer(app.app.config['SECRET_KEY'])
#     return serializer.dumps(email, salt=app.app.config['SECURITY_PASSWORD_SALT'])


# def confirm_token(token, expiration=6):
#     serializer = URLSafeTimedSerializer(app.app.config['SECRET_KEY'])
#     try:
#         email = serializer.loads(
#             token,
#             salt=app.app.config['SECURITY_PASSWORD_SALT'],
#             max_age=expiration
#         )
#     except:
#         return False
#     return email


def generate_confirmation_token(email):
    s = Serializer(app.app.config['SECRET_KEY'], expires_in=app.app.config['SECURITY_PASSWORD_EXPIRES'])
    return s.dumps(email,salt=app.app.config['SECURITY_PASSWORD_SALT'])
    


def confirm_token(token, expiration=120):
    expiration = app.app.config['SECURITY_PASSWORD_EXPIRES']
    s = Serializer(app.app.config['SECRET_KEY'], expires_in=expiration)
    try:
        email = s.loads(
            token,
            salt=app.app.config['SECURITY_PASSWORD_SALT'])
    except:
        return False
    return email



def generate_count_token(email):
    # expiration = app.app.config['SECURITY_PASSWORD_EXPIRES']
    expiration = 600
    s = Serializer(app.app.config['SECRET_KEY'], expires_in=expiration)
    return s.dumps(email,salt=app.app.config['SECURITY_PASSWORD_SALT'])
    


def count_token(token, expiration=120):
    # expiration = app.app.config['SECURITY_PASSWORD_EXPIRES']
    expiration = 600
    s = Serializer(app.app.config['SECRET_KEY'], expires_in=expiration)
    try:
        email = s.loads(
            token,
            salt=app.app.config['SECURITY_PASSWORD_SALT'])
    except:
        return False
    return email



