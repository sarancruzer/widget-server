from flask_sqlalchemy import Pagination
from flask_restful import abort
from shared.db import db
import math
from sqlalchemy.sql.expression import text



def sqlalchemy_paginate(params):
    
    print("params")
    print(params)


    paginateJson = {
        'total' : params.total,
        'pages' : params.pages,
        'has_next' : params.has_next,
        'has_prev' : params.has_prev,
        'next_page' : params.next_num,
        'prev_page' : params.prev_num,
    }
    # total = params.total
    # items = params.items
    # pages = params.pages
    # has_next = params.has_next
    # has_prev = params.has_prev
    # next_page = params.next_num
    # prev_page = params.prev_num


    
    return paginateJson



def custom_paginate(query,page,per_page,params):

    #  which page is being requested. if none specified - assume first page
    current_page = int(page)
    #  number of items to display per page
    items_per_page = per_page
    #  where should the query start from
    start_index = (current_page - 1) * items_per_page

    # params = {'search_term': '@aa'}
    
    query = text(query)
    #  total items
    results = db.engine.execute(query,params)
    total_items = len(results.fetchall())


    print("RESULTS")
    print(total_items)
    
    #  total pages. round it to the upper limit.
    total_pages = math.ceil(total_items / items_per_page)


    #  items for the current page
    query = str(query) + " LIMIT " + str(start_index) + ", " + str(items_per_page)

    print("qqqqqqqqqqqqqqqqqqqqqqq")
    print(query)

    query = text(query)

    results = db.engine.execute(query,params)

    print("rrrrrrrrrrrrrrrrrrrrrrrrrrr")
    print(results)

    next_page = current_page + 1
    prev_page = current_page - 1

    if current_page < total_pages:
        has_next = True
    else:
        has_next = False
    
    if prev_page < 1:
        has_prev = False
    else:
        has_prev = True

    paginateJson = {
        'total' : total_items,
        'pages' : total_pages,
        'has_next' : has_next,
        'has_prev' : has_prev,
        'next_page' : next_page,
        'prev_page' : prev_page,
    }

    
    
    return results, paginateJson

