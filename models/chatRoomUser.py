from shared.db import db
from sqlalchemy import Table, Column, Integer, String, Date, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text


class ChatRoomUsersModel(db.Model):
    __tablename__ = 'chat_rooms_users'

    room_user_id = Column(Integer, primary_key=True)
    user_id = Column('user_id', Integer)
    room_id = Column('room_id', Integer)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True,server_default=text('0'))
    

    @classmethod
    def get_all_rooms(cls, website_id):        
        query = "SELECT cr.*,cru.user_id FROM chat_rooms_users as cru LEFT JOIN chat_rooms as cr ON cr.room_id = cru.room_id WHERE cr.website_id=" + website_id
        res = db.engine.execute(query) 
        return res

    @classmethod
    def get_by_roomid(cls, room_id):
        return cls.query.filter_by(room_id = room_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush()
        return
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
    
    @classmethod
    def to_json(cls, x):
            return {
                'room_id': x.room_id,
                'user_id': x.user_id,
                'company_id': x.company_id,
                'website_id': x.website_id,
                'room_name': x.room_name,
                'room_code': x.room_code,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    
