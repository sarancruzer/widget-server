from flask_restful import Resource, reqparse
from models.website import WebsiteModel
from models.chatRoom import ChatRoomsModel
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from flask import json
from models.chatHistoryDetails import ChatHistoryDetailsModel
import re
from flask.templating import render_template
from shared.mailer import chatReqMail
from flask import request
from models.userAssignWebsite import UserAssignWebsiteModel
from models.user import UserModel
from shared.util import custom_paginate



class RoomAvailable(Resource):    
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('api_key', help = 'This field cannot be blank', required = False)
        parser.add_argument('name', help = 'This field cannot be blank', required = False)
        parser.add_argument('email', help = 'This field cannot be blank', required = False)
        parser.add_argument('chat_history_id', help = 'This field cannot be blank', required = False)

                
        try:

            data = parser.parse_args()

            websiteList = WebsiteModel.get_by_apikey(data['api_key'])
            website_data = WebsiteModel.to_json(websiteList)


            chatroomList = ChatRoomsModel.get_available_rooms(website_data['website_id'])

            if chatroomList is None:
                return {
                'status': 400,
                'success': False,
                'message': 'Please wait sometime agents are busy rightnow!',
                }, 400
            else:
               
                chatroom_data = ChatRoomsModel.to_json(chatroomList)
         
                get_room_data = ChatRoomsModel.find_by_id(chatroom_data['room_id'])

                get_room_data.name =  data['name'],
                get_room_data.email =  data['email'],
                get_room_data.chat_history_id =  data['chat_history_id'],
                get_room_data.save_to_db()

               
                return {
                    'status': 200,
                    'success': True,
                    'message': 'Chatroom list received',
                    'data': chatroom_data
                    }, 200
                

        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred available room ',
            }, 400



class SendMailOperator(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('api_key', help = 'This field cannot be blank', required = False)
        parser.add_argument('name', help = 'This field cannot be blank', required = False)
        parser.add_argument('email', help = 'This field cannot be blank', required = False)
        parser.add_argument('chat_history_id', help = 'This field cannot be blank', required = False)

                
        try:

            data = parser.parse_args()
            print(data)        

            websiteList = WebsiteModel.get_by_apikey(data['api_key'])
            website_data = WebsiteModel.to_json(websiteList)
            chatroomList = ChatRoomsModel.get_available_rooms(website_data['website_id'])

            chatroom_data = ChatRoomsModel.to_json(chatroomList)

            website_id = website_data['website_id']
            page = request.args.get('page', 1)
            query, params = UserAssignWebsiteModel.get_operator_by_website_id(website_id)
            per_page = 10
            result, cus_paging = custom_paginate(query, page, per_page,params)
                        
            operatordata = list(map(lambda x: UserModel.to_json(x), result))
            print("===============================================================")
            print(operatordata)
            print("===============================================================")
            for value in operatordata:
                print("88888888888888888888888888888888")
                print(value['email'])
                print(value['name'])

                receiver_address = value['email']
                receiver_name = str(value['name'])
                room_name = chatroom_data['room_name']

               
                print("receiver_address =================================")
                print(receiver_address)
                chatReqMail("Chat Request Mail", render_template('chatReq.html',data = data), data,receiver_address,room_name,receiver_name)

            return {
                'status': 200,
                'success': True,
                'message': 'Operator mail sent'
                }, 200
                    

        # except:
        except Exception as e:
            print(e)
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred available room ',
            }, 400

class GetAllRooms(Resource):    
    
    @jwt_required
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('website_id', help = 'This field cannot be blank', required = False)

        current_user = get_jwt_identity()

        try:

            data = parser.parse_args()

            chatroomList = ChatRoomsModel.get_all_rooms(data['website_id'])


            chatroomjoinedList = ChatRoomsModel.get_all_joined_rooms(data['website_id'])

            if chatroomList is None:
                return {
                'status': 400,
                'success': False,
                'message': 'There is no rooms in this website!',
                }, 400

            else:
                chatroom_joined_data = list(map(lambda x: ChatRoomsModel.to_json(x), chatroomjoinedList))
                chatroom_data = ChatRoomsModel.to_json(chatroomList)              

                room_data = []
                for value in chatroom_joined_data:

                    room_data.append(value)
                
                room_data.append(chatroom_data)
                
                return {
                    'status': 200,
                    'success': True,
                    'message': 'Chatroom list received',
                    'data': room_data
                    }, 200
                

        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred all rooms'+ str(e),
            }, 400


class GetActiveRooms(Resource):    
    
    @jwt_required
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('website_id', help = 'This field cannot be blank', required = False)

        current_user = get_jwt_identity()

                
        try:

            data = parser.parse_args()

            chatroomList = ChatRoomsModel.get_active_rooms(data['website_id'],current_user['user_id'])
          
            if chatroomList is None:
                return {
                'status': 400,
                'success': False,
                'message': 'There is no active rooms in this website!',
                }, 400
            else:
                chatroom_data = list(map(lambda x: ChatRoomsModel.to_customjson(x), chatroomList))
                print("MMMMMMAAAAAAAAAAAANNNNNNNNNNNN")
                print(len(chatroom_data))
                for value in chatroom_data:

                    value['chats'] = re.sub('"user":"true"', '"user1":"true"', value['chats'])
                    value['chats'] = re.sub('"machine":"true"', '"user":"true"', value['chats'])
                    value['chats'] = re.sub('"user1":"true"', '"machine":"true"', value['chats'])

                return {
                    'status': 200,
                    'success': True,
                    'message': 'Chatroom active room list received',
                    'data': chatroom_data
                    }, 200
                

        # except:
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred active rooms',
            }, 400


