from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref



class PlayBookReplyOptionModel(db.Model):
    __tablename__ = 'playbk_reply_opt'

    playbk_reply_opt_id = Column('playbk_reply_opt_id',Integer, primary_key=True,autoincrement=True)
    playbook_id = Column('playbook_id',Integer, nullable = False)
    node_id = Column('node_id', Integer, nullable = False) 
    option_value = Column('option_value', String(100), nullable = False) 
    option_value_id = Column('option_value_id', Integer, nullable = False) 
    branch_id = Column('branch_id',Integer)
    is_branch = Column('is_branch',Integer)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return

    @classmethod
    def find_last_added_record(cls, playBook_id,response,node_id):
        return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id,option_value=response).first()
    
    @classmethod
    def get_by_playBook_id(cls, playBook_id):
        return cls.query.filter_by(playbook_id = playBook_id).all()

    @classmethod
    def get_option_node(cls,playBook_id,node_id,types,branch,ids):
        print("GGETTTTTTTTT OOPPPPPPPPPPPPPPPPTTTTTTTTTTTT")
        print(types)
        print("=========")
        if types == '4':
            return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id,is_branch=1).all()
        else:
            if branch!=0:
                return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id,branch_id=ids).all()
            else:
                return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id).all()

    @classmethod
    def get_option_node_branch(cls,playBook_id,node_id,opt_id):
        return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id,branch_id=opt_id).all()
    
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def to_json(cls, x):
            return {
                'playbk_reply_opt_id':x.playbk_reply_opt_id,
                'playbook_id':x.playbook_id,
                'node_id': x.node_id,
                'option_value':x.option_value,
                "option_value_id":x.option_value_id,
                "is_branch":x.is_branch,
                'branch_id':x.branch_id,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
   

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
