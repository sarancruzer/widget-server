from shared.db import db

from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref



class CategoryModel(db.Model):
    __tablename__ = 'category'

    category_id = Column('category_id',Integer, primary_key=True)
    website_id = Column('website_id',Integer, nullable = False)
    category_name = Column('category_name', String(100), nullable = False) 
    created_by = Column('created_by', Integer, nullable = True) 
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    
    @classmethod
    def get_by_website_id(cls, website_id):      


        return cls.query.filter_by(website_id=website_id).all()


    @classmethod
    def get_by_category(cls, website_id):      
        print("category website_id")   
        print(website_id)       

        return{
            'category': list(map(lambda x: CategoryModel.to_json_custom(x), CategoryModel.query.filter_by(website_id=website_id).all()))

        }     

    @classmethod
    def find_by_id(cls, category_id):        
        return { 'category': list(map(lambda x: CategoryModel.to_json(x), cls.query.filter_by(category_id=category_id).all())) }

    @classmethod
    def get_by_id(cls, category_id):        


        return cls.query.filter_by(category_id=category_id).first()

    @classmethod
    def find_by_name(cls, category_name,website_id):
        return cls.query.filter_by(category_name = category_name,website_id=website_id).first()


    @classmethod
    def get_by_categoryname(cls, categoryname, usertoken):
        
        return {
            'category': list(map(lambda x: CategoryModel.to_json(x), CategoryModel.query.filter_by(category_name = categoryname, user_token = usertoken, status = 0).all()))
        }


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    


    @classmethod
    def to_json(cls, x):
            return {
                'category_id':x.category_id,
                'website_id':x.website_id,
                'category_name': x.category_name,
                'created_by': x.created_by,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    @classmethod
    def to_json_custom(cls, x):
            return {
                'category_id':x.category_id,
                'category_name': x.category_name
                                 
                }       
    
       
    @classmethod
    def return_all(cls, page, searchTerm,website_id):        
        page = int(page)
        
        db.session.rollback()

        data = db.session.query(CategoryModel).filter_by(website_id=website_id).filter(CategoryModel.category_name.like("%" + searchTerm + "%")).paginate(page=page, per_page=10)

        return data

    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()            
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

   

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
