from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate
from models.package import PackageModel
from models.packageDetails import PackageDetailsModel
from shared.mailer import packageSubscribedMail

parser = reqparse.RequestParser()

parser.add_argument('package_id', required=False, help='This field cannot be left blank')
parser.add_argument('package_name', required=False, help='Must enter the package_name')
parser.add_argument('package_price', required=False, help='Must enter the package_price')
parser.add_argument('package_duration', required=False, help='Must enter the package_duration')



class Package(Resource):

    
    def get(self, id):

        item_list = PackageModel.find_by_id(id)
        item = PackageModel.to_json(item_list)

        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'package received',
                'data': item
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = PackageModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'package has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
        new_data = PackageModel.get_by_id(id)

        if new_data is None:
            new_data = PackageModel(id, data['package_id'])
        else:
            # new_data.user_id = 1
            new_data.package_name = data['package_name']
            new_data.package_price = data['package_price']
            new_data.package_duration = data['package_duration']
            

        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'package has been updated',
                'data': PackageModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the packages',
            }, 400
       

class PackageList(Resource):
    
    # @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')

        result = PackageModel.return_all(page, searchTerm)
        items = result.items
        cus_paging = sqlalchemy_paginate(result)        
        
        data = list(map(lambda x: PackageModel.to_json(x), items))


        for value in data:
   
            pack_det_list = PackageDetailsModel.get_by_id(value['package_id'])
            # pack_data = list(map(lambda x: PackageDetailsModel.to_json(x), pack_det_list))
            pack_data = PackageDetailsModel.to_json(pack_det_list)
            value['pack_details'] = pack_data
        
        return {
                'status': 200,
                'success': True,
                'message': 'packages received',
                'data':data,
                'paginate':cus_paging,
                }, 200
    
    @jwt_required

    def post(self):
       
        data = parser.parse_args()
        current_user = get_jwt_identity()

        
        new_data = PackageModel(
            package_id=data['package_id'],
            package_name=data['package_name'],
            package_price=data['package_price'],
            package_duration=data['package_duration'],
            status=1
        )

        try:
            new_data.save_to_db()
            return {
                    'status': 200,
                    'success': True,
                    'message': 'package has been created ',
                    'data': PackageModel.to_json(new_data)
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the package',
            }, 400
            

