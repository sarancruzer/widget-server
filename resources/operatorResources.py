from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from flask.templating import render_template

from flask import request
from shared.util import custom_paginate
from models.package import PackageModel
from models.user import UserModel
from models.userAssignWebsite import UserAssignWebsiteModel
from models.website import WebsiteModel
from shared.mailer import welcomeMail
from shared.token import generate_confirmation_token
import app
import uuid

parser = reqparse.RequestParser()

parser.add_argument('operator_name', help = 'This field cannot be blankk', required = True)
parser.add_argument('email', help = 'This field cannot be blank', required = False)
parser.add_argument('mobile', help = 'This field cannot be blank', required = True)
parser.add_argument('designation', help = 'This field cannot be blank', required = True)
parser.add_argument('user_type', help = 'This field cannot be blank', required = True)


class Operator(Resource):
    def get(self, id):


        item = UserModel.get_by_id(id)

        result = UserModel.to_json(item)

        if result:
            return {
                'status': 200,
                'success': True,
                'message': 'Operator received',
                'data': result
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Operator not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = PackageModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'package has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
       

        new_data = UserModel.get_by_id(id)


        if new_data is None:
            new_data = UserModel(id, id)

        else:
            
            new_data.name = data['operator_name']
            new_data.email = data['email']
            new_data.mobile = data['mobile']
            new_data.designation = data['designation']
            new_data.user_type = data['user_type']


        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'operator has been updated',
                'data': UserModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the packages',
            }, 400
       

class OperatorList(Resource):
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        website_id = request.args.get('website_id', '')

        query, params = UserAssignWebsiteModel.get_operator_by_website_id(website_id)

        per_page = 10
        result, cus_paging = custom_paginate(query, page, per_page,params)

        data = list(map(lambda x: UserModel.to_json(x), result))

        return {
                'status': 200,
                'success': True,
                'message': 'Operators received',
                'data':data
                }, 200
    
    
    @jwt_required
    def post(self):

        parser = reqparse.RequestParser()

        parser.add_argument('website_id', help = 'This field cannot be blank', required = True)
        parser.add_argument('operator_name', help = 'This field cannot be blank', required = True)
        parser.add_argument('designation', help = 'This field cannot be blank', required = True)
        parser.add_argument('mobile', help = 'This field cannot be blank', required = True)
        parser.add_argument('email', help = 'This field cannot be blank', required = True)
        parser.add_argument('user_type', help = 'This field cannot be blank', required = True)

        

        data = parser.parse_args()

        current_user = get_jwt_identity()

        website_data = WebsiteModel.get_by_id(data['website_id'])

        register_user_data = UserModel.find_by_email(data['email']) 


        if UserModel.find_by_email_website(data['email'],data['website_id']):
            return {
                'status': 409,
                'success': False,
                'message': 'This user {} already assigned this website'.format(data['operator_name']),
            },409

        if register_user_data:
            if UserAssignWebsiteModel.check_if_assigned(register_user_data.user_id,data['website_id']):
                return {
                    'status': 409,
                    'success': False,
                    'message': 'This user {} already assigned this website'.format(data['operator_name']),
                },409


        if UserModel.find_by_email_company(data['email'],current_user['company_id']):
            return {
                'status': 408,
                'success': False,
                'message': 'This user {} already registered in this company, Would you like to assign the user into this {} website'.format(data['operator_name'],website_data.website_name),
            },408
               
        
        if UserModel.find_by_email(data['email']):
            return {
                'status': 409,
                'success': False,
                'message': 'Email {} already exists'.format(data['email']),
            },409

        if UserModel.find_by_mobile(data['mobile']):
            return {
                'status': 409,
                'success': False,
                'message': 'Mobile {} already exists'.format(data['mobile']),
            },409


        try:

            token = generate_confirmation_token(data['email'])

            token = token.decode("utf-8")
            data['url'] = app.app.config['DOMAIN_URL'] + "auth/verify/email/" + str(token)

            password = str(uuid.uuid4().hex[:8])

            new_user = UserModel(
                company_id = current_user['company_id'],
                website_id = data['website_id'],
                name = data['operator_name'],
                email = data['email'],
                password = UserModel.generate_hash(password),
                mobile = data['mobile'],
                designation = data['designation'],
                website = current_user['website'],
                user_type = data['user_type'],
                email_verify_token=token,
                status=1          
            )


            new_user.save_to_db()

            uaw_data = UserAssignWebsiteModel(
                company_id = current_user['company_id'],
                website_id = data['website_id'],
                user_id = new_user.user_id
                )

            uaw_data.save_to_db()            
           
            data['password'] = password            
            welcomeMail(data['email'], data['operator_name'],"Claribot Registration", "welcome to claritaz bot!", data)

            return {
                'status': 200,
                'success': True,
                'message': 'User {} have successfully registered into our portal'.format(data['email']),
                'data': UserModel.to_json(new_user)
                }, 200

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred creating the User',
            }, 400


class OperatorAssign(Resource):
    @jwt_required
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('email', help = 'This field cannot be blank', required = True)
        parser.add_argument('website_id', help = 'This field cannot be blank', required = True)

        data = parser.parse_args()

 
        current_user = get_jwt_identity()
 
        user_data = UserModel.find_by_email(data['email'])

        try:

            uaw_data = UserAssignWebsiteModel(
                company_id = current_user['company_id'],
                website_id = data['website_id'],
                user_id = user_data.user_id
                )

            uaw_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'User {} have successfully assigned into our website'.format(data['email'])
                }, 200

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred assigning the User',
            }, 400



class chatReqMail(Resource):

    def post(self):

        parser = reqparse.RequestParser()

        parser.add_argument('name', required=True, help='This field cannot be left blank')
        parser.add_argument('email', required=True, help='This field cannot be left blank')
        parser.add_argument('message', required=True, help='This field cannot be left blank')

        data = parser.parse_args()
        print("===============================")
        print(data)
        print("===============================")
      
        chatReqMail("Chat Request Mail", render_template('chatReq.html',data = data), data)

        try:
            return {
                    'status': 200,
                    'success': True,
                    'message': 'mail sent',
                    }, 200  
                   
        
        except:
            
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred while sending mail',
            }, 400

