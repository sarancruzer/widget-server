REST-tutorial

Files for my REST API tutorials featuring a server written in Python and a web client written in Javascript. Here are the articles:

Setup
-----sudo apt install oracle-java8-installer

- Install Python 3 and git.
- Run `setup.sh` (Linux, OS X, Cygwin) or `setup.bat` (Windows)
- Run `./app.py` to start the server (on Windows use `flask\Scripts\python rest-server.py` instead)
- Alternatively, run `./app.py` to start the Flask-RESTful version of the server.
- Open `http://localhost:5000` on your web browser to run the client

# flask_restful

#mysql query
## mySql copy rows into same table with key value changed (not overwriting existing)

INSERT INTO questions (user_id,class_name,sentence,response,image_link,href_link,audio_link,video_link) SELECT 7,class_name,sentence,response,image_link,href_link,audio_link,video_link FROM questions WHERE user_id=1

3.7.3 

virtualenv --python=python3.6 botvenv

# Using Python 3 in virtualenv
- virtualenv -p python3 venv
- https://stackoverflow.com/questions/23842713/using-python-3-in-virtualenv


# To install required pip library
- pip3.6 install -r requirements.txt
- https://stackoverflow.com/questions/7225900/how-to-install-packages-using-pip-according-to-the-requirements-txt-file-from-a

# To prepare the installed library to txt file
- pip3 freeze > requirements.txt

# Getting Flask to use Python3 (Apache/mod_wsgi)
- https://stackoverflow.com/questions/30642894/getting-flask-to-use-python3-apache-mod-wsgi

# error apache2.service“ and ”journalctl -xe"
- https://stackoverflow.com/questions/32926207/error-apache2-service-and-journalctl-xe/35548631


# Command “python setup.py egg_info” failed with error code 1 in /tmp/pip-install-fs0wmmw4/mysqlclient/
- https://stackoverflow.com/questions/49862527/command-python-setup-py-egg-info-failed-with-error-code-1-in-tmp-pip-install

- sudo apt-get install libmysqlclient-dev
- sudo pip3 install mysqlclient



-----------------------------
SERVER
-------------------------------

[Unit]
Description=uWSGI instance to serve widgetproject
After=network.target

[Service]
User=ubuntu
Group=www-data
WorkingDirectory=/var/www/html/widget-server
Environment="PATH=/var/www/html/widget-server/venv/bin"
ExecStart=/var/www/html/widget-server/venv/bin/uwsgi --ini widgetproject.ini

[Install]
WantedBy=multi-user.target

-------------------------------------


sudo nano /etc/systemd/system/widgetserver.service

sudo systemctl start widgetserver

sudo systemctl stop widgetserver

sudo systemctl enable widgetserver

sudo systemctl restart widgetserver

sudo systemctl daemon-reload    

sudo nano /etc/nginx/sites-available/widgetserver

uwsgi --socket 0.0.0.0:5000 --protocol=http -w wsgi:app

sudo rm -r /etc/nginx/sites-enabled/widgetserver

sudo ln -s /etc/nginx/sites-available/widgetserver /etc/nginx/sites-enabled

sudo nginx -t

sudo systemctl restart nginx



--------------------------------
SERVER
---------------------------------

server {
    listen 5001;
    server_name 54.171.116.100;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/var/www/html/widget-server/widgetproject.sock;
    }
}





# SSL 

sudo certbot --server https://acme-v02.api.letsencrypt.org/directory -d *.claritaz.com --manual --preferred-challenges dns-01 certonly

sudo certbot --nginx -d bot.claritaz.com -d www.bot.claritaz.com


- certbot delete --cert-name domain.com-0001
- certbot delete --cert-name bot.claritaz.com

sudo openssl x509 -dates -noout -in /etc/letsencrypt/live/bot.claritaz.com/fullchain.pem
sudo openssl x509 -dates -noout -in /etc/letsencrypt/live/claritaz.com/fullchain.pem

sudo certbot certificates


# You should test your configuration at:
- https://www.ssllabs.com/ssltest/analyze.html?d=bot.claritaz.com


IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/bot.claritaz.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/bot.claritaz.com/privkey.pem
   Your cert will expire on 2019-01-28. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot again
   with the "certonly" option. To non-interactively renew *all* of
   your certificates, run "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le

# Expand the subdomain

- sudo certbot --nginx


# widget script

<!-- quadruple india -->
<script type="text/javascript" src="https://bot.claritaz.com/widget-server/widgetScriptLoader.js" apiKey="Z9OCKCFMG2H7ZF8APF2FQHMQB35012ZVYFBQOU8TFAT15AAUEM"></script>

<!-- quadruple automation -->
<script type="text/javascript" src="https://bot.claritaz.com/widget-server/widgetScriptLoader.js" apiKey="9E47NDJ0PBGZ3DU7A7UL92WJT1MEXDZ5FVZ3J8KZTVUP5PFK49"></script>

<!-- claritaz -->
<script type="text/javascript" src="https://bot.claritaz.com/widget-server/widgetScriptLoader.js" apiKey="THU0JYEQOSZA30IS3BHDOHSF718MIT9NOD04EKP3Q4SQN54K7G"></script>


# cron url :

localhost:5000/api/payment/subscription/cron


# pdf kit
- https://pypi.org/project/pdfkit/
- https://www.youtube.com/watch?v=Jg20ZFqF7BE

# Flask mail template
- https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xi-email-support


# Scrapy 

-http://doc.scrapy.org/en/latest/topics/selectors.html
-https://scrapy.org/

- pip3 install twisted
- sudo -H pip3 install twisted

- pip3 install scrapy 
- pip3 install beautifulsoup4
- pip3 install pdfkit
- pip3 install flask-socketio

# Deployment process

# gunicorn

- http://exploreflask.com/en/latest/deployment.html#the-host

- http://docs.gunicorn.org/en/stable/install.html

- https://programmer.help/blogs/5c13afa73d10a.html

- https://tutorials.technology/tutorials/71-How-to-setup-Flask-with-gunicorn-and-nginx-with-examples.html

- gunicorn app:app -p app.pid -D
 
- gunicorn app:app -p app.pid -b 0.0.0.0:5005 -D

- gunicorn -k gevent --worker-connections 10 app:app -p app.pid -b 0.0.0.0:5005 -D 



- https://medium.com/ymedialabs-innovation/deploy-flask-app-with-nginx-using-gunicorn-and-supervisor-d7a93aa07c18

- https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-16-04

- https://medium.com/ymedialabs-innovation/deploy-flask-app-with-nginx-using-gunicorn-and-supervisor-d7a93aa07c18



- pip3 install twisted
- sudo -H pip3 install twisted

- pip3 install scrapy 
- pip3 install beautifulsoup4

- pip3 install flask-socketio


sudo kill $(sudo lsof -t -i:5001)


For destroy running pid
 - kill -HUP `cat app.pid`
 - kill `cat app.pid`

# Algorithm used in chatbot
  -- Naive Bayes Algorithm

# Question Generation
  -- Question Generation via Overgenerating Transformations and Ranking

# Used in Algorithm

 - Word Tokenize
 - Stemmer(LancasterStemmer)
 - Autocorrect(Spell check)
 
# To install Autocorrect
-- pip install autocorrect


pip3 install --upgrade tensorflow
<!-- 
 API_KEY = 'trnsl.1.1.20190712T131459Z.86b28404a5652724.a8176784eb6f48ab968ccdf7c4d923d974f91dc8'
        url = 'https://translate.yandex.net/api/v1.5/tr.json/getLangs'
        params = dict(key=API_KEY,ui='en')

        res = (requests.get(url,params=params))
        print((res.text))
        return -->

        AIzaSyD41yjtbF0PZ5K_okxgEMpdcAZwB8KEZ7U
