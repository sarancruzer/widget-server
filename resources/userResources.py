from flask_restful import Resource, reqparse
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)

from models.user import RevokedTokenModel, UserModel

import random
import string
import json

from flask import request

import uuid

import werkzeug, os
from models.company import CompanyModel
from flask.wrappers import Response
from models.website import WebsiteModel
from shared.mailer import welcomeMail, emailVerifyMail,ForgetpasswordMail
from shared.token import generate_confirmation_token, confirm_token



from models.widgetLayout import WidgetLayoutModel
from models.userAssignWebsite import UserAssignWebsiteModel
from shared import db
import app
from shared.logs import configLogs
from logging.handlers import RotatingFileHandler
from models.userAddress import UserAddressModel
from models.category import CategoryModel
from models.question import QuestionModel


from flask import Flask, request, jsonify, abort
from services.crawlerSerNew import demoprocess, getAllUrlNew

import threading
from models.crawler import CrawlerModel
import re
from models.packResponseCount import PackResponseCountModel

spiders = ['dmoz']


try:
    import urlparse
except ImportError:
    import urllib.parse as urlparse


import tldextract

UPLOAD_FOLDER = 'static/img/'

default = []


class threadClass:

    def __init__(self,**kwargs):
        self.total_url = kwargs.get('total_url')
        self.crawled_url = kwargs.get('crawled_url')
        self.company_id = kwargs.get('company_id')
        self.website_id = kwargs.get('website_id')
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True                       # Daemonize thread
        thread.start()                             # Start the execution                         # Start the execution

    def run(self):
        
        
        # This might take several minutes to complete
        t_url, c_url = demoprocess(self.total_url,self.crawled_url,self.company_id,self.website_id)
        




class AppCrawler(Resource):
    
    @jwt_required
    def post(self):

        current_user = get_jwt_identity()
       
        total_url = []
        crawled_url = []        
        content = request.get_json(silent=True)

        url = content['url']
        website_id = content['website_id']      
        total_url = url  
                
        company_id = current_user['company_id']
        # website_id = current_user['website_id']
        domain = url[0]
        
        try:
            subdomain = url[1]  
        except IndexError:
            subdomain = ''
       

        begin = threadClass(total_url = total_url,crawled_url = crawled_url,company_id = company_id,website_id = website_id)
        
        # if CrawlerModel.find_by_domain(domain):
           
        #     return {
        #         'status': 404,
        #         'success': True,
        #         'message': 'Domain {} already crawled'.format(domain),
        #     },200
            
        try:

                crawler_item = CrawlerModel.get_by_website_id(website_id)
                if crawler_item:
                    crawler_item.delete_from_db()



                crawler = CrawlerModel(
                    company_id = company_id,
                    website_id = website_id,
                    domain = domain,
                    subdomain = subdomain,
                    status = 1   
                )
                crawler.save_to_db()

            
                return {
                            'status': 200,
                            'success': True,
                            'message': 'Website has been crawled successfully',
                            'result': []
                        }, 200

        
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred while crawling the website'+str(e),
            }, 400
            


class Api(Resource):
    def get(self):
        return {'message': 'Api works'}, 200

class UserRegistration(Resource):
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('company_name', help = 'This field cannot be blank', required = False)
        parser.add_argument('business_type', help = 'This field cannot be blank', required = True)
        parser.add_argument('country', help = 'This field cannot be blank', required = True)
        parser.add_argument('contact_person', help = 'This field cannot be blank', required = True)
        parser.add_argument('designation', help = 'This field cannot be blank', required = True)
        parser.add_argument('mobile', help = 'This field cannot be blank', required = True)
        parser.add_argument('email', help = 'This field cannot be blank', required = True)
        parser.add_argument('password', help = 'This field cannot be blank', required = True)
        parser.add_argument('website', help = 'This field cannot be blank', required = True)


        data = parser.parse_args()
        
        if UserModel.find_by_email(data['email']):
            return {
                'status': 409,
                'success': False,
                'message': 'Email {} already exists'.format(data['email']),
            },409

        if UserModel.find_by_mobile(data['mobile']):
            return {
                'status': 409,
                'success': False,
                'message': 'Mobile {} already exists'.format(data['mobile']),
            },409

        
        
        website_data = tldextract.extract(data['website'])
        website = website_data.domain + '.' + website_data.suffix
        apiKey = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(50)))

        scriptSourceCode = '<script type="text/javascript" src="https://bot.claritaz.com/widget-server/widgetScriptLoader.js" apiKey="' + apiKey + '"></script>'
        
        # scriptSourceCode = '<script type="text/javascript" src="' + app.app.config['DOMAIN_URL_SCRIPT'] + 'widgetScriptLoader.js" apiKey="' + apiKey + '"></script>'
        greeting = 'greetings'

        try:

            new_company = CompanyModel(  
                company_code = str(uuid.uuid4().hex),             
                company_name = data['company_name'],
                business_type = data['business_type'],
                country = data['country'],
                contact_person = data['contact_person'],
                designation = data['designation'],
                mobile = data['mobile'],
                email = data['email'],
                website = website
                # no_of_website = data['website_count']                  
            )

            new_company.save_to_db()
        
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400 
        
        try:
            new_website = WebsiteModel(               
                    website_code = str(uuid.uuid4().hex),
                    company_id = new_company.company_id,
                    website_name = website,
                    api_key = apiKey,
                    trained_json_path = "",
                    script_source_code = scriptSourceCode,
                    active_status = 1

                )

            new_website.save_to_db()
            #conf=[]
            #conf["config"].append({'faq_algorithm':'deeppavlov'})
            conf = {'config': [{'faq_algorithm': 'deeppavlov'}]}
            directory = "faq_config_json/" + new_company.company_code + "/"

            if not os.path.exists(directory):
                os.makedirs(directory)

            #conf.append({ "faq_algorithm": "deeppavlov"})  
                
            filepath = directory + new_website.website_code + '.json'           

            with open(filepath, mode='w') as feedsjson:
                json.dump(conf, feedsjson, indent=4, sort_keys=True)
                
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400 


        try:

            filepath = "customlogs/" + new_company.company_code + "/" + website

            os.makedirs(filepath , exist_ok=True)  # Python>3.2

            logHandler = RotatingFileHandler(filepath + "/" + 'info.log', maxBytes=512000, backupCount=3)
            app.app.logger.removeHandler(logHandler)
        
            

            new_widget = WidgetLayoutModel(
                company_id = new_company.company_id,
                website_id = new_website.website_id,
                logo = "uploads/logo/logo.png",  
                primary_color = "#e42038",
                user_icon_path = "uploads/icons/user/user1.png",
                bot_icon_path = "uploads/icons/bot/bot1.png",
                bot_header_background = "#e42038",
                bot_body_background = "#ddd",
                user_msg_foreground = "#000",         
                user_msg_background = "#eff5fb",
                bot_msg_foreground = "#000",         
                bot_msg_background = "#fff",         
                screen_open_default = 1,
                status = 1
            )
            
            new_widget.save_to_db()

            # res = createDefaultWidgetComponents(new_company.company_id,new_website.website_id)

        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400 

            
        
        try:

            token = generate_confirmation_token(data['email'])
            token = token.decode("utf-8")
            data['url'] = app.app.config['DOMAIN_URL'] + "auth/verify/email/" + str(token)
           
            

            new_user = UserModel(
                company_id = new_company.company_id,
                website_id = new_website.website_id,
                name = data['contact_person'],
                email = data['email'],
                password = UserModel.generate_hash(data['password']),
                mobile = data['mobile'],
                website = website,
                user_type = 2,
                designation = data['designation'],
                email_verify_token=token,
                status=1          
            )

            new_user.save_to_db()
            
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400 


        try:

            new_website.created_by = new_user.user_id
            new_website.save_to_db()


            uaw_data = UserAssignWebsiteModel(
                company_id = new_company.company_id,
                website_id = new_website.website_id,
                user_id = new_user.user_id
                )

            uaw_data.save_to_db()
            
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400 


        try:            
            
            new_user_address = UserAddressModel(
                user_id = new_user.user_id,
                name = data['contact_person'],
                mobile = data['mobile'],
                pincode = "",
                country = data['country'],
                address = "",
                city = "",
                state = "",
                landmark = "",
                mobile_alternate= "",
                status=1          
            )

            new_user_address.save_to_db()
            
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400 

        
        try:            
            
            new_category = CategoryModel(
            website_id = new_website.website_id,
            category_name=greeting,
            created_by= new_user.user_id,
            status=1
        
            )

            new_category.save_to_db()
            
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400     
        
        try:            
            
            default_qns = []
            with open('jsons/greetings.json', 'r') as f: 
                default_qns = json.load(f)
                # return default_qns

            for value in default_qns:

                rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))

                new_question = QuestionModel(
                    user_id=new_user.user_id,
                    class_name=rand,
                    website_id=new_website.website_id,
                    sentence=value['sentence'],
                    category_id=new_category.category_id,
                    response=value['responses'],
                    image_link="",
                    audio_link="",
                    video_link="",
                    href_link="",
                    playbook_id=0,
                    status=1,
                    publish_status=0
                )
                new_question.save_to_db()

            
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400     
            
        try:            
            
            new_pack_response_count = PackResponseCountModel(
            api_key = apiKey,
            response_count = 0,
            status = 1
            )
            new_pack_response_count.save_to_db()
            
        except Exception as e:
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400     
        
        try:
            welcomeMail(data['email'], data['contact_person'],"Claribot Registration", "welcome to claritaz bot!", data)
            
            result = {}
            user_data = UserModel.to_json(new_user)
            company_data = CompanyModel.to_json(new_company)
            
            result['user'] = user_data
            result['company'] = company_data


            access_token = create_access_token(identity = result['user'])
            refresh_token = create_refresh_token(identity = result['user'])
                     
            return {
                'status': 200,
                'success': True,
                'message': 'User {} has successfully registered into our portal'.format(data['email']),
                'access_token': access_token,
                'refresh_token': refresh_token,
                'data':result                           
            },200

        # except (ValueError, KeyError, TypeError) as error:
        except Exception as e:
            
            return {
                'status': 400,
                'success': False,                
                'message': str(e),
            },400         



class UserLogin(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('email', help = 'This field cannot be blank', required = False)
        parser.add_argument('password', help = 'This field cannot be blank', required = False)


        
        data = parser.parse_args()


        logg = configLogs
        # logg.logger.info("sklajdflsjaldfjsdfkl","sdasda","Asqweqwewqewqed")


        
        app.app.logger.info('%s %s %s %s %s 5xx INTERNAL SERVER ERROR\n%s',
                      request.remote_addr,
                      request.method,
                      request.scheme,
                      request.full_path,
                      200)
       
        current_user = UserModel.find_by_email(data['email'])

        if not current_user:

            app.app.logger.error('%s %s %s %s %s 5xx INTERNAL SERVER ERROR\n%s',
                      request.remote_addr,
                      request.method,
                      request.scheme,
                      request.full_path,
                      404)

            return {
                'status': 404,
                'success': False,                
                'message': 'User {} doesn\'t exist'.format(data['email']),
            },404

       
        userObj = UserModel.to_json(current_user)
        

        try:
        
            current_company = CompanyModel.find_by_company_id(current_user.company_id)
            companyObj = CompanyModel.to_json(current_company)
            userObj['company'] = companyObj
        except Exception as e:
            # db.db.session.rollback()
            print(e)

        
        if UserModel.verify_hash(data['password'], current_user.password):
            access_token = create_access_token(identity = userObj)
            refresh_token = create_refresh_token(identity = userObj)
            
            return {
                'status': 200,
                'success': True,
                'access_token': access_token,
                'refresh_token': refresh_token,
                'user': userObj,
                'message': 'Logged in as {}'.format(current_user.email),
            },200

        else:
            return {
                'status': 400,
                'success': False,                
                'message': 'Your password is wrong',
            },400






class User(Resource):

    def get(self, id):

        item = UserModel.get_by_id(id)

        userObj = UserModel.to_json(item)

        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'users received',
                'data': userObj
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'User not found',
            }, 404
    
    @jwt_required
    def delete(self, id):

        item = UserModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'user has been deleted',
            }, 200
   
    @jwt_required
    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument('name', help = 'This field cannot be blank', required = True)
        parser.add_argument('mobile', help = 'This field cannot be blank', required = True)

        # Create or Update
        data = parser.parse_args()
        new_data = UserModel.get_by_id(id)

        if new_data is None:
            new_data = UserModel(id, data['name'])
        else:
            # new_data.user_id = 
            new_data.name = data['name']
            new_data.mobile = data['mobile']            

        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'user has been updated',
                'data': UserModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the users',
            }, 400



class UserList(Resource):            
        
    # @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')
        result = UserModel.return_all(page, searchTerm)
        
        # items = result.items
        # cus_paging = custom_paginate(result)        
        
        # data = list(map(lambda x: UserModel.to_json(x), items))
        return {
                'status': 200,
                'success': True,
                'message': 'users received',
                'data':result
                }, 200

class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, 500


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}


    
    def delete(self):
        return UserModel.delete_all()

class SecretResource(Resource):
    @jwt_required
    def get(self):
        return {
            'answer': 42
        }

class UserData(Resource):
    def post(self ,id ):        
        parser = reqparse.RequestParser()      
        data = parser.parse_args()

        apikey = id
        
        websiteItem = WebsiteModel.get_by_apikey(apikey)
        
        websiteObj = WebsiteModel.to_json(websiteItem)

        result = {}

        widgetItem = WidgetLayoutModel.find_by_website_id(websiteObj['website_id'])

        widgetObj = {}
        
        categoryitem = CategoryModel.get_by_category(websiteObj['website_id'])
        print(categoryitem['category'])
        categoryitem['category'].append({'category_id':0, 'category_name':'All'})

        print("=======================================================")

        print("1111111111111111111111111111111111111111111111111111111111")

        if widgetItem:
            widgetObj = WidgetLayoutModel.to_json(widgetItem)
        
        result['category'] = categoryitem
        result['website'] = websiteObj
        result['components'] = widgetObj

        if websiteObj['active_status']:
            pass
            if result:
                return {
                    'status': 200,
                    'success': True,
                    'message': 'user details received',
                    'data': result
                }, 200

            else:
                return {
                    'status': 404,
                    'success': False,
                    'message': 'User not found',
                }, 404
        else:
            return {
                'status': 404,
                'success': False,
                'message': 'User not found',
            }, 404


class VerifyEmail(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('email', help = 'This field cannot be blank', required = False)
        parser.add_argument('token', help = 'This field cannot be blank', required = True)
       
        params = parser.parse_args()

        user = UserModel.find_by_token(params['token'])

        data = UserModel.to_json(user)
        if user.email_verify_status:
            return {
                'status': 200,
                'success': True,
                'message': 'Your email address already verified!',
                'data':data

            },200
        else:

            email = confirm_token(params['token'])            
            if email == False:
                return {
                'status': 400,
                'success': False,
                'message': 'The Email confirmation link is invalid or has expired.'
            },400

            #update code  
            new_data = UserModel.get_by_id(user.user_id)
            new_data.email_verify_status = 1
            new_data.save_to_db()
            
            return {
                'status': 200,
                'success': True,
                'message': 'The following email address has been successfully verified',
                'data': data

            },200

class ResendVerifyEmail(Resource):
    def post(self):

            parser = reqparse.RequestParser()
            parser.add_argument('token', help = 'This field cannot be blank', required = True)
       
            data = parser.parse_args()
            user_data = UserModel.find_by_token(data['token'])
            if user_data is None:
                return {
                    'status': 400,
                    'success': False,
                    'message': 'Your token is not in our database.'
                },400


            token = ""
            if user_data:
                user = UserModel.to_json(user_data)
                token = generate_confirmation_token(user['email'])
                user_data.email_verify_token = token
                user_data.save_to_db()
            
            token = token.decode("utf-8") 
            
            user['url'] = app.app.config['DOMAIN_URL'] + "auth/verify/email/" + str(token)
            
            welcomeMail(user['email'], user['name'],"Claribot Registration", "welcome to claritaz bot!", user)


            try:
                    return {
                    'status': 200,
                    'success': True,
                    'message': 'The Resend Email has been successfully sent.'

                },200

            except Exception as e:
                return {
                    'status': 400,
                    'success': False,
                    'message': 'An error occurred Resending the email.'

                },400

    
class currentUser(Resource):
    @jwt_required
    def post(self):
        parser = reqparse.RequestParser()

        current_user = get_jwt_identity()
       

        userList = UserModel.get_by_id(current_user['user_id'])
        user_data = UserModel.to_json(userList)
                
        return {
            'status': 200,
            'success': True,
            'data': user_data,
            'message': 'current user details',
            },200



class SendForgotPasswordEmail(Resource):
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('email', help = 'This field cannot be blank', required = True)
    
        data = parser.parse_args()
        user_data = UserModel.find_by_email(data['email'])

        if user_data is None:
            return {
                'status': 400,
                'success': False,
                'message': 'Invalid email address.'
            },400

        token = ""

        if user_data:
            user = UserModel.to_json(user_data)
            token = generate_confirmation_token(user['email'])
       
        token = token.decode("utf-8") 
        
        user['url'] = app.app.config['DOMAIN_URL'] + "auth/password/forgot/verify/email/" + str(token)
        
        ForgetpasswordMail(user['email'], user['name'],"Claribot Forgot password!", "Last your password? Please click and enter your password.", user)

        try:

            user_data.reset_paswd_token = token
            user_data.save_to_db()

            return {
                'status': 200,
                'success': True,
                'message': 'The Forgot password email has been successfully sent.'

            },200

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred sending forgot password email.'

            },400


class VerifyForgotPasswordEmail(Resource):
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('email', help = 'This field cannot be blank', required = False)
        parser.add_argument('token', help = 'This field cannot be blank', required = True)
       
        params = parser.parse_args()
       
        user = UserModel.find_by_password_token(params['token'])
        
        data = UserModel.to_json(user)

        if confirm_token(params['token']):
            return {
                'status': 200,
                'success': True,
                'message': 'Forgot password has been successfully verified',
                'data': data

            },200
        else:
            return {
                'status': 400,
                'success': False,
                'message': 'Your token invalid or has expired.'

            },400


class UpdateForgotPasswordEmail(Resource):
    def post(self):

        parser = reqparse.RequestParser()
        parser.add_argument('token', help = 'This field cannot be blank', required = False)
        parser.add_argument('password', help = 'This field cannot be blank', required = False)
        parser.add_argument('confirm_password', help = 'This field cannot be blank', required = True)
       
        params = parser.parse_args()
        
        user = UserModel.find_by_password_token(params['token'])

        if params['password'] != params['confirm_password']:
            return {
                'status': 400,
                'success': True,
                'message': 'The password and confim password should same!',
                'data': False

            },400


        data = UserModel.to_json(user)

        try:
            user.password = UserModel.generate_hash(params['password'])
            user.save_to_db()

            return {
                'status': 200,
                'success': True,
                'message': 'Password updated successfully!',
                'data': data
            },200

        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'Password update failed!.'

            },400
            

class UserAddress(Resource):

    @jwt_required
    def get(self):

        current_user = get_jwt_identity()
        try:
            item = UserAddressModel.get_by_userid(current_user['user_id'])
            userAddressObj = []
            if item:
                userAddressObj = UserAddressModel.to_json(item)
            
            return {
                'status': 200,
                'success': True,
                'message': 'users address received',
                'data': userAddressObj
            }, 200

        except Exception as e:
            return {
                'status': 404,
                'success': False,
                'message': 'User address not found',
            }, 404



    @jwt_required
    def post(self):

        current_user = get_jwt_identity()

        parser = reqparse.RequestParser()
        parser.add_argument('name', help = 'This field cannot be blank', required = False)
        parser.add_argument('mobile', help = 'This field cannot be blank', required = False)
        parser.add_argument('pincode', help = 'This field cannot be blank', required = True)
        parser.add_argument('country', help = 'This field cannot be blank', required = False)
        parser.add_argument('address', help = 'This field cannot be blank', required = True)
        parser.add_argument('city', help = 'This field cannot be blank', required = True)
        parser.add_argument('state', help = 'This field cannot be blank', required = True)
        parser.add_argument('landmark', help = 'This field cannot be blank', required = False)
        parser.add_argument('mobile_alternate', help = 'This field cannot be blank', required = False)

       
        params = parser.parse_args()

        item = UserAddressModel.get_by_userid(current_user['user_id'])    


        if item is None:
            item = UserAddressModel(
                    user_id = current_user['user_id'],
                    name = params['name'],
                    mobile = params['mobile'],
                    pincode = params['pincode'],
                    country = params['country'],
                    address = params['address'],
                    city = params['city'],
                    state = params['state'],
                    landmark = params['landmark'],
                    mobile_alternate = params['mobile_alternate']    
                )

        else:
            item.name = params['name']
            item.mobile = params['mobile']
            item.pincode = params['pincode']
            item.country = params['country']
            item.address = params['address']
            item.city = params['city']
            item.state = params['state']
            item.landmark = params['landmark']
            item.mobile_alternate = params['mobile_alternate']    
        try:
            item.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'User address updated successfully!',
            },200

        except Exception as e:
            
            db.db.session.rollback()

            return {
                'status': 400,
                'success': False,
                'message': 'User address update failed!.'

            },400


class PasswordChange(Resource):
    
    @jwt_required
    def post(self):

        current_user = get_jwt_identity()
        parser = reqparse.RequestParser()
        parser.add_argument('current_password',help = "This field cannot be blank", required = True)
        parser.add_argument('new_password',help = "This field cannot be blank", required = True)
        parser.add_argument('confirm_password',help = "This field cannot be blank", required = True)

        params = parser.parse_args()

        if UserModel.verify_hash(params['current_password'], current_user['password']) == False:
            return {
                'status': 400,
                'success': False,                
                'message': 'Your Current password is wrong',
            },400

        

        if params['new_password'] != params['confirm_password']:
            return {
                'status': 400,
                'success': False,
                'message': 'The password and confirm password are not same!'
            },400

        user_list = UserModel.get_by_id(current_user['user_id'])
        
        user_list.password = UserModel.generate_hash(params['new_password'])
        
        try:
            user_list.save_to_db()

            return {
                'status': 200,
                'success': True,
                'message': 'Password updated successfully!',
            },200

        except Exception as e:
            
            db.db.session.rollback()

            return {
                'status': 400,
                'success': False,
                'message': 'Password updated failed!.'

            },400


        
class SendVerifyEmail(Resource):
    
    @jwt_required
    def post(self):
        
        current_user = get_jwt_identity()
      
        token = generate_confirmation_token(current_user['email'])
       
        token = token.decode("utf-8")

        data = {'url':''}
              
        data['url'] = app.app.config['DOMAIN_URL'] + "auth/verify/email/" + str(token)

        try:
            
            user_list = UserModel.get_by_id(current_user['user_id'])
            user_list.email_verify_token = token
            user_list.save_to_db()
            
            emailVerifyMail(current_user['email'], current_user['name'],"Claribot Registration", "welcome to claritaz bot!",data)

            if emailVerifyMail:
                return {
                    'status': 200,
                    'success': True,
                    'message': 'Verify mail has been sent successfully',
                }, 200
                
            else:
                return {
                'status': 400,
                'success': False,
                'message': 'Verify email not send',
            }, 400 

        except Exception as e:
            db.db.session.rollback()
            return {
                'status': 400,
                'success': False,
                'message': 'Verify email not send',
            }, 400                  

