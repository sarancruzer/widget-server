from bs4 import BeautifulSoup
import urllib.request
import tldextract
from shared import db

from services.question_generator import QuestionGenerator
import random
import string
from models.question import QuestionModel
from models.crawlerlList import CrawlerListModel
import re

kk = 0


def demoprocess(total_url, crawled_url, company_id, website_id):
    
    global kk
    
    
    for value in total_url:

            if value not in crawled_url:

                temp_url, weburl = getAllUrlNew(value, company_id, website_id)

                total_url.remove(value)
                crawled_url.append(weburl)

                in_first = set(total_url)
                in_second = set(temp_url)
                in_second_but_not_in_first = in_second - in_first
                total_url = total_url + list(in_second_but_not_in_first)

                if total_url:
                    demoprocess(total_url, crawled_url, company_id, website_id)
    
    kk = kk + 1

    return total_url, crawled_url


def getAllUrlNew(weburl, company_id, website_id):

    weburl = weburl.replace(' ', '')

    temp_url = []
    ext = tldextract.extract(weburl)

    weburlArr = weburl.split("://")

    print(ext)
    print(weburlArr)

    domain = ext.domain
    subdomain = ext.subdomain
    suffix = ext.suffix

    if not subdomain:
        subdomain = "www"

    
    host = weburlArr[0]+ '://' + subdomain + '.' + domain + '.' + suffix + '/'
    print(host)
  
    
    url = weburl
    
    try:
        s = urllib.request.urlopen(url)
        print(weburl)
        soup = BeautifulSoup(s)

        # for link in soup.find_all('a', href=lambda href: href and not href.startswith('mailto:')and not href.startswith('_/assets')and not href.startswith('javascript')and not href.endswith('png')):
        for link in soup.find_all('a', href=lambda href: href and not href.startswith('#')and not href.startswith('http')and not href.startswith('mailto:')and not href.startswith('javascript')and not href.endswith('jpg')and not href.endswith('png')and not href.endswith('jpeg')and not href.startswith('tel:')):

            if link:
                href = host + link['href']
                href = href.replace(' ', '%20')
                temp_url.append(href)

        # kill all script and style elements
        for script in soup(["script", "style"]):
            script.decompose()  # rip it out

        # get text
        text = soup.get_text()

        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = '\n'.join(chunk for chunk in chunks if chunk)

        crawler_data = CrawlerListModel(
            company_id=company_id,
            website_id=website_id,
            url=url,
            content=str(text),
            status=1
        )
    
        crawler_data.save_to_db()



        # delete_crawl = "DELETE FROM `question` WHERE `website_id` = "+ str(website_id) +" AND (`status` = 3 OR `status` = 2)"

        # delete = db.db.engine.execute(delete_crawl)




        q  = QuestionGenerator()
        question_type = ['Wh', 'Are', 'Who', 'Do']
        question_list = q.generate_question(text, question_type)
        print(question_list)

        for value in question_list:
            print(value)
            print(value['Q'])
            print(value['A'])
 
            rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))

            new_data = QuestionModel(
                user_id=1,
                class_name=rand,
                website_id=website_id,
                sentence=value['Q'],
                category_id = 1,
                response=value['A'],
                image_link="",
                audio_link="",
                video_link="",
                href_link="",
                status=2,
                publish_status=0
            )
            new_data.save_to_db()
            
    except urllib.error.HTTPError as e:
        # Return code error (e.g. 404, 501, ...)
        # ...
        print('HTTPError: {}'.format(e.code))
        print(weburl)

    except urllib.error.URLError as e:
        # Not an HTTP-specific error (e.g. connection refused)
        # ...
        print('URLError: {}'.format(e.reason))
        print(weburl)

        

    return temp_url, weburl