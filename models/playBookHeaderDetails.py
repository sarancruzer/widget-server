from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date
from flask import jsonify, abort, session
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.sqltypes import Text, DATETIME, TIMESTAMP
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import relationship, backref



class PlayBookHeaderDetailsModel(db.Model):
    __tablename__ = 'api_header_detail'

    api_header_detail_id = Column('api_header_detail_id',Integer, primary_key=True,autoincrement=True)
    playbook_id = Column('playbook_id',Integer, nullable = False)
    node_id = Column('node_id', Integer, nullable = False) 
    header_key = Column('header_key', Integer, nullable = False) 
    header_value = Column('header_value',String(255), nullable = False)
    api_detail_id = Column('api_detail_id', Integer, nullable = False) 
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)
    

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return

    @classmethod
    def find_last_added_record(cls, playBook_id,response,node_id):
        return cls.query.filter_by(playbook_id = playBook_id,node_id=node_id,option_value=response).first()

    @classmethod
    def find_by_playBook_id(cls, playBook_id):
        return cls.query.filter_by(playbook_id = playBook_id).all()

    @classmethod
    def find_by_api_id(cls, api_id):
        return cls.query.filter_by(api_detail_id = api_id).all()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def to_json(cls, x):
            return {
                'api_header_detail_id':x.api_header_detail_id,
                'playbook_id':x.playbook_id,
                'node_id': x.node_id,
                'header_key':x.header_key,
                "header_value":x.header_value,
                "api_detail_id":x.api_detail_id,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
   

# db.create_all(bind=['MYSQL'])
# db.create_all(bind=['MONGO'])
# db.session.commit()
