from shared.db import db
from sqlalchemy import Column, Integer

from sqlalchemy.sql.sqltypes import Text, DATETIME
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func

class PackChatbotCountModel(db.Model):
    __tablename__ = 'pack_chatbot_count'

    pack_chatbot_count_id = Column('pack_chatbot_count_id',Integer, primary_key=True)
    api_key = Column('api_key', Text, nullable = False)
    user_token = Column('user_token', Text, nullable = False)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True,server_default= '1')
    
        
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        db.session.rollback()
        return 

   
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
  

    @classmethod
    def to_json(cls, x):
            return {
                'pack_chatbot_count_id':x.pack_chatbot_count_id,
                'api_key': x.api_key,
                'user_token': x.user_token,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def return_all(cls, api_key):        
        
        db.session.commit()

        data = db.session.query(PackChatbotCountModel).filter_by(api_key=api_key).all()

        return data

    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}
