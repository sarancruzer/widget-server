from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)
from models.crawler import CrawlerModel
from models.website import WebsiteModel
from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate



parser = reqparse.RequestParser()
parser.add_argument('crawler_id', required=False, help='This field cannot be left blank')
parser.add_argument('company_id', required=False, help='Must enter the company_id')
parser.add_argument('website_id', required=False, help='Must enter the website_id')
parser.add_argument('domain', required=False, help='Must enter the domain')
parser.add_argument('subdomain', required=False, help='Must enter the subdomain')
parser.add_argument('created_at', required=False, help='Must enter the created_at')
parser.add_argument('updated_at', required=False, help='Must enter the updated_at')




class WebsiteCrawler(Resource):

    
    def get(self, id):

        item = WebsiteModel.get_by_id(id)

        result = WebsiteModel.to_json(item)

        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'crawler received',
                'data': result
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
            
    @jwt_required
    def delete(self, id):

        item = CrawlerModel.get_by_id(id)
        if item:
            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'Crawler has been deleted',
            }, 200

 
    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
        new_data = CrawlerModel.get_by_id(id)

        if new_data is None:
            new_data = CrawlerModel(id, data['crawler_id'])
        else:
            
                new_data.company_id=data['company_id']
                new_data.website_id=data['website_id']
                new_data.domain=data['domain']
                new_data.subdomain=data['subdomain']
                
            

        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'crawler has been updated',
                'data': CrawlerModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the crawler',
            }, 400

           
class GetCrawlWebsite(Resource):
    
    @jwt_required
    def get(self,id):
        website_id = id

        crawler_list = CrawlerModel.get_by_website_id(website_id)

        crawler_data = []
        if crawler_list:
            crawler_data = CrawlerModel.to_json(crawler_list)
        

        return {
                'status': 200,
                'success': True,
                'message': 'crawl website received',
                'data': crawler_data
                }, 200

