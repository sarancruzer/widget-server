from shared.db import db
from sqlalchemy import Column, Integer
from sqlalchemy.sql.sqltypes import  DATETIME
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func
from flask.json import jsonify
from flask import json



class UserAssignWebsiteModel(db.Model):
    
    __tablename__ = 'user_assign_website'

    uaw_id = Column('uaw_id',Integer, primary_key=True)
    company_id = Column('company_id', Integer, nullable = False) 
    website_id = Column('website_id', Integer, nullable=False)
    user_id = Column('user_id', Integer, nullable=False)    
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True)



    @classmethod
    def get_by_website_id(cls, website_id):        
    
        return cls.query.filter_by(website_id=website_id).all()

    @classmethod
    def get_operator_by_website_id(cls,website_id):

        query = "SELECT u.* FROM user_assign_website as uaw LEFT JOIN user as u ON u.user_id = uaw.user_id WHERE uaw.website_id = " + str(website_id)

        query = query + " order by uaw.created_at DESC"

        params = {}

        return query, params


    @classmethod
    def get_by_operator_count(cls, website_id):
        return cls.query.filter_by(website_id = website_id).count()

    @classmethod
    def check_if_assigned(cls, user_id, website_id):

        return cls.query.filter_by(user_id = user_id,website_id = website_id).first()

          
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush()
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    

    @classmethod
    def to_json(cls, x):
            return {
                'uaw_id':x.uaw_id,
                'company_id': x.company_id,
                'website_id':x.website_id,
                'user_id':x.user_id,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

