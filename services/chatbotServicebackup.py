import nltk
import json
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
from models.question import QuestionModel
from flask_jwt_extended.utils import get_jwt_identity, create_access_token
from models.user import UserModel
from models.contextGroup import ContextGroupModel
from models.contextGroupDetails import ContextGroupDetailsModel
from models.contextResponse import ContextResponseModel
from models.website import WebsiteModel
from models.chatbotResponse import ChatbotResponseModel
from shared import db
import app
from logging.handlers import RotatingFileHandler
import logging
import os
from sqlalchemy.sql.functions import now
from models.company import CompanyModel
from models.packResponseCount import PackResponseCountModel

from datetime import datetime
from dateutil.relativedelta import relativedelta
from models.chatHistoryDetails import ChatHistoryDetailsModel
from resources.restrictionResources import ResponseCount
from autocorrect import spell# word stemmer
stemmer = LancasterStemmer()

sample = []
training_data = []
corpus_words = {}
class_words = {}

def out_fun(filepath):
    with open(filepath, 'r') as f:
        training_data = json.load(f)
        #print ("%s sentences in training data" % len(training_data))
        return training_data

# y = out_fun()
# print(y)


# # capture unique stemmed words in the training corpus
# corpus_words = {}
# class_words = {}
# # turn a list into a set (of unique items) and then a list again (this removes duplicates)
# classes = list(set([a['class'] for a in y]))
# for c in classes:
#     # prepare a list of words within each class
#     class_words[c] = []

# # loop through each sentence in our training data
# for data in y:
#     # tokenize each sentence into words
#     for word in nltk.word_tokenize(data['sentence']):
#         # ignore a some things
#         if word not in ["?", "'s"]:
#             # stem and lowercase each word
#             stemmed_word = stemmer.stem(word.lower())
#             # have we not seen this word already?
#             if stemmed_word not in corpus_words:
#                 corpus_words[stemmed_word] = 1
#             else:
#                 corpus_words[stemmed_word] += 1

#             # add the word to our words in class list
#             class_words[data['class']].extend([stemmed_word])



def calculate_class_score_commonality(sentence, class_name, show_details=True):
    score = 0
    # tokenize each word in our new sentence
    for word in nltk.word_tokenize(sentence):
        # check to see if the stem of the word is in any of our classes
        if stemmer.stem(word.lower()) in class_words[class_name]:
            # treat each word with relative weight
            score += (1 / corpus_words[stemmer.stem(word.lower())])

            if show_details:
                print("?????????????????????????????")
                print ("   match: %s (%s)" % (stemmer.stem(word.lower()), 1 / corpus_words[stemmer.stem(word.lower())]))
    print("score score score score")
    print(score)
    return score

   

# def classify(sentence):
#     high_class = None
#     high_score = 0
#     for c in class_words.keys():
#         score = calculate_class_score_commonality(sentence, c, show_details=False)
#         if score > high_score and score > 0.1:
#             high_class = c
#             high_score = score


#     resList = responses(high_class,y)
#     return resList

def classify(sentence,yy):


    # capture unique stemmed words in the training corpus

    # turn a list into a set (of unique items) and then a list again (this removes duplicates)
    classes = list(set([a['class'] for a in yy]))
    for c in classes:
        # prepare a list of words within each class
        class_words[c] = []
    
    # loop through each sentence in our training data
    for data in yy:
        # tokenize each sentence into words
        for word in nltk.word_tokenize(data['sentence']):
            # ignore a some things
            if word not in ["?", "'s"]:
                # stem and lowercase each word
                stemmed_word = stemmer.stem(word.lower())
                # have we not seen this word already?
                if stemmed_word not in corpus_words:
                    corpus_words[stemmed_word] = 1
                else:
                    corpus_words[stemmed_word] += 1

                # add the word to our words in class list
                class_words[data['class']].extend([stemmed_word])



    
    high_class = None
    high_score = 0
    
    class_data = []
    for c in class_words.keys():

        score = calculate_class_score_commonality(sentence, c, show_details=False)
        print ("Class: %s  Score: %s \n" % (c, (score, c)))

        # score = 0
        # # tokenize each word in our new sentence
        # for word in nltk.word_tokenize(sentence):
        #     # check to see if the stem of the word is in any of our classes
        #     if stemmer.stem(word.lower()) in class_words[c]:
        #         # treat each word with relative weight
        #         score += (1 / corpus_words[stemmer.stem(word.lower())])

     
        if  score > 1:
            print("score score================= score score")
            print(score,c)
            class_data.append(c)
            high_score = score

    print("class_data class_data class_data")
    print(class_data)
    high_class = class_data

    print("high_class high_class high_class")
    print(high_class)

    # resList = responses(high_class,yy)
    return high_class,yy 


from datetime import datetime


def askqFunc(data):

    sentence = data['sentence']
    apikey = data['api_key']
    context = data['context']
    context_token = data['context_token']
    response = data['response']
    context_res_id = data['context_res_id']
   
    user_token = data['user_token']
    
    userModelRes = UserModel.get_by_api_key(apikey)    
    website_data = WebsiteModel.get_by_api_key(apikey)
    
    # website_data = WebsiteModel.to_json(website_list)

    # company_list = CompanyModel.get_by_apikey(apikey)
    # company_data = CompanyModel.to_json(website_list)

    api_key = data['api_key']
        
    query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"'"

    package_details = db.db.engine.execute(query)
    pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]
    RESPONSE_PERMIT_COUNT = pack_result['response_count']
    RESPONSE_CURRENT_COUNT = 0

    resultList = PackResponseCountModel.get_by_api_key(api_key)
        
    result = []
    if resultList:
        result = PackResponseCountModel.to_json(resultList)
        RESPONSE_CURRENT_COUNT = result['response_count']

    if RESPONSE_CURRENT_COUNT > RESPONSE_PERMIT_COUNT:
        
        return {
                'status': 400,
                'success': False,
                'message': 'An error occurred getting the response',
            }, 400

    rList = {'CLASS':'','QUESTION':'','HREF_LINK':'', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': '', 'RAW_TEXT': '', 'SUGGESTIONS_TEXT': []}

    if context and context_token:
        # updateContextualProcess(context,context_token,response,context_res_id)
        contextRes, contextGp, contextGpDetails,context_token,conv_status = updateContextualProcess(context,context_token,sentence,context_res_id)
        if conv_status == True:

            contextSuccessMsg = getContextualSuccessMsg(context,website_data['website_id'])

            rList['RAW_TEXT'] = contextSuccessMsg
            rList['CONTEXT_RES_ID'] = ""
            rList['SUGGESTIONS_TEXT'] = ""
            rList['RESPONSE_TYPE'] = ""
            rList['CONTEXT'] = ""
        else:
            rList['RAW_TEXT'] = contextRes[0]['sentence']
            rList['CONTEXT_RES_ID'] = contextRes[0]['context_resp_id']
            rList['SUGGESTIONS_TEXT'] = contextGpDetails['context_group_details']
            rList['RESPONSE_TYPE'] = contextGp['response_type']
            rList['CONTEXT'] = context
            

        rList['QUESTION'] = ""
        rList['HREF_LINK'] = ""
        rList['AUDIO_LINK'] = ""
        rList['VIDEO_LINK'] = ""
        rList['IMAGE_LINK'] = ""
        rList['USER_TOKEN'] = context_token

    else:
        
        directory_path = website_data['trained_json_path']
        print("====================================")
        print(website_data['trained_json_path'])
        # directory_path = "jsons/claritaz.json"
        yy = out_fun(directory_path)

        high_class, yy = classify(sentence,yy)
        if len(high_class) ==1:
            print("==============length equal==========")
            print("length is equal to one")
      
            rList = responses(sentence,high_class,apikey,context_token,data['interest_category'])

        else:
            print("==============length Not equal==========")
            print("length is Notequal to one")    

        # save every response into chatbot_response table
        # new_data = ChatbotResponseModel(
        #     company_id = website_data['company_id'],
        #     website_id =  website_data['website_id'],
        #     user_token =  user_token,
        #     context_name =  rList['CONTEXT'],
        #     sentence =  rList['QUESTION'],
        #     response =  rList['RAW_TEXT']
        #     )
        # new_data.save_to_db()

        chat_res_data = ChatHistoryDetailsModel(
            context_name =  rList['CONTEXT'],
            sentence =  sentence,
            response =  rList['RAW_TEXT'],
            chat_res_id = data['chat_history_id']
            )
        chat_res_data.save_to_db()





        query = "UPDATE pack_response_count SET response_count=response_count+1 WHERE api_key='"+ apikey +"'"
        result = db.db.engine.execute(query)

        current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        date_query = "UPDATE chat_history SET updated_at='"+current_date+"' WHERE chat_resp_id="+ data['chat_history_id']
        result = db.db.engine.execute(date_query)

  
    
    return rList





def responses(sentence,class_val,apikey,context_token,interest_category):

    rList = {'CLASS':'','QUESTION':'','HREF_LINK':'', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': '', 'RAW_TEXT': '', 'SUGGESTIONS_TEXT': []}
    print("class_val================================")
    print(class_val)
    website_list = WebsiteModel.get_by_apikey(apikey)
    website_data = WebsiteModel.to_json(website_list)
    
    if(class_val != None):

        check_classname = QuestionModel.find_by_class_val(class_val)
        if check_classname == 0:
            class_val = None

    if(class_val != None):

        for value in class_val:
            print("888888888888888888888==========")
            print(value)

            rList['CLASS'] = class_val

            qresult = QuestionModel.get_by_classname(class_val,website_data['website_id'])

        # qq = qRes['questions'][0]
        qq = qresult

        print(qq)
        print("CCCCCCCCCCCOOOOOOOOOOOOOOOOOOO")       
        if qq.context:
            contextRes, contextGp, contextGpDetails,context_token = newContextualProcess(qq.context,website_data)

            rList['QUESTION'] = ""
            rList['RAW_TEXT'] = contextRes[0]['sentence']
            rList['HREF_LINK'] = ""
            rList['AUDIO_LINK'] = ""
            rList['VIDEO_LINK'] = ""
            rList['IMAGE_LINK'] = ""
            rList['CATEGORY'] = ""
            rList['SUGGESTIONS_TEXT'] = contextGpDetails
            rList['RESPONSE_TYPE'] = contextGp['response_type']
            rList['CONTEXT'] = qq.context
            rList['USER_TOKEN'] = context_token
            rList['CONTEXT_RES_ID'] = contextRes[0]['context_resp_id']
        
        else:
            # print(qq)
            rList['QUESTION'] = qq.sentence
            rList['RAW_TEXT'] = qq.response
            rList['HREF_LINK'] = qq.href_link
            rList['AUDIO_LINK'] = qq.audio_link
            rList['VIDEO_LINK'] = qq.video_link
            rList['IMAGE_LINK'] = qq.image_link
            rList['CATEGORY'] = qq.category_id
            rList['SUGGESTIONS_TEXT'] = []
            rList['RESPONSE_TYPE'] = ""
            rList['CONTEXT'] = ""
            rList['USER_TOKEN'] = ""
            rList['CONTEXT_RES_ID'] = ""

        return rList


   
    else:
        suggests=['About your company','Mission of your company?','What is iPITCH?','Do you have Business Incubation service?','Talk to our Agent']

        rList['QUESTION'] = sentence
        rList['RAW_TEXT'] = "I am unsure what you have asked for let me give you some suggestions please select one to continue"
        rList['HREF_LINK'] = ""
        rList['AUDIO_LINK'] = ""
        rList['VIDEO_LINK'] = ""
        rList['IMAGE_LINK'] = ""
        rList['CATEGORY'] = ""
        rList['SUGGESTIONS_TEXT'] = suggests
        rList['RESPONSE_TYPE'] = "BUTTON"
        rList['CONTEXT'] = ""
        rList['USER_TOKEN'] = ""
        rList['CONTEXT_RES_ID'] = ""

    return rList



def newContextualProcess(context_name,website_data):

    context_token = create_access_token(identity = website_data)

    context_gp_list = ContextGroupModel.get_by_contextname(context_name,website_data['website_id'])


    # context_gp = ContextGroupModel.to_json(context_gp_list)
    context_gp = list(map(lambda x: ContextGroupModel.to_json(x), context_gp_list))

    for value in context_gp:
        
        new_data = ContextResponseModel(
                website_id=website_data['website_id'],
                context_token=context_token,
                context_gp_id=value['context_gp_id'],
                context_name=value['context_name'],
                sentence=value['sentence'],
                status=0
            )
        new_data.save_to_db()

    
    getContextResData = getContextualProcess(context_name,context_token)
 

    
    context_gp_details = ContextGroupDetailsModel.get_by_context_gp_id_custom(getContextResData[0]['context_gp_id'])

    context_gp_data = ContextGroupModel.get_by_context_gp_id(getContextResData[0]['context_gp_id'])

    context_gp = ContextGroupModel.to_json(context_gp_data)
    
    return getContextResData,context_gp,context_gp_details['context_group_details'],context_token

def getContextualProcess(context_name,context_token):

    
    context_resp_list = ContextResponseModel.get_by_contextname(context_name,context_token)

    context_resp = list(map(lambda x: ContextResponseModel.to_json(x), context_resp_list))

    return context_resp    

def updateContextualProcess(context_name,context_token,response,context_res_id):



    new_data = ContextResponseModel.get_by_id(context_res_id)

    new_data.response = response
    new_data.status = 1

    new_data.save_to_db()

    getContextResData = getContextualProcess(context_name,context_token)
    conv_status = False

    if getContextResData:
        # context_data = ContextResponseModel.get_by_id(context_res_id)
        # context_data = ContextResponseModel.to_json(context_data)

        context_data = ContextResponseModel.get_by_contextname(context_name,context_token)
        context_data = list(map(lambda x: ContextResponseModel.to_json(x), context_data))

        context_gp_details = ContextGroupDetailsModel.get_by_context_gp_id_custom(context_data[0]['context_gp_id'])
        context_gp_data = ContextGroupModel.get_by_context_gp_id(context_data[0]['context_gp_id'])
        context_gp = ContextGroupModel.to_json(context_gp_data)

    else:
        conv_status = True
        context_gp_details = []
        context_gp = []
        # context_gp_details['context_group_details'] = []
        # context_gp['context_group'] = []


    return getContextResData,context_gp,context_gp_details,context_token,conv_status
def getContextualSuccessMsg(context,website_id):
    context_msg = ContextGroupModel.get_success_by_contextname(context,website_id)

    return context_msg.success_msg


def readLogs(api_key):

    website_data = WebsiteModel.get_by_api_key(api_key)

    query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE api_key='"+ api_key +"'"

    package_details = db.db.engine.execute(query)

    pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]


    logs_count = pack_result['logs_count']


    # current_date = datetime.datetime.strptime("2011-01-01","%Y-%m-%d")
    date_before_week = datetime.today() - relativedelta(weeks=logs_count)

    filepath = "customlogs/" + str(website_data['company_code']) + "/" + str(website_data['website_name']) + "/" + "info.log"
    # filepath = "logs/" + company_code + "/" + website_name + "/" + "info.log"

    data = []
    with open(filepath, 'r') as f_in:
       
        lines = (line.rstrip() for line in f_in) 
        lines = list(line for line in lines if line) # Non-blank lines in a list

        for key,value in enumerate(lines):
            infoList = value.split('INFO')

            logged_date = infoList[0].split(',')[0]
            infoqaList = infoList[1].split('###')
            question = infoqaList[0]
            answer = infoqaList[1]

            logged_date = datetime.strptime(logged_date, '%Y-%m-%d %H:%M:%S')

            if date_before_week <= logged_date:
                data.append({'question':question, 'answer':answer,'logged_date': str(logged_date)})
        

    return data