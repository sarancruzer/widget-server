from shared.db import db
from sqlalchemy import Column, Integer
from sqlalchemy.sql.sqltypes import Text, DATETIME
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func

class PackResponseCountModel(db.Model):
    __tablename__ = 'pack_response_count'

    pack_resp_count_id = Column('pack_resp_count_id',Integer, primary_key=True)
    api_key = Column('api_key', Text, nullable = False)
    response_count = Column('response_count', Integer, nullable = False)
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True,server_default= '1')
    
        
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        db.session.rollback()
        return 

   
    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
  

    @classmethod
    def to_json(cls, x):
            return {
                'pack_resp_count_id':x.pack_resp_count_id,
                'api_key': x.api_key,
                'response_count': x.response_count,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    
    @classmethod
    def to_json_custom(cls,x):
        return {
            'response_count':x.response_count,
            'chatbot_count':x.chatbot_count,
            'logs_count': x.logs_count,
            'bot_theme': x.bot_theme,
            'website_count': x.website_count,
            'company_name': x.company_name,
            'business_type': x.business_type,
            'country':x.country,
            'contact_person': x.contact_person,
            'designation': x.designation,
            'mobile':x.mobile,
            'email':x.email,
            'website':x.website,
            'pack_end_at':x.pack_end_at

        }
    
    @classmethod
    def get_by_api_key(cls, api_key):                
        db.session.commit()
        data = db.session.query(PackResponseCountModel).filter_by(api_key=api_key).order_by(text("pack_resp_count_id desc")).first()
        return data
 
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

    
    @classmethod
    def get_count_response(cls, api_key):
        return cls.query.filter_by(api_key = api_key).first()
   
