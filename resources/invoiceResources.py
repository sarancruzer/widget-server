from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate
from models.invoice import InvoiceModel
import io
from flask.helpers import send_file, make_response
import base64
from flask.templating import render_template
import pdfkit
from models.company import CompanyModel
from models.package import PackageModel
import app

import os
APP_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEMPLATE_PATH = os.path.join(APP_PATH, 'templates/')

parser = reqparse.RequestParser()

parser.add_argument('role_id', required=False, help='This field cannot be left blank')
parser.add_argument('role_name', required=False, help='Must enter the role_name')



class InvoiceList(Resource):
    
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')


        query, params = InvoiceModel.return_all(page, searchTerm)
   
        per_page = 10
        result, cus_paging = custom_paginate(query, page, per_page,params)
        data = list(map(lambda x: InvoiceModel.to_json(x), result))

        return {
                'status': 200,
                'success': True,
                'message': 'Invoice received',
                'data':data,
                'paginate': cus_paging
                }, 200
    


class InvoiceView(Resource):
    
    @jwt_required
    def get(self,id):

        invoice_list = InvoiceModel.get_by_invoiceid(id)
        invoice_data = InvoiceModel.to_customjson(invoice_list)

        company_list = CompanyModel.find_by_company_id(invoice_data['company_id'])
        company_data = CompanyModel.to_json(company_list)

        package_list = PackageModel.get_by_id(invoice_data['package_id'])
        package_data = PackageModel.to_json(package_list)

        invoice_data['company_name'] = company_data['company_name']
        invoice_data['email'] = company_data['email']
        invoice_data['contact_person'] = company_data['contact_person']
        invoice_data['mobile'] = company_data['mobile']

        invoice_data['package_name'] = package_data['package_name']
        invoice_data['package_price'] = package_data['package_price']
        invoice_data['package_duration'] = package_data['package_duration']



        try:
            rendered = render_template('invoice.html',invoice = invoice_data)
            pdf = pdfkit.from_string(rendered, False)

            response = make_response(pdf)
            response.headers['Content-Type'] = 'application/pdf'
            response.headers['Content-Disposition'] = 'inline; filename=output.pdf'

            return response
            
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'data':str(e),
                'message': 'An error occurred updating the packages',
            }, 400
       

            

