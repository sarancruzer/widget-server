from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from models.category import CategoryModel

from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate
from models.user import UserModel
from resources.questionResources import QuestionList
from models.question import QuestionModel
from models.playBook import PlayBookModel
import random
import string
from models.playBookResponseDetail import PlayBookResponseModel
from models.playBookReplyOption import PlayBookReplyOptionModel
from models.playBookApiDetail import PlayBookApiDeatailsModel
from models.playBookHeaderDetails import PlayBookHeaderDetailsModel



parser = reqparse.RequestParser()

parser.add_argument('playBook_id', required=False, help='This field cannot be left blank')
parser.add_argument('website_id', required=False, help='This field cannot be left blank')
parser.add_argument('playBook_name', required=False, help='Must enter the category_name')
parser.add_argument('playBook_init_question', required=False, help='Must enter the category_name')
parser.add_argument('process_type', required=False, help='This field cannot be left blank')



class PlayBook(Resource):
    def get(self):
        item = PlayBookModel.get_all_playBook()
        data = list(map(lambda x: PlayBookModel.to_json(x), item))

        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'question received',
                'data': data
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
class CreatePlayBook(Resource):
    
    @jwt_required
    def post(self):
        
        params = parser.parse_args()
   
        if PlayBookModel.find_by_name(params['playBook_name'],params['website_id']):
            return {
                'status': 409,
                'success': False,
                'message': 'Playbook Name {} already exists'.format(params['playBook_name']),
            },409

        current_user = get_jwt_identity()

        userId = current_user['user_id']
        
        rand = (''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(8)))

        new_data = PlayBookModel(
            website_id = params['website_id'],
            playBook_name=params['playBook_name'],
            status=1
        )
        
        try:
            new_data.save_to_db()
            added_data = PlayBookModel.find_by_name(params['playBook_name'],params['website_id'])
            
            playBook_data = PlayBookModel.to_json(added_data)
            qus_data = QuestionModel(
            user_id=userId,
            class_name=rand,
            website_id=params['website_id'],
            sentence=params['playBook_init_question'],
            category_id=1,
            response='context_sentence',
            status=1,
            publish_status=1,
            conversation_status=1,
            playbook_id = playBook_data['playbook_id']
            )
            qus_data.save_to_db()
            
            return {
                    'status': 200,
                    'success': True,
                    'message': 'PlayBook '+params['playBook_name']+' has been created',
                    'data': PlayBookModel.to_json(new_data)
                }, 200

        except Exception as e:
            print(e)
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the category',
            }, 400
            

class getPlayBookData(Resource):
    def get(self,playbook_id,question_id):
        print(playbook_id+' '+question_id)
        item = QuestionModel.get_by_id(question_id)
        item = QuestionModel.to_json(item)
        print("IIIIIIIIIIITTTEEEEEEEMMMMMMMMMM")
        print(item)
        item['playBook_init_question'] = item['sentence']
        playbook_group_record = PlayBookResponseModel.get_by_playbook_id_edit(item['playbook_id'])
        playbook_group_record = list(map(lambda x: PlayBookResponseModel.to_json(x), playbook_group_record))

        item['question_node'] = playbook_group_record
        for idx,value in enumerate(item['question_node']):
            value['sentence'] = value['text_response']
            value['nodeType'] = value['node_type']
            value['nodeId'] = value['node_id']
            
            value['json_api_header']=[]
            if value["node_type"] == "3" or value["node_type"] == "4":
                
                get_options = PlayBookReplyOptionModel.get_option_node(playbook_id,value['node_id'],value["node_type"],value['branch_id'],value['sequence'])
                get_options = list(map(lambda x: PlayBookReplyOptionModel.to_json(x), get_options))
                for option in get_options:
                    option['newBranch']=[]
                    option['response'] = option['option_value']
                value["response_options"]=get_options
                if value["node_type"] == "4":
                    for response in value["response_options"]:
                        response['response'] = response['option_value']
                        sub_playbook_group_record = PlayBookResponseModel.get_by_playbook_branch(response['playbook_id'],response['node_id'],response['playbk_reply_opt_id'])
                        sub_playbook_group_record = list(map(lambda x: PlayBookResponseModel.to_json(x), sub_playbook_group_record))
                        response['newBranch']=sub_playbook_group_record

                        for new in response['newBranch']:
                            new['sub_nodeType']=new['node_type']
                            new['sub_jump_playbook']=new['jump_playbook']
                            new['sub_jump_node_id']=new['node_id']
                            new['sub_sentence']=new['text_response']
                            new['sub_json_api_type']=''
                            new['sub_json_api_url']=''
                            new['sub_json_api_body']=''
                            new['sub_json_api_header']=''
                            get_sub_options = PlayBookReplyOptionModel.get_option_node_branch(playbook_id,new['node_id'],new["playbk_resp_det_id"])
                            get_sub_options = list(map(lambda x: PlayBookReplyOptionModel.to_json(x), get_sub_options))
                            new['newBranch_options'] =get_sub_options 
            if value["node_type"] == "7":
                get_api_detail = PlayBookApiDeatailsModel.find_by_palyBook_node_id(playbook_id,value['node_id'])
                get_api_detail = list(map(lambda x: PlayBookApiDeatailsModel.to_json(x), get_api_detail))
                print(get_api_detail[0])
                value['json_api_url'] = get_api_detail[0]['api_url']
                value['json_api_type'] = get_api_detail[0]['api_method']
                get_api_header_detail = PlayBookHeaderDetailsModel.find_by_api_id(get_api_detail[0]['playBook_api_detail_id'])
                print("AAAAAAAAAAAAAAAAAAAA")
                print(get_api_header_detail)
                get_api_header_detail = list(map(lambda x: PlayBookHeaderDetailsModel.to_json(x), get_api_header_detail))
                value['json_api_header']=get_api_header_detail
        return {
                    'status': 200,
                    'success': True,
                    'message': 'PlayBook detail has been created',
                    'data': item
                }, 200
   

class SavePlayBookData(Resource):
    def post(self):

        params = parser.parse_args()     
        node = request.get_json(silent=True)
        ques_node = node['question_node']
        
        print(ques_node)
        print("DEEETTAAAIILLL ques_node")
        print(params['process_type'])
        
        if params['process_type']=='edit':
            playBookResponse = PlayBookResponseModel.clear_all_by_playbook_id(params['playBook_id'])
            # playBookResponse.delete_from_db()
            # playBookOption = PlayBookReplyOptionModel.get_by_playBook_id(params['playBook_id'])
            # playBookOption.delete_from_db()
            # apiDetail = PlayBookApiDeatailsModel.find_by_node_id(params['playBook_id'])
            # apiDetail.delete_from_db()
            # apiHeader = PlayBookHeaderDetailsModel.find_by_playBook_id(params['playBook_id'])
            # apiHeader.delete_from_db()
            print("999999999999999999999999999999999999999")
            print(params)
            new_data = QuestionModel.get_by_playbookid(params['playBook_id'])
            print(new_data)
            if new_data:
                new_data.sentence = params['playBook_init_question']
                new_data.publish_status = 1
                new_data.save_to_db()
        if params['process_type']=='edit': message = 'PlayBook details has been updated..!'
        else: message = 'PlayBook details has been saved..!'
        for idx,value in enumerate(ques_node):
            # print(idx)
            print(value)
            
            response_data = PlayBookResponseModel(
                    playbook_id=params['playBook_id'],
                    node_id=idx+1,
                    node_type=value['nodeType'],
                    sequence=idx+1,
                    text_response=value['sentence'],
                    wait_status=0,
                    jump_node=value['jump_node_id'],
                    jump_playbook=value['jump_playbook'],
                    branch_id=0,
                    status=1
            )
            if response_data.node_type == '1' or response_data.node_type == '7':
                response_data.wait_status = 0
            else:
                response_data.wait_status = 1
            response_data.save_to_db()
            print("LLLLLLLLLLLLAST INSERTED ID")
            print(response_data.playbk_resp_det_id)
            
            if value['nodeType'] == '3' or value['nodeType'] == '4':
                last_inner=''
                for option in value['response_options']:
                    print("option values")
                    print(option)
                    reply_option_data = PlayBookReplyOptionModel(
                        playbook_id=params['playBook_id'],
                        node_id=idx+1,
                        option_value=option['response'],
                        status=1
                    )
                    if value['nodeType'] == '4':
                        reply_option_data = PlayBookReplyOptionModel(
                            playbook_id=params['playBook_id'],
                            node_id=idx+1,
                            option_value=option['response'],
                            status=1,
                            is_branch=1
                        )
                    reply_option_data.save_to_db()
                    
                    if value['nodeType'] == '4':
                        added_option_data = PlayBookReplyOptionModel.find_last_added_record(params['playBook_id'],option['response'],idx+1)
                        playBook_data = PlayBookReplyOptionModel.to_json(added_option_data)
                        inner = 1
                        print("IIIIIIIIIIIINNNNNNNNNNNNNNNNNN")
                        print(str(last_inner)+' '+str(playBook_data['playbk_reply_opt_id']))
                        if last_inner!='':    
                            if last_inner!=playBook_data['playbk_reply_opt_id']:
                                inner += 1
                        for sub_ind,new in enumerate(option['newBranch']):
                            reply_response_data = PlayBookResponseModel(
                                playbook_id=params['playBook_id'],
                                node_id=idx+1,
                                node_type=new['sub_nodeType'],
                                sequence=sub_ind+1,
                                text_response=new['sub_sentence'],
                                wait_status=0,
                                inner_node = inner,
                                jump_node=new['sub_jump_node_id'],
                                jump_playbook=new['sub_jump_playbook'],
                                branch_id=playBook_data['playbk_reply_opt_id'],
                                status=1
                            )
                            if reply_response_data.node_type == '1' or reply_response_data.node_type == '7':
                                reply_response_data.wait_status = 0
                            else:
                                reply_response_data.wait_status = 1
                            reply_response_data.save_to_db()
                            last_inner = reply_response_data.branch_id
                            added_subBranch_data = PlayBookResponseModel.find_last_added_record(params['playBook_id'],new['sub_sentence'],sub_ind+1)
                            subBranch_data = PlayBookResponseModel.to_json(added_subBranch_data)
                            
                            if new['sub_nodeType'] == '3':
                                
                                for sub_option in new['newBranch_options']:
                                    sub_options = PlayBookReplyOptionModel(
                                        playbook_id=params['playBook_id'],
                                        node_id=idx+1,
                                        option_value=sub_option['option_value'],
                                        status=1,
                                        branch_id=subBranch_data['playbk_resp_det_id']
                                    )
                                    sub_options.save_to_db()
                            if new['sub_nodeType'] == '7':
                                sub_api_detail_data = PlayBookApiDeatailsModel(
                                    playbook_id=params['playBook_id'],
                                    node_id=idx+1,
                                    branch_id=0,
                                    api_url=value['json_api_url'],
                                    option_id=reply_option_data.playbk_reply_opt_id,
                                    api_method=value['json_api_type'],
                                    api_response_type='JSON',
                                    status=1
                                )
                                sub_api_detail_data.save_to_db()
                              
                                for sub_header_option in new['sub_json_api_header']:
                                    sub_api_header = PlayBookHeaderDetailsModel(
                                        playbook_id=params['playBook_id'],
                                        node_id=idx+1,
                                        header_key=sub_header_option['header_key'],
                                        header_value=sub_header_option['header_value'],
                                        api_detail_id=sub_api_detail_data.playBook_api_detail_id,
                                        status=1,
                                    )
                                    sub_api_header.save_to_db()
                        #inner += 1
            if value['nodeType'] == '7':
                api_detail_data = PlayBookApiDeatailsModel(
                    playbook_id=params['playBook_id'],
                    node_id=idx+1,
                    branch_id=0,
                    api_url=value['json_api_url'],
                    option_id=0,
                    api_method=value['json_api_type'],
                    api_response_type='JSON',
                    status=1
                )
                api_detail_data.save_to_db()
                
                for header in value['json_api_header']:
                    api_header = PlayBookHeaderDetailsModel(
                        playbook_id=params['playBook_id'],
                        node_id=idx+1,
                        header_key=header['header_key'],
                        header_value=header['header_value'],
                        api_detail_id=api_detail_data.playBook_api_detail_id,
                        status=1,
                    )
                    api_header.save_to_db()

        return {
                    'status': 200,
                    'success': True,
                    'message': message,
                    #'data': PlayBookModel.to_json(new_data)
                }, 200

