import nltk
import json
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
from models.question import QuestionModel
from flask_jwt_extended.utils import get_jwt_identity, create_access_token
from models.user import UserModel
from models.contextGroup import ContextGroupModel
from models.contextGroupDetails import ContextGroupDetailsModel
from models.contextResponse import ContextResponseModel
from models.website import WebsiteModel
from models.chatbotResponse import ChatbotResponseModel
from shared import db
import app
from logging.handlers import RotatingFileHandler
import logging
import os
from sqlalchemy.sql.functions import now
from models.company import CompanyModel
from models.packResponseCount import PackResponseCountModel

from datetime import datetime
from dateutil.relativedelta import relativedelta
from models.chatHistoryDetails import ChatHistoryDetailsModel
from resources.restrictionResources import ResponseCount
from autocorrect import spell# word stemmer
from models.playBookResponseDetail import PlayBookResponseModel
from models.playBookReplyOption import PlayBookReplyOptionModel
from flask import request
from models.playBookApiDetail import PlayBookApiDeatailsModel
from models.playBookHeaderDetails import PlayBookHeaderDetailsModel
import requests

from faq_model.deeppavlovResource.faq.tfidf_logreg_en_faq import interact as faq_logreg

stemmer = LancasterStemmer()

sample = []
training_data = []


def out_fun(filepath):
    with open(filepath, 'r') as f:
        training_data = json.load(f)
        #print ("%s sentences in training data" % len(training_data))
        return training_data

# y = out_fun()
# print(y)


# # capture unique stemmed words in the training corpus
# corpus_words = {}
# class_words = {}
# # turn a list into a set (of unique items) and then a list again (this removes duplicates)
# classes = list(set([a['class'] for a in y]))
# for c in classes:
#     # prepare a list of words within each class
#     class_words[c] = []

# # loop through each sentence in our training data
# for data in y:
#     # tokenize each sentence into words
#     for word in nltk.word_tokenize(data['sentence']):
#         # ignore a some things
#         if word not in ["?", "'s"]:
#             # stem and lowercase each word
#             stemmed_word = stemmer.stem(word.lower())
#             # have we not seen this word already?
#             if stemmed_word not in corpus_words:
#                 corpus_words[stemmed_word] = 1
#             else:
#                 corpus_words[stemmed_word] += 1

#             # add the word to our words in class list
#             class_words[data['class']].extend([stemmed_word])



# def calculate_class_score_commonality(sentence, class_name, show_details=True):
#     score = 0
#     # tokenize each word in our new sentence
#     for word in nltk.word_tokenize(sentence):
#         # check to see if the stem of the word is in any of our classes
#         if stemmer.stem(word.lower()) in class_words[class_name]:
#             # treat each word with relative weight
#             score += (1 / corpus_words[stemmer.stem(word.lower())])

#             if show_details:
#                 print ("   match: %s (%s)" % (stemmer.stem(word.lower()), 1 / corpus_words[stemmer.stem(word.lower())]))
#     return score

   

# def classify(sentence):
#     high_class = None
#     high_score = 0
#     for c in class_words.keys():
#         score = calculate_class_score_commonality(sentence, c, show_details=False)
#         if score > high_score and score > 0.1:
#             high_class = c
#             high_score = score


#     resList = responses(high_class,y)
#     return resList

def classify(sentence,yy):


    # capture unique stemmed words in the training corpus
    corpus_words = {}
    class_words = {}
    # turn a list into a set (of unique items) and then a list again (this removes duplicates)
    classes = list(set([a['class'] for a in yy]))
    for c in classes:
        # prepare a list of words within each class
        class_words[c] = []

    print("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
    print(yy)
    print("SENTENCE SENTENCE SENTENCE SENTENCE")
    print(sentence)    
    
    # loop through each sentence in our training data
    for data in yy:
        # tokenize each sentence into words
        for word in nltk.word_tokenize(data['sentence']):
            # ignore a some things
            if word not in ["?", "'s"]:
                # stem and lowercase each word
                stemmed_word = stemmer.stem(word.lower())
                # have we not seen this word already?
                if stemmed_word not in corpus_words:
                    corpus_words[stemmed_word] = 1
                else:
                    corpus_words[stemmed_word] += 1

                # add the word to our words in class list
                class_words[data['class']].extend([stemmed_word])



    
    high_class = None
    high_score = 0
    
  
    for c in class_words.keys():

        # score = calculate_class_score_commonality(sentence, c, show_details=False)

        score = 0
        # tokenize each word in our new sentence
        for word in nltk.word_tokenize(sentence):
            # check to see if the stem of the word is in any of our classes
            if stemmer.stem(word.lower()) in class_words[c]:
                # treat each word with relative weight
                score += (1 / corpus_words[stemmer.stem(word.lower())])

        print("SSSSSSSSCCCCCCCCCCOOOOOOOORRRRRRREEEEEEEEEEE")
        print(score)
        if score > high_score and score > 0.40:
            high_class = c
            high_score = score


    # resList = responses(high_class,yy)
    return high_class,yy 


from datetime import datetime


def askqFunc(data):

    sentence = data['sentence']
    apikey = data['api_key']
    context = data['context']
    context_token = data['context_token']
    response = data['response']
    context_res_id = data['context_res_id']
   
    user_token = data['user_token']
    data['deepavluv'] ='yes'
    userModelRes = UserModel.get_by_api_key(apikey)    
    website_data = WebsiteModel.get_by_api_key(apikey)
    
    # website_data = WebsiteModel.to_json(website_list)

    company_list = CompanyModel.find_by_company_id(website_data['company_id'])
    company_data = CompanyModel.to_json(company_list)


    api_key = data['api_key']
        
    query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"'"

    package_details = db.db.engine.execute(query)
    pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]
    RESPONSE_PERMIT_COUNT = pack_result['response_count']
    RESPONSE_CURRENT_COUNT = 0

    resultList = PackResponseCountModel.get_by_api_key(api_key)
    
        
    result = []
    if resultList:
        result = PackResponseCountModel.to_json(resultList)
        RESPONSE_CURRENT_COUNT = result['response_count']

    if RESPONSE_CURRENT_COUNT > RESPONSE_PERMIT_COUNT:
        
        return {
                'status': 400,
                'success': False,
                'message': 'An error occurred getting the response',
            }, 400

    rList = {'CLASS':'','QUESTION':'','HREF_LINK':'', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': '', 'RAW_TEXT': '', 'SUGGESTIONS_TEXT': []}
    if data['playBook_id']:
            print("DDDDDDDDDDDAAAAAAAAAAAAAATTTTTTTTTTTTAAAAAAAAAAAAA")
            print(data)
            if(data['selected_nodeType'] == "3" or data['selected_nodeType'] == "2"):
                if(data['selected_nodeType'] == "3"):
                    play_book_msg = PlayBookResponseModel.get_by_playbook_limit(data['playBook_id'],data['selected_option']['node_id'],data['selected_option']['branch_id'],data['conversation_sequence'],data['inner_node'])
                if(data['selected_nodeType'] == "2"):

                    node = request.get_json(silent=True)
                    option_node = node['selected_option']
                    data['selected_option'] = option_node

                    print("PPPPPLAAAAAAAAAAAAAAYYYYYYY BBBBBOOOOOOOOOKKKKKKKKKK")
                    print(option_node)

                    play_book_msg = PlayBookResponseModel.get_by_playbook_limit(data['playBook_id'],data['node_id'],data['branch_id'],data['conversation_sequence'],data['inner_node'])

                print(play_book_msg)
                if play_book_msg:
                    get_palybook_data = list(map(lambda x: PlayBookResponseModel.to_json(x), play_book_msg))
                    print("play_book_msg4444444444444444444444444444444444sssssssss")
                    print(get_palybook_data)
                    

                    data_list=[]
                    test_list=[]
                    wait_ind=100
                    
                    for idxy,datas in enumerate(get_palybook_data):
                        if datas['wait_status']==1:
                            wait_ind = idxy
                            break
                    for idx,datass in enumerate(get_palybook_data):
                        
                        
                        #if datass['wait_status']==0 and datass['sequence']==idx+1 and idx < wait_ind:
                        if datass['wait_status']==0 and idx < wait_ind:
                            #json api check start
                            if datass['node_type']=='7':
                                get_api_by_node = PlayBookApiDeatailsModel.find_by_node_id(data['playBook_id'])
                                if get_api_by_node:
                                    get_api_by_node = list(map(lambda x: PlayBookApiDeatailsModel.to_json(x), get_api_by_node))
                                  
                                    get_api_header = PlayBookHeaderDetailsModel.find_by_api_id(get_api_by_node[0]['playBook_api_detail_id'])
                                    if get_api_header:
                                        get_api_header = list(map(lambda x: PlayBookHeaderDetailsModel.to_json(x), get_api_header))

                                    API_KEY = '13076a9b6217587cbe0cfcb16b965c84'
                                    url = get_api_by_node[0]['api_url']
                                    headers=[]
                                    for head in get_api_header:
                                        head['header_value']= "'%s'" % head['header_value']
                                        val = head['header_key']+'='+head['header_value']
                                        headers.append(val)
                                        s = ', '
                                        header = s.join(headers)
                                    params = dict(access_key='13076a9b6217587cbe0cfcb16b965c84', currencies='EUR,GBP,CAD,PLN', source='USD', format='1')
                                    #header = eval(header)
                                    print("*******************************")
                                    print(header)
                                    #params = dict('',header)
                                    # for value in get_api_header:
                                    #     pass
                                    res = (requests.get(url,params=params))
                                    print((res.text))
                                    datass['text_response'] =res.text
                           
                            #json api check start
                            data_list.append(datass)
                            
                        #if datass['wait_status']!=0 and datass['sequence']==idx+1:
                        if datass['wait_status']!=0:
                            test_list.append(datass)
                    if len(test_list):               
                        data_list.append(get_palybook_data[len(data_list)])
                    print("PPPPPPPPPPPPOOOOOOOOOOOOOOOOOOOOOOOOOOO")
                    print(test_list)

                    if len(data_list) and data_list[-1]['node_type'] == "6":
                        print("44444444444444444444555555555555555555555555555555")
                        play_book_by_id = PlayBookResponseModel.get_by_playbook_id(test_list[-1]['jump_playbook'],'res')  
                        get_palybook_data_by_id = list(map(lambda x: PlayBookResponseModel.to_json(x), play_book_by_id))
                        
                        wait_ind=100
                        for idxy,data_id in enumerate(get_palybook_data_by_id):
                            #print(get_palybook_data_by_id)
                            
                            if data_id['wait_status']==1:
                                wait_ind = idxy
                                break
                        for idx,datasss in enumerate(get_palybook_data_by_id):
                            if datasss['wait_status']==0 and datasss['sequence']==idx+1 and idx < wait_ind:
                                data_list.append(datasss)
                            print("+++++++++++++++++++++++++")
                            print(datasss)
                            if datasss['wait_status']!=0 and datasss['sequence']==idx+1:
                                test_list.append(datasss)
                                print(len(test_list))
                                data_list.append(test_list[-1])
                                
                    for opt_list in (data_list):
                        
                        if opt_list['node_type'] == '3' or opt_list['node_type'] == '4':
                            get_node_option = PlayBookReplyOptionModel.get_option_node(opt_list['playbook_id'],opt_list['node_id'],opt_list['node_type'],opt_list['branch_id'],opt_list['playbk_resp_det_id'])
                            get_palybook_option = list(map(lambda x: PlayBookReplyOptionModel.to_json(x), get_node_option))

                            data_list[len(data_list)-1]['options'] = get_palybook_option
                    
                    rList['QUESTION'] = sentence
                    rList['PLAYBOOK_TEXT'] = data_list
                    rList['PLAYBOOK_ID'] = data['playBook_id']
                    rList['HREF_LINK'] = ""
                    rList['AUDIO_LINK'] = ""
                    rList['VIDEO_LINK'] = ""
                    rList['IMAGE_LINK'] = ""
                    rList['SUGGESTIONS_TEXT'] = []
                    rList['RESPONSE_TYPE'] = ""
                    rList['CONTEXT'] = ""
                    rList['USER_TOKEN'] = ""
                    rList['CONTEXT_RES_ID'] = ""
                
                    for id,val in enumerate(rList['PLAYBOOK_TEXT']):
                        if id > 0:
                            sentence=''
                        chat_res_data = ChatHistoryDetailsModel(
                            context_name =  '',
                            sentence =  sentence,
                            response =  val['text_response'],
                            chat_res_id = data['chat_history_id']
                            )
                        chat_res_data.save_to_db()
                    return rList
            
            if(data['selected_nodeType'] == "4"):
                
                jump_node = 0
                play_book_msg_branch = PlayBookResponseModel.get_by_playbook_branch(data['playBook_id'],data['selected_option']['node_id'],data['selected_option']['playbk_reply_opt_id'])
                if play_book_msg_branch:
                    
                    get_palybook_data = list(map(lambda x: PlayBookResponseModel.to_json(x), play_book_msg_branch))
                    print(get_palybook_data)
                    
                    data_list=[]
                    test_list=[]
                    wait_ind=100
                    for idxy,datas in enumerate(get_palybook_data):
                        if datas['wait_status']==1:
                            wait_ind = idxy
                            break
                    for idx,datass in enumerate(get_palybook_data):
                       
                        #if datass['wait_status']==0 and datass['sequence']==idx+1 and idx < wait_ind:
                        if datass['wait_status']==0 and idx < wait_ind:
                            data_list.append(datass)
                        #if datass['wait_status']!=0 and datass['sequence']==idx+1:
                        if datass['wait_status']!=0:
                            test_list.append(datass)
                    if len(test_list):                
                        data_list.append(get_palybook_data[len(data_list)])
                    print("7777777777777777777777777777777777777777RRRRRRRRRR88888888888888888")
                    print(data_list)
                    if data_list[-1]['wait_status'] == 0:
                            
                            #get_data_after_wait_status = PlayBookResponseModel.get_by_playbook_limit(data['playBook_id'],get_palybook_data[-1]['node_id'],data['selected_option']['branch_id'],data['conversation_sequence'],data['inner_node'])
                            get_data_after_wait_status = PlayBookResponseModel.get_by_playbook_id(data['playBook_id'],data_list[-1]['node_id'])
                            #get_data_after_wait_status = PlayBookResponseModel.get_by_playbook_branch(data['playBook_id'],data['selected_option']['node_id'],data['selected_option']['playbk_reply_opt_id'])
                            if get_data_after_wait_status:
                                get_palybook_wait_data = list(map(lambda x: PlayBookResponseModel.to_json(x), get_data_after_wait_status))
                                
                                for idxy,wait_status in enumerate(get_palybook_wait_data):
                                    if wait_status['wait_status']==1:
                                        wait_ind = idxy
                                        break
                                for idx,wait_status_msg in enumerate(get_palybook_wait_data):
                                    print("7777777777777777777777777777777777777777RRRRRRRRRRaaa")
                                    print(wait_status_msg)
                                    print(idx)
                                    print(wait_ind)

                                    
                                    if wait_status_msg['wait_status']==0 and idx < wait_ind:
                                        data_list.append(wait_status_msg)
                                    
                                    if wait_status_msg['wait_status']!=0:
                                        test_list.append(wait_status_msg)
                                    if wait_ind ==0:
                                        test_list.append(get_palybook_wait_data[0])
                            if len(test_list):                
                                    #data_list.append(get_palybook_data[len(data_list)])
                                    data_list.append(test_list[-1])
                    elif data_list[-1]['wait_status'] == 1 and data_list[-1]['node_type'] == '5':    
                            
                            get_data_after_jump_node = play_book_msg = PlayBookResponseModel.get_by_playbook_jumpid(data['playBook_id'],data_list[-1]['jump_node'])
                            print("AAAAAAAAAAFFFFFFFFFFTTTTTTRRRRRRRR")
                            print(get_data_after_jump_node)
                            if get_data_after_jump_node:
                                get_palybook_jump_node = list(map(lambda x: PlayBookResponseModel.to_json(x), get_data_after_jump_node))
                                print(get_palybook_jump_node)
                                print("7777777777777777777777777777777777777777RRRRRRRRRReeeeee")
                                # data_list=[]
                                # test_list=[]
                                # wait_ind=100
                                for idxy,jump_status in enumerate(get_palybook_jump_node):
                                    if jump_status['wait_status']==1:
                                        wait_ind = idxy
                                        break

                                for idx,jump_node_msg in enumerate(get_palybook_jump_node):
                                    
                                    if jump_node_msg['wait_status']==0 and jump_node_msg['sequence']==idx+1 and idx < wait_ind:
                                        data_list.append(jump_node_msg)
                                        print("jump_node_msgPPPPPPPPPPPPPPPPPPPP")
                                        print(wait_ind)
                                        print(jump_node_msg)
                                        print("=====================")
                                    if jump_node_msg['wait_status']!=0 and jump_node_msg['sequence']==idx+1:
                                        print("jump_node_msgPPPPPPPPPPPPPPPPPPPP")
                                        print(jump_node_msg)
                                        test_list.append(jump_node_msg)
                                if len(test_list):                
                                    data_list.append(test_list[0])
                                    jump_node = 1
                    elif len(test_list) and test_list[-1]['node_type'] == "6":
                       
                        play_book_by_id = PlayBookResponseModel.get_by_playbook_id(test_list[-1]['jump_playbook'],'res')  
                        get_palybook_data_by_id = list(map(lambda x: PlayBookResponseModel.to_json(x), play_book_by_id))
                        test_list=[]
                        wait_ind=100
                        for idxy,data_id in enumerate(get_palybook_data_by_id):
                    
                            if data_id['wait_status']==1:
                                wait_ind = idxy
                                break
                        print(wait_ind)
                        print("pppppppppppppp%$$$$$$$$$$$$$$$$$1111111")
                        for idx,datasss in enumerate(get_palybook_data_by_id):
                            if datasss['wait_status']==0 and datasss['sequence']==idx+1 and idx < wait_ind:
                                data_list.append(datasss)
                                print(datasss)
                                print("pppppppppppppp%$$$$$$$$$$$$$$$$$222222")
                            if datasss['wait_status']!=0 and datasss['sequence']==idx+1:
                                print(datasss)
                                print("pppppppppppppp%$$$$$$$$$$$$$$$$$33333333")
                                test_list.append(datasss)
                                print((test_list))
                        if len(test_list):
                            data_list.append(test_list[0])
                    print(data_list)
                    print("jump_node_msg['playbk_resp_det_id']")
                    for opt_list in (data_list):
                        
                        print(opt_list['node_type'])
                        if opt_list['node_type'] == '3' or opt_list['node_type'] == '4':
                            print("CCHECKKKKKKKKKKKKKKKKK NNNNNOOOOODDDDDDEeee")
                            print(opt_list['node_id'])
                            print(data['selected_option']['node_id'])

                            if jump_node==0:
                                if data['selected_option']['node_id'] == opt_list['node_id']:

                                    get_node_option = PlayBookReplyOptionModel.get_option_node_branch(opt_list['playbook_id'],opt_list['node_id'],opt_list['playbk_resp_det_id'])
                                else:
                                    get_node_option = PlayBookReplyOptionModel.get_option_node(opt_list['playbook_id'],opt_list['node_id'],opt_list['node_type'],opt_list['branch_id'],opt_list['playbk_resp_det_id'])
                            else:
                                #get_node_option = PlayBookReplyOptionModel.get_option_node(opt_list['playbook_id'],opt_list['node_id'],'')

                                get_node_option = PlayBookReplyOptionModel.get_option_node(opt_list['playbook_id'],opt_list['node_id'],opt_list['node_type'],opt_list['branch_id'],opt_list['playbk_resp_det_id'])

                            get_palybook_option = list(map(lambda x: PlayBookReplyOptionModel.to_json(x), get_node_option))

                            data_list[-1]['options'] = get_palybook_option
                            
                    rList['QUESTION'] = sentence
                    rList['PLAYBOOK_TEXT'] = data_list
                    rList['PLAYBOOK_ID'] = data['playBook_id']
                    rList['HREF_LINK'] = ""
                    rList['AUDIO_LINK'] = ""
                    rList['VIDEO_LINK'] = ""
                    rList['IMAGE_LINK'] = ""
                    rList['SUGGESTIONS_TEXT'] = []
                    rList['RESPONSE_TYPE'] = ""
                    rList['CONTEXT'] = ""
                    rList['USER_TOKEN'] = ""
                    rList['CONTEXT_RES_ID'] = ""

                    for id,val in enumerate(rList['PLAYBOOK_TEXT']):
                        if id > 0:
                            sentence=''
                        chat_res_data = ChatHistoryDetailsModel(
                            context_name =  '',
                            sentence =  sentence,
                            response =  val['text_response'],
                            chat_res_id = data['chat_history_id']
                            )
                        chat_res_data.save_to_db()

                    return rList
    faq_algorithm = 'faq_config_json/'+company_data['company_code']+'/'+website_data['website_code']+'.json'
    with open(faq_algorithm, 'r+') as f:
        algo_data = json.load(f)
    
    print("OOOOOOOOPEEEEEEEENNNN JASONNNNNNNNN")
    print(algo_data['config'][0])
    if context and context_token:
        # updateContextualProcess(context,context_token,response,context_res_id)
        contextRes, contextGp, contextGpDetails,context_token,conv_status = updateContextualProcess(context,context_token,sentence,context_res_id)
        if conv_status == True:

            contextSuccessMsg = getContextualSuccessMsg(context,website_data['website_id'])

            rList['RAW_TEXT'] = contextSuccessMsg
            rList['CONTEXT_RES_ID'] = ""
            rList['SUGGESTIONS_TEXT'] = ""
            rList['RESPONSE_TYPE'] = ""
            rList['CONTEXT'] = ""
        else:
            rList['RAW_TEXT'] = contextRes[0]['sentence']
            rList['CONTEXT_RES_ID'] = contextRes[0]['context_resp_id']
            rList['SUGGESTIONS_TEXT'] = contextGpDetails['context_group_details']
            rList['RESPONSE_TYPE'] = contextGp['response_type']
            rList['CONTEXT'] = context
            

        rList['QUESTION'] = ""
        rList['HREF_LINK'] = ""
        rList['AUDIO_LINK'] = ""
        rList['VIDEO_LINK'] = ""
        rList['IMAGE_LINK'] = ""
        rList['USER_TOKEN'] = context_token
    
    

   
    # elif algo_data['config'][0]['faq_algorithm']=='deeppavlov':
 
    #     if app.app.config['ENV']=='development':

    #         dest = '/var/www/html/widget-dev/widget-server/faq_model/deeppavlovResource/configs/'+company_data['company_code']+'/'+website_data['website_code']+'/tfidf_logreg_en_faq.json'
    #     elif app.app.config['ENV']=='production':
    #         dest = '/var/www/html/widget-server/faq_model/deeppavlovResource/configs/'+company_data['company_code']+'/'+website_data['website_code']+'/tfidf_logreg_en_faq.json'
      
    #     faq_result = faq_logreg.FaqInteractService.faq_interact(sentence,dest)
     
    #     result = faq_result[0][0][0]
    #     score = faq_result[1]
    #     print(result)
    #     print(score)
    #     high_class = 0
    #     for scr in score[0]:
    #         print(scr)
    #         if scr > 0.4:
    #             high_class = 1
    #     qresult = QuestionModel.get_by_classname(result,website_data['website_id'])

    #     if high_class == 1:
    #         rList['QUESTION'] = sentence
    #         rList['RAW_TEXT'] = qresult.response
    #         rList['HREF_LINK'] = qresult.href_link
    #         rList['AUDIO_LINK'] = qresult.audio_link
    #         rList['VIDEO_LINK'] = qresult.video_link
    #         rList['IMAGE_LINK'] = qresult.image_link
    #         rList['CATEGORY'] =  qresult.category_id
    #         rList['SUGGESTIONS_TEXT'] = []
    #         rList['RESPONSE_TYPE'] = ""
    #         rList['CONTEXT'] = ""
    #         rList['USER_TOKEN'] = ""
    #         rList['CONTEXT_RES_ID'] = ""
    #     else:
    #         suggests=['About your company','Mission of your company?','What is iPITCH?','Do you have Business Incubation service?','Talk to our Agent']
    #         rList['QUESTION'] = sentence
    #         rList['RAW_TEXT'] = "I am unsure what you have asked for let me give you some suggestions please select one to continue."
    #         rList['HREF_LINK'] = ""
    #         rList['AUDIO_LINK'] = ""
    #         rList['VIDEO_LINK'] = ""
    #         rList['IMAGE_LINK'] = ""
    #         rList['CATEGORY'] = ""
    #         rList['SUGGESTIONS_TEXT'] = suggests
    #         rList['RESPONSE_TYPE'] = "BUTTON"
    #         rList['CONTEXT'] = ""
    #         rList['USER_TOKEN'] = ""
    #         rList['CONTEXT_RES_ID'] = ""

    else:
        if algo_data['config'][0]['faq_algorithm']=='deeppavlov':
            if app.app.config['ENV']=='development':

                dest = '/var/www/html/widget-dev/widget-server/faq_model/deeppavlovResource/configs/'+company_data['company_code']+'/'+website_data['website_code']+'/tfidf_logreg_en_faq.json'
            elif app.app.config['ENV']=='production':
                dest = '/var/www/html/widget-server/faq_model/deeppavlovResource/configs/'+company_data['company_code']+'/'+website_data['website_code']+'/tfidf_logreg_en_faq.json'
      
            faq_result = faq_logreg.FaqInteractService.faq_interact(sentence,dest)
        
            result = faq_result[0][0][0]
            score = faq_result[1]
            print(result)
            print(score)
            high_class = 0
            for scr in score[0]:
                print(scr)
                if scr > 0.4:
                    high_class = 1
            qresult = QuestionModel.get_by_classname(result,website_data['website_id'])
            print("PPPPLAAAAAAAAYYYYY")
            print(qresult.playbook_id)
            if qresult.playbook_id != None and qresult.playbook_id != 0 and qresult.playbook_id != 'NULL':
                rList = responses(sentence,result,apikey,context_token,data['interest_category'])
                
            if high_class == 1:
                rList['QUESTION'] = sentence
                rList['RAW_TEXT'] = qresult.response
                rList['HREF_LINK'] = qresult.href_link
                rList['AUDIO_LINK'] = qresult.audio_link
                rList['VIDEO_LINK'] = qresult.video_link
                rList['IMAGE_LINK'] = qresult.image_link
                rList['CATEGORY'] =  qresult.category_id
                rList['SUGGESTIONS_TEXT'] = []
                rList['RESPONSE_TYPE'] = ""
                rList['CONTEXT'] = ""
                rList['USER_TOKEN'] = ""
                rList['CONTEXT_RES_ID'] = ""
            else:
                suggests=['About your company','Mission of your company?','What is iPITCH?','Do you have Business Incubation service?','Talk to our Agent']
                rList['QUESTION'] = sentence
                rList['RAW_TEXT'] = "I am unsure what you have asked for let me give you some suggestions please select one to continue."
                rList['HREF_LINK'] = ""
                rList['AUDIO_LINK'] = ""
                rList['VIDEO_LINK'] = ""
                rList['IMAGE_LINK'] = ""
                rList['CATEGORY'] = ""
                rList['SUGGESTIONS_TEXT'] = suggests
                rList['RESPONSE_TYPE'] = "BUTTON"
                rList['CONTEXT'] = ""
                rList['USER_TOKEN'] = ""
                rList['CONTEXT_RES_ID'] = ""
            
            if qresult.playbook_id != None and qresult.playbook_id != 0 and qresult.playbook_id != 'NULL':
                print("##################### RESULLTTTTTT PLAY")
                print(len(rList['PLAYBOOK_TEXT']))
                print(rList)
                for id,val in enumerate(rList['PLAYBOOK_TEXT']):
                    if id > 0:
                        sentence=''
                    chat_res_data = ChatHistoryDetailsModel(
                        context_name =  '',
                        sentence =  sentence,
                        response =  val['text_response'],
                        chat_res_id = data['chat_history_id']
                        )
                    chat_res_data.save_to_db()
            else:
                print("##################### RESULLTTTTTT FAQ")
                print(rList)
                
                chat_res_data = ChatHistoryDetailsModel(
                    context_name =  '',
                    sentence =  sentence,
                    response =  rList['RAW_TEXT'],
                    chat_res_id = data['chat_history_id']
                    )
                chat_res_data.save_to_db()
            query = "UPDATE pack_response_count SET response_count=response_count+1 WHERE api_key='"+ apikey +"'"
            result = db.db.engine.execute(query)

            current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            date_query = "UPDATE chat_history SET updated_at='"+current_date+"' WHERE chat_resp_id="+ data['chat_history_id']
            result = db.db.engine.execute(date_query)
            api_key = data['api_key']
            
            query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"'"
            
            query = query + "ORDER BY s.subs_id DESC"

            print(query)
            print("######################################")
            package_details = db.db.engine.execute(query)
            pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]
            print(pack_result)
            print("###############========================")
            RESPONSE_PERMIT_COUNT = pack_result['response_count']
            RESPONSE_CURRENT_COUNT = 0

            resultList = PackResponseCountModel.get_by_api_key(api_key)
                
            result = []
            if resultList:
                result = PackResponseCountModel.to_json(resultList)
                RESPONSE_CURRENT_COUNT = result['response_count']
            
            
            print("+++++++=============RESPONSE_PERMIT_COUNT==================+++++++++++++++++++")
            print(RESPONSE_PERMIT_COUNT)    
            print("+++++++==============RESPONSE_CURRENT_COUNT=================+++++++++++++++++++")
            print(RESPONSE_CURRENT_COUNT)    

            if RESPONSE_CURRENT_COUNT > RESPONSE_PERMIT_COUNT:
                
                return {
                        'status': 400,
                        'success': False,
                        'message': 'An error occurred getting the response',
                    }, 400
        else:
            directory_path = website_data['trained_json_path']
            # directory_path = "jsons/claritaz.json"
            yy = out_fun(directory_path)

            high_class, yy = classify(sentence,yy)
        
            rList = responses(sentence,high_class,apikey,context_token,data['interest_category'])


            # save every response into chatbot_response table
            # new_data = ChatbotResponseModel(
            #     company_id = website_data['company_id'],
            #     website_id =  website_data['website_id'],
            #     user_token =  user_token,
            #     context_name =  rList['CONTEXT'],
            #     sentence =  rList['QUESTION'],
            #     response =  rList['RAW_TEXT']
            #     )
            # new_data.save_to_db()
            print("#####################")
            print(rList)
            chat_res_data = ChatHistoryDetailsModel(
                context_name =  rList['CONTEXT'],
                sentence =  sentence,
                response =  rList['RAW_TEXT'],
                chat_res_id = data['chat_history_id']
                )
            chat_res_data.save_to_db()

            query = "UPDATE pack_response_count SET response_count=response_count+1 WHERE api_key='"+ apikey +"'"
            result = db.db.engine.execute(query)

            current_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
            date_query = "UPDATE chat_history SET updated_at='"+current_date+"' WHERE chat_resp_id="+ data['chat_history_id']
            result = db.db.engine.execute(date_query)
            api_key = data['api_key']
            
            query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE w.api_key='"+ api_key +"'"
            
            query = query + "ORDER BY s.subs_id DESC"

            print(query)
            print("######################################")
            package_details = db.db.engine.execute(query)
            pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]
            print(pack_result)
            print("###############========================")
            RESPONSE_PERMIT_COUNT = pack_result['response_count']
            RESPONSE_CURRENT_COUNT = 0

            resultList = PackResponseCountModel.get_by_api_key(api_key)
                
            result = []
            if resultList:
                result = PackResponseCountModel.to_json(resultList)
                RESPONSE_CURRENT_COUNT = result['response_count']
            
            
            print("+++++++=============RESPONSE_PERMIT_COUNT==================+++++++++++++++++++")
            print(RESPONSE_PERMIT_COUNT)    
            print("+++++++==============RESPONSE_CURRENT_COUNT=================+++++++++++++++++++")
            print(RESPONSE_CURRENT_COUNT)    

            if RESPONSE_CURRENT_COUNT > RESPONSE_PERMIT_COUNT:
                
                return {
                        'status': 400,
                        'success': False,
                        'message': 'An error occurred getting the response',
                    }, 400


  
    
    return rList





def responses(sentence,class_val,apikey,context_token,interest_category):

    rList = {'CLASS':'','QUESTION':'','HREF_LINK':'', 'AUDIO_LINK': '', 'VIDEO_LINK': '', 'IMAGE_LINK': '', 'RAW_TEXT': '', 'SUGGESTIONS_TEXT': []}


    website_list = WebsiteModel.get_by_apikey(apikey)
    website_data = WebsiteModel.to_json(website_list)
    if(class_val != None):

        check_classname = QuestionModel.find_by_class_val(class_val)
        if check_classname == 0:
            class_val = None

    if(class_val != None):

        rList['CLASS'] = class_val

        qresult = QuestionModel.get_by_classname(class_val,website_data['website_id'])

        # qq = qRes['questions'][0]
        qq = qresult


         
        if qq.playbook_id != 0 and qq.playbook_id != 'NULL':
            print(qq.playbook_id)
            print("QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ") 
            play_book_msg = PlayBookResponseModel.get_by_playbook_id(qq.playbook_id,'res')
            get_palybook_data = list(map(lambda x: PlayBookResponseModel.to_json(x), play_book_msg))
            print(get_palybook_data)
            print("get_palybook_data")
            
            data_list=[]
            test_list=[]
            wait_ind=100
            for idxy,datas in enumerate(get_palybook_data):
                
             
                if datas['wait_status']==1:
                    wait_ind = idxy
                    break
            for idx,data in enumerate(get_palybook_data):
                print("WWWWAAAAAAAAAAAAIIIIIIIIIIITTTTTTTTT")
                print(wait_ind)
                print(idx)
                print(type(datas['wait_status']))
                if data['wait_status']==0 and data['sequence']==idx+1 and idx < wait_ind:
                #if data['wait_status']==0 and idx < wait_ind:
                    data_list.append(data)
                
                if data['wait_status']!=0 and data['sequence']==idx+1 and idx == wait_ind:
                    test_list.append(data)
                    print(len(test_list))
                print("TTTTTTTTTTTTTTTRRRRRRRRRRRRRRR")
                print(test_list)
                if len(test_list) and test_list[-1]['node_type'] == "6":
                       
                    play_book_by_id = PlayBookResponseModel.get_by_playbook_id(test_list[-1]['playbook_id'],'res')  
                    get_palybook_data_by_id = list(map(lambda x: PlayBookResponseModel.to_json(x), play_book_by_id))
                    
                    wait_ind=100
                    for idxy,data_id in enumerate(get_palybook_data_by_id):
                
                        if data_id['wait_status']==1:
                            wait_ind = idxy
                            break
                    for idx,datasss in enumerate(get_palybook_data):
                        if datasss['wait_status']==0 and datasss['sequence']==idx+1 and idx < wait_ind:
                            data_list.append(datasss)
                
                        if datasss['wait_status']!=0 and datasss['sequence']==idx+1:
                            test_list.append(datasss)
                            print(len(test_list))

                if data['node_type']=='7':
                    get_api_by_node = PlayBookApiDeatailsModel.find_by_node_id(qq.playbook_id)
                    if get_api_by_node:
                        get_api_by_node = list(map(lambda x: PlayBookApiDeatailsModel.to_json(x), get_api_by_node))
                                  
                        get_api_header = PlayBookHeaderDetailsModel.find_by_api_id(get_api_by_node[0]['playBook_api_detail_id'])
                        if get_api_header:
                            get_api_header = list(map(lambda x: PlayBookHeaderDetailsModel.to_json(x), get_api_header))

                            API_KEY = '13076a9b6217587cbe0cfcb16b965c84'
                            url = get_api_by_node[0]['api_url']
                            headers=[]
                            for head in get_api_header:
                                head['header_value']= "'%s'" % head['header_value']
                                val = head['header_key']+'='+head['header_value']
                                headers.append(val)
                                s = ', '
                                header = s.join(headers)
                                params = dict(access_key='13076a9b6217587cbe0cfcb16b965c84', currencies='EUR,GBP,CAD,PLN', source='USD', format='1')
                                #header = eval(header)
                                print("*******************************")
                                print(header)
                                #params = dict('',header)
                                # for value in get_api_header:
                                #     pass
                                res = (requests.get(url,params=params))
                                print((res.text))
                                data['text_response'] =res.text
                    #data_list.append(data)
                    if data['wait_status']!=0:
                        test_list.append(data)      
            if len(test_list):     
                data_list.append(get_palybook_data[len(data_list)])
            
            for opt_list in (data_list):
              
                if opt_list['node_type'] == '3' or opt_list['node_type'] == '4':
                    get_node_option = PlayBookReplyOptionModel.get_option_node(opt_list['playbook_id'],opt_list['node_id'],opt_list['node_type'],0,opt_list['playbk_resp_det_id'])
                    get_palybook_option = list(map(lambda x: PlayBookReplyOptionModel.to_json(x), get_node_option))

                    data_list[len(data_list)-1]['options'] = get_palybook_option
            rList['QUESTION'] = qq.sentence
            rList['PLAYBOOK_TEXT'] = data_list
            rList['PLAYBOOK_ID'] = qq.playbook_id
            rList['HREF_LINK'] = ""
            rList['AUDIO_LINK'] = ""
            rList['VIDEO_LINK'] = ""
            rList['IMAGE_LINK'] = ""
            rList['SUGGESTIONS_TEXT'] = []
            rList['RESPONSE_TYPE'] = ""
            rList['CONTEXT'] = ""
            rList['USER_TOKEN'] = ""
            rList['CONTEXT_RES_ID'] = ""
            print("oooooooooooooooooooooooo")
            print(rList)
            return rList

        if qq.context:
            contextRes, contextGp, contextGpDetails,context_token = newContextualProcess(qq.context,website_data)

            rList['QUESTION'] = ""
            rList['RAW_TEXT'] = contextRes[0]['sentence']
            rList['HREF_LINK'] = ""
            rList['AUDIO_LINK'] = ""
            rList['VIDEO_LINK'] = ""
            rList['IMAGE_LINK'] = ""
            rList['CATEGORY'] = ""
            rList['SUGGESTIONS_TEXT'] = contextGpDetails
            rList['RESPONSE_TYPE'] = contextGp['response_type']
            rList['CONTEXT'] = qq.context
            rList['USER_TOKEN'] = context_token
            rList['CONTEXT_RES_ID'] = contextRes[0]['context_resp_id']
        
        else:
            # print(qq)
            rList['QUESTION'] = qq.sentence
            rList['RAW_TEXT'] = qq.response
            rList['HREF_LINK'] = qq.href_link
            rList['AUDIO_LINK'] = qq.audio_link
            rList['VIDEO_LINK'] = qq.video_link
            rList['IMAGE_LINK'] = qq.image_link
            rList['CATEGORY'] = qq.category_id
            rList['SUGGESTIONS_TEXT'] = []
            rList['RESPONSE_TYPE'] = ""
            rList['CONTEXT'] = ""
            rList['USER_TOKEN'] = ""
            rList['CONTEXT_RES_ID'] = ""

        return rList


   
    else:
        suggests=['About your company','Mission of your company?','What is iPITCH?','Do you have Business Incubation service?','Talk to our Agent']


        rList['QUESTION'] = sentence
        rList['RAW_TEXT'] = "I am unsure what you have asked for let me give you some suggestions please select one to continue."
        rList['HREF_LINK'] = ""
        rList['AUDIO_LINK'] = ""
        rList['VIDEO_LINK'] = ""
        rList['IMAGE_LINK'] = ""
        rList['CATEGORY'] = ""
        rList['SUGGESTIONS_TEXT'] = suggests
        rList['RESPONSE_TYPE'] = "BUTTON"
        rList['CONTEXT'] = ""
        rList['USER_TOKEN'] = ""
        rList['CONTEXT_RES_ID'] = ""

    return rList



def newContextualProcess(context_name,website_data):

    context_token = create_access_token(identity = website_data)

    context_gp_list = ContextGroupModel.get_by_contextname(context_name,website_data['website_id'])


    # context_gp = ContextGroupModel.to_json(context_gp_list)
    context_gp = list(map(lambda x: ContextGroupModel.to_json(x), context_gp_list))

    for value in context_gp:
        
        new_data = ContextResponseModel(
                website_id=website_data['website_id'],
                context_token=context_token,
                context_gp_id=value['context_gp_id'],
                context_name=value['context_name'],
                sentence=value['sentence'],
                status=0
            )
        new_data.save_to_db()

    
    getContextResData = getContextualProcess(context_name,context_token)
 

    
    context_gp_details = ContextGroupDetailsModel.get_by_context_gp_id_custom(getContextResData[0]['context_gp_id'])

    context_gp_data = ContextGroupModel.get_by_context_gp_id(getContextResData[0]['context_gp_id'])

    context_gp = ContextGroupModel.to_json(context_gp_data)
    
    return getContextResData,context_gp,context_gp_details['context_group_details'],context_token

def getContextualProcess(context_name,context_token):

    
    context_resp_list = ContextResponseModel.get_by_contextname(context_name,context_token)

    context_resp = list(map(lambda x: ContextResponseModel.to_json(x), context_resp_list))

    return context_resp    

def updateContextualProcess(context_name,context_token,response,context_res_id):



    new_data = ContextResponseModel.get_by_id(context_res_id)

    new_data.response = response
    new_data.status = 1

    new_data.save_to_db()

    getContextResData = getContextualProcess(context_name,context_token)
    conv_status = False

    if getContextResData:
        # context_data = ContextResponseModel.get_by_id(context_res_id)
        # context_data = ContextResponseModel.to_json(context_data)

        context_data = ContextResponseModel.get_by_contextname(context_name,context_token)
        context_data = list(map(lambda x: ContextResponseModel.to_json(x), context_data))

        context_gp_details = ContextGroupDetailsModel.get_by_context_gp_id_custom(context_data[0]['context_gp_id'])
        context_gp_data = ContextGroupModel.get_by_context_gp_id(context_data[0]['context_gp_id'])
        context_gp = ContextGroupModel.to_json(context_gp_data)

    else:
        conv_status = True
        context_gp_details = []
        context_gp = []
        # context_gp_details['context_group_details'] = []
        # context_gp['context_group'] = []


    return getContextResData,context_gp,context_gp_details,context_token,conv_status
def getContextualSuccessMsg(context,website_id):
    context_msg = ContextGroupModel.get_success_by_contextname(context,website_id)

    return context_msg.success_msg


def readLogs(api_key):

    website_data = WebsiteModel.get_by_api_key(api_key)

    query = "SELECT pd.*,c.*,s.pack_end_at FROM website as w LEFT JOIN company as c ON c.company_id = w.company_id LEFT JOIN subscription as s ON s.company_id = w.company_id LEFT JOIN package_details as pd ON pd.package_id = s.package_id WHERE api_key='"+ api_key +"'"

    package_details = db.db.engine.execute(query)

    pack_result = list(map(lambda x: PackResponseCountModel.to_json_custom(x), package_details))[0]


    logs_count = pack_result['logs_count']


    # current_date = datetime.datetime.strptime("2011-01-01","%Y-%m-%d")
    date_before_week = datetime.today() - relativedelta(weeks=logs_count)

    filepath = "customlogs/" + str(website_data['company_code']) + "/" + str(website_data['website_name']) + "/" + "info.log"
    # filepath = "logs/" + company_code + "/" + website_name + "/" + "info.log"

    data = []
    with open(filepath, 'r') as f_in:
       
        lines = (line.rstrip() for line in f_in) 
        lines = list(line for line in lines if line) # Non-blank lines in a list

        for key,value in enumerate(lines):
            infoList = value.split('INFO')

            logged_date = infoList[0].split(',')[0]
            infoqaList = infoList[1].split('###')
            question = infoqaList[0]
            answer = infoqaList[1]

            logged_date = datetime.strptime(logged_date, '%Y-%m-%d %H:%M:%S')

            if date_before_week <= logged_date:
                data.append({'question':question, 'answer':answer,'logged_date': str(logged_date)})
        

    return data



