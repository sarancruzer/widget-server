from flask_restful import Resource, reqparse
from flask_jwt_extended import (jwt_required, get_jwt_identity)

from models.category import CategoryModel

from flask import request
from shared.util import custom_paginate, sqlalchemy_paginate
from models.user import UserModel
from resources.questionResources import QuestionList
from models.question import QuestionModel
import requests
import time
import shutil
# from deeppavlov import configs
# from deeppavlov.core.commands.infer import build_model
parser = reqparse.RequestParser()

parser.add_argument('category_id', required=False, help='This field cannot be left blank')
parser.add_argument('website_id', required=False, help='This field cannot be left blank')
parser.add_argument('category_name', required=False, help='Must enter the category_name')



class categoryMaster(Resource):
    def get(self):
   
        website_id = request.args.get('website_id', '')
        item = CategoryModel.get_by_website_id(website_id)
  
        data = list(map(lambda x: CategoryModel.to_json(x), item))

        # faq = build_model(configs.faq.fasttext_tfidf_autofaq, load_trained=  True)
        # a = faq(["I need help"])
        # print(a)
        print("AAAAAAAAAAAPPPPPPPPPPPPPPPIIIIIIIIIIIIIIII")
        
        if data:
            return {
                'status': 200,
                'success': True,
                'message': 'category received',
                'data': data
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404

class Category(Resource):

    
    def get(self):
       
        website_id = request.args.get('website_id', '')
        item = CategoryModel.find_by_id(website_id)


        
        if item:
            return {
                'status': 200,
                'success': True,
                'message': 'category received',
                'data': item
            }, 200

        else:
            return {
                'status': 404,
                'success': False,
                'message': 'Item not found',
            }, 404
            
    @jwt_required
    def delete(self, id):

        item = QuestionModel.get_by_categoryid(id)

        if item is None:
            
            item = CategoryModel.get_by_id(id)

            item.delete_from_db()

            return {
                'status': 200,
                'success': True,
                'message': 'Category has been deleted',
            }, 200
        
        else:
            return {
                    'status':400,
                    'success': True,
                    'message': "Category has been already used"
                    
                }, 400
   
           

    @jwt_required
    def put(self, id):
        # Create or Update
        data = parser.parse_args()
        new_data = CategoryModel.get_by_id(id)

        if new_data is None:
            new_data = CategoryModel(id, data['category_id'])
        else:
            new_data.category_name = data['category_name']
                
        try:
            new_data.save_to_db()
            return {
                'status': 200,
                'success': True,
                'message': 'category has been updated',
                'data': CategoryModel.to_json(new_data)
            }, 200
        except:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred updating the category',
            }, 400
       

class CategoryList(Resource):
    
    @jwt_required
    def get(self):
        page = request.args.get('page', 1)
        searchTerm = request.args.get('searchTerm', '')
        website_id = request.args.get('website_id', '')
        print("AAAAAAAAAAAPPPPPPPPPPPPPPPIIIIIIIIIIIIIIIId")
        
        result = CategoryModel.return_all(page, searchTerm,website_id)
        items = result.items
        cus_paging = sqlalchemy_paginate(result)        
        
        data = list(map(lambda x: CategoryModel.to_json(x), items))
        
        return {
                'status': 200,
                'success': True,
                'message': 'category received',
                'data':data,
                'paginate': cus_paging
                }, 200
    
    
    @jwt_required
    def post(self):
        
       
        data = parser.parse_args()
        if CategoryModel.find_by_name(data['category_name'],data['website_id']):
            return {
                'status': 409,
                'success': False,
                'message': 'Category Name {} already exists'.format(data['category_name']),
            },409
      
        current_user = get_jwt_identity()

        new_data = CategoryModel(
            category_id=data['category_id'],
            website_id = data['website_id'],
            category_name=data['category_name'],
            created_by= current_user['user_id'],
            status=1
        )

        try:
            new_data.save_to_db()
            return {
                    'status': 200,
                    'success': True,
                    'message': 'category has been created ',
                    'data': CategoryModel.to_json(new_data)
                }, 200
        except Exception as e:
            return {
                'status': 400,
                'success': False,
                'message': 'An error occurred inserting the category',
            }, 400
            
            

