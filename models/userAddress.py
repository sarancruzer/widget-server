from shared.db import db
from passlib.hash import pbkdf2_sha256 as sha256
from sqlalchemy import Table, Column, Float, Integer, String, Date, Text
from sqlalchemy.sql.sqltypes import DATETIME
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import text
from flask_jwt_extended.utils import get_jwt_identity
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import backref, relationship, joinedload




class UserAddressModel(db.Model):

    __tablename__ = 'user_address'

    user_address_id = Column(Integer, primary_key=True)
    user_id = Column("user_id", Integer,nullable = False)
    name = Column("name", String(100),nullable = False)
    mobile = Column("mobile", String(100), nullable = False)
    pincode = Column('pincode', String(100), nullable = False)
    country = Column('country', Text, nullable = False)
    address = Column('address', Text, nullable = False)
    city = Column('city', Text, nullable = False)
    state = Column('state', Text, nullable = False)
    landmark = Column('landmark', String(100), nullable = True)
    mobile_alternate = Column('mobile_alternate', Integer, nullable = True)       
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),                        )
    status = Column('status', Integer ,nullable = True,server_default=text('1'))
    

    @classmethod
    def get_by_userid(cls, id):        
        return cls.query.filter_by(user_id=id).first()

    def save_to_db(self):

        db.session.add(self)
        db.session.commit()
        db.session.flush()
        return
    
    def delete_from_db(self):

        db.session.delete(self)
        db.session.commit()

    @classmethod
    def to_json(cls, x):
            return {
                'user_address_id': x.user_address_id,
                'user_id': x.user_id,
                'name':x.name,
                'mobile':x.mobile,
                'pincode': x.pincode,
                'country': x.country,
                'address': x.address,
                'city':x.city,
                'state': x.state,
                'landmark':x.landmark,
                'mobile_alternate': x.mobile_alternate,
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at)
            }
    
    
    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}
   

