from shared.db import db
from sqlalchemy import Table, Column, Float, Integer, String, Date
from sqlalchemy.sql.sqltypes import Text, DATETIME
from sqlalchemy.sql.expression import text
from sqlalchemy.sql.functions import func


class IconModel(db.Model):
    __tablename__ = 'icons'

    icon_id = Column('icon_id',Integer, primary_key=True)
    icon_name = Column('icon_name', Text, nullable = True) 
    icon_type = Column('icon_type', Text, nullable = True) 
    icon_path = Column('icon_path', Text, nullable = True) 
   
    created_at = Column('created_at', DATETIME, nullable=False,server_default=func.current_timestamp())
    updated_at = Column('updated_at', DATETIME, nullable=False,server_default=text('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
    status = Column('status', Integer, nullable = True)

    

    @classmethod
    def return_all(cls):        
        return { 'icons': list(map(lambda x: IconModel.to_json(x), IconModel.query.all())) }

    
    @classmethod
    def get_user_icons(cls):        
        return { 'user_icons': list(map(lambda x: IconModel.to_json(x), IconModel.query.filter_by(icon_type = "user").all())) }

    @classmethod
    def get_bot_icons(cls):        
        return { 'bot_icons': list(map(lambda x: IconModel.to_json(x), IconModel.query.filter_by(icon_type = "bot").all())) }
            
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
        db.session.flush(self)
        return 

   

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()


    @classmethod
    def to_json(cls, x):
            return {
                'icon_id':x.icon_id,
                'icon_name':x.icon_name,
                'icon_type':x.icon_type,
                'icon_path': x.icon_path,
                
                'created_at':str(x.created_at),
                'updated_at':str(x.updated_at),
                'status':x.status
            }
    

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}

