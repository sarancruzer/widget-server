import json

from shared.logs import  configLogs
from models.question import QuestionModel
from services.chatbotService import askqFunc
logger = configLogs()

import time

def automateTestAskQFunc(id):
    
    i=0
    
    with open('jsons/aianr_full.json', 'r') as f:
        data = json.load(f)
    
        for value in data:

            time.sleep(2)
            # print(value)            
            res = askqFunc(value['sentence'])            
            if res['class'] == value['class']:                
                logger.error("SUCCESS " + str(value['class']) + " ===== " + str(res['class']))
                pass            
            else:
                i = i+1
                logger.error("FAILED " + str(value['class']) + " ===== " + str(res['class']))
            
    return i


def automateImportData():    

    
    with open('jsons/aianr.json', 'r') as f:
        data = json.load(f)
        
        print(data)
        for value in data:
            
            new_user = QuestionModel(
                user_id = 2,
                class_name = value['class'],
                sentence = value['sentence'],
                response = value['responses'],
                status = 1
            )        
            new_user.save_to_db()
            
    
    return "null"


        


        # for value in data:
        #     pass
            
            
    
